add wave -noupdate -group FT601 -label i_usbClk /top_level_vhd_tst/usbClk
add wave -noupdate -group FT601 -label o_usbWRn /top_level_vhd_tst/usbWRn
add wave -noupdate -group FT601 -label i_usbRXFn /top_level_vhd_tst/usbRXFn
add wave -noupdate -group FT601 -label io_usbD -radix hexadecimal /top_level_vhd_tst/usbD
add wave -noupdate -group FT601 -label io_usbBE -radix hexadecimal /top_level_vhd_tst/usbBE
add wave -noupdate -group FT601 -label i_usbTXEn /top_level_vhd_tst/usbTXEn

add wave -noupdate -group TXFIFO0 -label i_txFifoWrclk /top_level_vhd_tst/txFifoWrclk(0)
add wave -noupdate -group TXFIFO0 -label i_txFifoWrreq /top_level_vhd_tst/txFifoWrreq(0)
add wave -noupdate -group TXFIFO0 -label o_txFifoWrfull /top_level_vhd_tst/txFifoWrfull(0)
add wave -noupdate -group TXFIFO0 -label i_txFifoData -radix hexadecimal /top_level_vhd_tst/txFifoData(0)

add wave -noupdate -group RXFIFO0 -label i_rxFifoRrclk /top_level_vhd_tst/rxFifoRdclk(0)
add wave -noupdate -group RXFIFO0 -label i_rxFifoRrreq /top_level_vhd_tst/rxFifoRdreq(0)
add wave -noupdate -group RXFIFO0 -label o_rxFifoRrempty /top_level_vhd_tst/rxFifoRdempty(0)
add wave -noupdate -group RXFIFO0 -label o_rxFifoQ -radix hexadecimal /top_level_vhd_tst/rxFifoQ(0)

add wave -noupdate -group TXFIFO1 -label i_txFifoWrclk /top_level_vhd_tst/txFifoWrclk(1)
add wave -noupdate -group TXFIFO1 -label i_txFifoWrreq /top_level_vhd_tst/txFifoWrreq(1)
add wave -noupdate -group TXFIFO1 -label o_txFifoWrfull /top_level_vhd_tst/txFifoWrfull(1)
add wave -noupdate -group TXFIFO1 -label i_txFifoData -radix hexadecimal /top_level_vhd_tst/txFifoData(1)

add wave -noupdate -group RXFIFO1 -label i_rxFifoRrclk /top_level_vhd_tst/rxFifoRdclk(1)
add wave -noupdate -group RXFIFO1 -label i_rxFifoRrreq /top_level_vhd_tst/rxFifoRdreq(1)
add wave -noupdate -group RXFIFO1 -label o_rxFifoRrempty /top_level_vhd_tst/rxFifoRdempty(1)
add wave -noupdate -group RXFIFO1 -label o_rxFifoQ -radix hexadecimal /top_level_vhd_tst/rxFifoQ(1)

add wave -noupdate -group DBG_TX0 -label state sim:/top_level_vhd_tst/i1/comusbwrite(0)/comusbwrite/state
add wave -noupdate -group DBG_TX1 -label state sim:/top_level_vhd_tst/i1/comusbwrite(1)/comusbwrite/state
add wave -noupdate -group DBG_RX0 -label state /top_level_vhd_tst/i1/comusbread(0)/comusbread/state
add wave -noupdate -group DBG_RX1 -label state /top_level_vhd_tst/i1/comusbread(1)/comusbread/state

add wave -noupdate -group RESET -label i_reset /top_level_vhd_tst/reset