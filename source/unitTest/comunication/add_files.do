transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

# Top level files
vcom -93 -work work {../../source/comunication/common/stdLogicArrayPkg.vhd}
vcom -93 -work work {../../source/comunication/usbRead/usbRead.vhd}
vcom -93 -work work {../../source/comunication/usbWrite/usbWrite.vhd}
vcom -93 -work work {../../source/comunication/usbFIFO/usbFIFO.vhd}
vcom -93 -work work {../../source/comunication/usbArbitrer/usbArbitrer.vhd}
vcom -93 -work work {../../source/comunication/communicationArrays.vhd}
vcom -93 -work work {../../source/comunication/communication.vhd}
