-- Copyright (C) 1991-2011 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "12/03/2012 13:50:45"
                                                            
-- Vhdl Test Bench template for design  :  usbWrite
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;  
USE ieee.numeric_std.all;   
use IEEE.STD_LOGIC_UNSIGNED.ALL;    

library work; 
use work.communicationArray.all;                       

ENTITY top_level_vhd_tst IS
GENERIC 
	(
		-- Cantidad de canales 2^CHANNELS
		constant CHANNELS : INTEGER := 1;
		-- Periodo del clock de lectura 
		constant FT601_CLK_PERIOD : time := 10 ns;
		-- Periodo del clock de escritura
		constant USR_CLK_PERIOD : time := 25 ns;
		-- Inputs Min Hold Time 
		constant IN_MIN_HOLD_TIME : time := 3.5 ns;
		-- Outputs Min Setup Time
		constant OUT_MIN_SETUP_TIME : time := 1 ns;
		
		constant TX_STIM_INC : std_logic_vector(31 downto 0) := x"04030201";
		constant TX_BYTES_STIM_INC : std_logic_vector(1 downto 0) := "01"
	);

END top_level_vhd_tst;
ARCHITECTURE usbFIFO_arch OF top_level_vhd_tst IS
-- constants                                                 
-- signals  
SIGNAL usbClk 	: std_logic;
SIGNAL usbWRn 	: std_logic;
SIGNAL usbRXFn 	: std_logic;
SIGNAL usbD  	: std_logic_vector (31 downto 0);
SIGNAL usbBE	: std_logic_vector (3 downto 0);
SIGNAL usbTXEn	: std_logic;
SIGNAL usbOEn	: std_logic;
SIGNAL usbRDn	: std_logic;
SIGNAL usbSIWUn	: std_logic;		
-- Se�ales de las fifo's de transmisi�n
SIGNAL txFifoWrclk	: std_logic_vector (2**CHANNELS-1 downto 0);
SIGNAL txFifoWrreq	: std_logic_vector (2**CHANNELS-1 downto 0);
SIGNAL txFifoWrfull : std_logic_vector (2**CHANNELS-1 downto 0);
SIGNAL txFifoData	: comm_array_of_std_logic_vector_34(2**CHANNELS-1 downto 0);
-- Se�ales de las fifo's de recepcici�n
SIGNAL rxFifoRdclk	: std_logic_vector (2**CHANNELS-1 downto 0);
SIGNAL rxFifoRdreq	: std_logic_vector (2**CHANNELS-1 downto 0);
SIGNAL rxFifoRdempty: std_logic_vector (2**CHANNELS-1 downto 0);
SIGNAL rxFifoQ		: comm_array_of_std_logic_vector_34(2**CHANNELS-1 downto 0);
-- Clock del usuario
SIGNAL usrClk 		: std_logic;
-- se�al de reset
SIGNAL reset	 	: std_logic; 

signal tx_data_stim : std_logic_vector(31 downto 0);
signal tx_be_stim: std_logic_vector(1 downto 0);
signal rx_data_expected : std_logic_vector(31 downto 0);
signal rx_be_expected: std_logic_vector(1 downto 0);
signal rx_beDecoded_expected: std_logic_vector(3 downto 0);

--alias test is
-- <<signal .i1.pendingData : std_logic>>;
 
COMPONENT communication
	GENERIC 
	(
		-- Numero total de canales que maneja el bloque 
		CHANNELS: INTEGER := 0;
		-- Tiempo para correr la politica de Round-robin
		ROUND_ROBIN_COUNTS: INTEGER := 1024
	);
	PORT (
		-- Se�ales de bus del FT601 a arbitrar
		usbClk	 	: in		std_logic;
		usbWRn 	 	: out		std_logic;
		usbRXFn	 	: in 		std_logic;
		usbD	  	: inout		std_logic_vector (31 downto 0);
		usbBE 		: inout		std_logic_vector (3 downto 0);
		usbTXEn	 	: in		std_logic;
		usbOEn	 	: out		std_logic;
		usbRDn	 	: out		std_logic;
		usbSIWUn	 	: out		std_logic;
		-- Se�ales de las fifo's de transmisi�n
		txFifoWrclk	: in	std_logic_vector (2**CHANNELS-1 downto 0);
		txFifoWrreq	: in	std_logic_vector (2**CHANNELS-1 downto 0);
		txFifoWrfull: out	std_logic_vector (2**CHANNELS-1 downto 0);
		txFifoData	: in	comm_array_of_std_logic_vector_34(2**CHANNELS-1 downto 0);
		-- Se�ales de las fifo's de recepcici�n
		rxFifoRdclk	: in	std_logic_vector (2**CHANNELS-1 downto 0);
		rxFifoRdreq	: in	std_logic_vector (2**CHANNELS-1 downto 0);
		rxFifoRdempty: out	std_logic_vector (2**CHANNELS-1 downto 0);
		rxFifoQ		: out	comm_array_of_std_logic_vector_34(2**CHANNELS-1 downto 0);
		-- se�al de reset
		reset	 	: in		std_logic 
	);
END COMPONENT;
BEGIN
	i1 : communication -- placa master
	GENERIC MAP (
		CHANNELS => CHANNELS,
		ROUND_ROBIN_COUNTS => 10
	)
	PORT MAP (
-- list connections usbbeinbetween master ports and signals
		usbClk => usbClk,
		usbWRn => usbWRn,
		usbRXFn => usbRXFn,
		usbD => usbD,
		usbBE => usbBE,
		usbTXEn => usbTXEn,
		usbOEn => usbOEn,
		usbRDn => usbRDn,
		usbSIWUn => usbSIWUn,
		txFifoWrclk => txFifoWrclk,
		txFifoWrreq => txFifoWrreq,
		txFifoWrfull => txFifoWrfull,
		txFifoData => txFifoData,
		rxFifoRdclk => rxFifoRdclk,
		rxFifoRdreq => rxFifoRdreq,
		rxFifoRdempty => rxFifoRdempty,
		rxFifoQ => rxFifoQ,
		reset => reset
	);

init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
        -- code that executes only once                      
WAIT;                                                       
END PROCESS init;                                                                                                                        


 
always : PROCESS 
	procedure stimFT601(
		constant usbRXFnStim 	: in	std_logic;
		constant usbDoe 		: in	std_logic_vector (3 downto 0);
		constant usbDStim 		: in	std_logic_vector (31 downto 0);
		constant usbBEoe 		: in	std_logic;
		constant usbBEStim 		: in	std_logic_vector (3 downto 0);
		constant usbTXEnStim 	: in 	std_logic
	) is
	begin
		usbRXFn <= usbRXFnStim;
		for I in 0 to 3 loop
			if (usbDoe(I) = '1') then
				usbD(8*(I+1)-1 downto 8*I) <= usbDStim(8*(I+1)-1 downto 8*I);
			else 
				usbD(8*(I+1)-1 downto 8*I) <= (others => 'Z');
			end if;
		end loop;
		if (usbBEoe = '1') then
			usbBE <= usbBEStim;
		else 
			usbBE <= (others => 'Z');
		end if;
		usbTXEn <= usbTXEnStim;
	end procedure stimFT601;
	
	procedure checkFT601(
		constant usbWRnExpected	: in	std_logic;
		constant usbDoeExpected	: in	std_logic_vector (3 downto 0);
		constant usbDExpected	: in    std_logic_vector (31 downto 0);
		constant usbBEoeExpected: in	std_logic;
		constant usbBEExpected	: in    std_logic_vector (3 downto 0)
	) is 
	begin
		assert usbWRn = usbWRnExpected 
		report
			"Unexpected result: " & "usbWRn =" & std_logic'image(usbWRn) & 
			"; " & "Expected = " & std_logic'image(usbWRnExpected)
		severity error;
		for I in 0 to 3 loop
			if (usbDoeExpected(I) = '1') then
				assert usbD(8*(I+1)-1 downto 8*I) = usbDExpected(8*(I+1)-1 downto 8*I) 
				report
					"Unexpected result: " & "q = " & integer'image(to_integer(unsigned(usbD(8*(I+1)-1 downto 8*I)))) &
					"; " & "Expected = " & integer'image(to_integer(unsigned(usbDExpected(8*(I+1)-1 downto 8*I))))
				severity error;
			end if;
		end loop;
		if (usbBEoeExpected = '1') then 
			assert usbBE = usbBEExpected
			report
				"Unexpected result: " & "q = " & integer'image(to_integer(unsigned(usbBE))) &
				"; " & "Expected = " & integer'image(to_integer(unsigned(usbBEExpected)))
			severity error;
		end if;
	end procedure checkFT601;
	
	procedure stimFT601AndCheck(
		-- Entradas de estimulo
		constant usbRXFnStim 	: in	std_logic;
		constant usbDoe 		: in	std_logic_vector (3 downto 0);
		constant usbDStim 		: in	std_logic_vector (31 downto 0);
		constant usbBEoe 		: in	std_logic;
		constant usbBEStim 		: in	std_logic_vector (3 downto 0);
		constant usbTXEnStim 	: in 	std_logic;
		-- Salidas esperadas
		constant usbWRnExpected	: in	std_logic;
		constant usbDoeExpected	: in	std_logic_vector (3 downto 0);
		constant usbDExpected	: in    std_logic_vector (31 downto 0);
		constant usbBEoeExpected: in	std_logic;
		constant usbBEExpected	: in    std_logic_vector (3 downto 0)
	) is 
	begin
		wait until rising_edge(usbClk);
		-- wait inputs hold time
		wait for IN_MIN_HOLD_TIME;
		--stimulate inputs
		stimFT601(usbRXFnStim, usbDoe, usbDStim,usbBEoe,usbBEStim,usbTXEnStim);
		--wait until output setup time to next clock
		wait for FT601_CLK_PERIOD-IN_MIN_HOLD_TIME-OUT_MIN_SETUP_TIME;
		--check expected output
		checkFT601(usbWRnExpected,usbDoeExpected,usbDExpected,usbBEoeExpected,usbBEExpected);
	end procedure stimFT601AndCheck;
	
	
	procedure stimTxCh(
		constant channel		 : in 	integer;
		constant txFifoWrreqStim : in	std_logic;
		constant txFifoDataStim  : in	std_logic_vector (33 downto 0)
	) is
	begin
		txFifoWrreq(channel) <= txFifoWrreqStim;
		txFifoData(channel) <= txFifoDataStim;
	end procedure stimTxCh;
	
	procedure stimTxChAndCheck(
		-- Entradas de estimulo
		constant channel 	: in	integer;
		constant txFifoWrreqStim : in	std_logic;
		constant txFifoDataStim  : in	std_logic_vector (33 downto 0);
		-- Salidas esperadas
		constant txFifoWrfullExpected	: in STD_LOGIC
	) is 
	begin
		wait until rising_edge(usrClk);
		-- wait inputs hold time
		wait for IN_MIN_HOLD_TIME;
		--stimulate inputs
		stimTxCh(channel, txFifoWrreqStim, txFifoDataStim);
		--wait until output setup time to next clock
		wait for USR_CLK_PERIOD-IN_MIN_HOLD_TIME-OUT_MIN_SETUP_TIME;
		--check expected output
		assert txFifoWrfull(channel) = txFifoWrfullExpected 
		report
			"Unexpected result: " & "txFifoWrfull(" & integer'image(channel) &
			") = " & std_logic'image(txFifoWrfull(channel) ) & 
			"; " & "Expected = " & std_logic'image(txFifoWrfullExpected)
		severity error;
	end procedure stimTxChAndCheck;
	
	procedure stimRxCh(
		constant channel		 : in 	integer;
		constant rxFifoRdreqStim : in	std_logic
	) is
	begin
		rxFifoRdreq(channel) <= rxFifoRdreqStim;
	end procedure stimRxCh;
	
	procedure stimRxChAndCheck(
		-- Entradas de estimulo
		constant channel		 : in 	integer;
		constant rxFifoRdreqStim : in	std_logic;
		-- Salidas esperadas
		constant rxFifoRdemptyExpected	: in STD_LOGIC;
		constant rxFifoBEExpected : in std_logic_vector (1 downto 0);
		constant rxFifoQExpected : in std_logic_vector (31 downto 0)
	) is 
	begin
		wait until rising_edge(usrClk);
		-- wait inputs hold time
		wait for IN_MIN_HOLD_TIME;
		--stimulate inputs
		stimRxCh(channel, rxFifoRdreqStim);
		--wait until output setup time to next clock
		wait for USR_CLK_PERIOD-IN_MIN_HOLD_TIME-OUT_MIN_SETUP_TIME;
		--check expected output
		assert rxFifoRdempty(channel) = rxFifoRdemptyExpected 
		report
			"Unexpected result: " & "rxFifoRrempty(" & integer'image(channel) &
			") = " & std_logic'image(rxFifoRdempty(channel) ) & 
			"; " & "Expected = " & std_logic'image(rxFifoRdemptyExpected)
		severity error;
		if (rxFifoRdempty(channel)='0') then 
			assert rxFifoQ(channel)(33 downto 32) = rxFifoBEExpected 
			report
				"Unexpected result: " & "rxFifoQ(" & integer'image(channel) &
				")(33:32) = " & integer'image(to_integer(unsigned(rxFifoQ(channel)(33 downto 31)))) &
				"; " & "Expected = " & integer'image(to_integer(unsigned(rxFifoBEExpected)))
			severity error;
			assert rxFifoQ(channel)(31 downto 0) = rxFifoQExpected 
			report
				"Unexpected result: " & "rxFifoQ(" & integer'image(channel) &
				")(31:0) = " & integer'image(to_integer(unsigned(rxFifoQ(channel)(31 downto 0)))) &
				"; " & "Expected = " & integer'image(to_integer(unsigned(rxFifoQExpected)))
			severity error;
		end if;
	end procedure stimRxChAndCheck;
	
	procedure waitForFIFONotFull(
		constant channel		 : in 	integer
	)is
	begin
		for I in 0 to 10 loop
			if txFifoWrfull(channel) = '0' then
				exit;
			else
				wait until rising_edge(usrClk);
				wait for USR_CLK_PERIOD-OUT_MIN_SETUP_TIME;
			end if;
		end loop;
		assert txFifoWrfull(channel) = '0' 
		report
			"Unexpected result: " & "txFifoWrfull(" & integer'image(channel) &
			") = " & std_logic'image(txFifoWrfull(channel) ) & 
			"; " & "Expected = " & std_logic'image('0')
		severity error;
	end procedure waitForFIFONotFull;
	
	procedure waitForFIFONotEmpty(
		constant channel		 : in 	integer
	)is
	begin
		for I in 0 to 10 loop
			if rxFifoRdempty(channel) = '0' then
				exit;
			else
				wait until rising_edge(usrClk);
				wait for USR_CLK_PERIOD-OUT_MIN_SETUP_TIME;
			end if;
		end loop;
		assert rxFifoRdempty(channel) = '0' 
		report
			"Unexpected result: " & "rxFifoRdempty(" & integer'image(channel) &
			") = " & std_logic'image(rxFifoRdempty(channel) ) & 
			"; " & "Expected = " & std_logic'image('0')
		severity error;
	end procedure waitForFIFONotEmpty;
	
	procedure waitForUsbWRnLow(
		constant timeout		 : in 	integer
	)is
	begin
		for I in 0 to timeout loop
			if usbWRn = '0' then
				exit;
			else
				wait until rising_edge(usbClk);
				wait for FT601_CLK_PERIOD-OUT_MIN_SETUP_TIME;
			end if;
		end loop;
		assert usbWRn = '0' 
		report
			"Unexpected result: " & "usbWRn = " & std_logic'image(usbWRn) & 
			"; " & "Expected = " & std_logic'image('0')
		severity error;
	end procedure waitForUsbWRnLow;

-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN
	tx_data_stim <= TX_STIM_INC;
	tx_be_stim <= TX_BYTES_STIM_INC;
	rx_data_expected <= TX_STIM_INC;
	rx_be_expected <= TX_BYTES_STIM_INC;
	
	-- Estado inicial
	-- (usbRXFn, usbDoe, usbD, usbBEoe, usbBE, usbTXEn), --Inputs
	stimFT601('1', x"2", x"0000F000", '0', x"0", '0');
	-- (channel, txFifoWrreq, txFifoData), --Inputs
	stimTxCh(0, '0', (others => '0'));
	stimTxCh(1, '0', (others => '0'));
	-- (channel, rxFifoRdreqStim), --Inputs
	stimRxCh(0, '0');
	stimRxCh(1, '0');
	
	-- Nos aseguramos que se lea la se�al de reset
	wait until rising_edge(usbClk);
	wait until rising_edge(usbClk);
	wait until rising_edge(usrClk);
	wait until rising_edge(usrClk);
	
	-- Escribimos en el canal 0
	waitForFIFONotFull(0);
	-- (channel, txFifoWrreq, txFifoData), --Inputs
		--[txFifoWrfull] --Outpus
	for I in 0 to 3 loop
		tx_data_stim <= tx_data_stim+TX_STIM_INC;
		tx_be_stim <= tx_be_stim+TX_BYTES_STIM_INC;
		stimTxChAndCheck(0, '1', tx_be_stim & tx_data_stim,
			'0');
	end loop;
	stimTxChAndCheck(0, '0', tx_be_stim & tx_data_stim,
		'0');
	
	-- Leemos el canal 0
	waitForUsbWRnLow(10);
	-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	checkFT601('0', "0001", x"00000001",'1',x"1");
	-- (usbRXFn, usbDoeStim, usbD, usbBEoeStim, usbBE, usbTXEn), --Inputs
		-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0001", x"00000001",'1',x"1");
	for I in 0 to 3 loop
		stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
			'0', "1111", rx_data_expected,'1',rx_beDecoded_expected);
		rx_data_expected <= rx_data_expected+TX_STIM_INC;
		rx_be_expected <= rx_be_expected+TX_BYTES_STIM_INC;
	end loop;
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
			'1', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
			'1', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"2", x"0000F000", '0', x"0", '0',
			'1', "0000", x"00000000",'0',x"0");
	
	-- Escribimos en el canal 1
	waitForFIFONotFull(1);
	-- (channel, txFifoWrreq, txFifoData), --Inputs
		--[txFifoWrfull] --Outpus
	for I in 0 to 3 loop
		tx_data_stim <= tx_data_stim+TX_STIM_INC;
		tx_be_stim <= tx_be_stim+TX_BYTES_STIM_INC;
		stimTxChAndCheck(1, '1', tx_be_stim & tx_data_stim,
			'0');
	end loop;
	stimTxChAndCheck(1, '0', tx_be_stim & tx_data_stim,
		'0');
	
	-- Leemos el canal 1
	waitForUsbWRnLow(10);
	-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	checkFT601('0', "0001", x"00000002",'1',x"1");
	-- (usbRXFn, usbDoeStim, usbD, usbBEoeStim, usbBE, usbTXEn), --Inputs
		-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0001", x"00000002",'1',x"1");
	for I in 0 to 3 loop
		stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
			'0', "1111", rx_data_expected,'1',rx_beDecoded_expected);
		rx_data_expected <= rx_data_expected+TX_STIM_INC;
		rx_be_expected <= rx_be_expected+TX_BYTES_STIM_INC;
	end loop;
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
			'1', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
			'1', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"2", x"0000F000", '0', x"0", '0',
			'1', "0000", x"00000000",'0',x"0");
			
	-- Recibimos datos de la PC
	stimFT601AndCheck('1', x"2", x"0000E000", '0', x"0", '0',
			'1', "0000", x"00000000",'0',x"0");
	-- Leemos el canal 1
	waitForUsbWRnLow(10);
	-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	checkFT601('0', "0001", x"00000001",'1',x"0");
	-- (usbRXFn, usbDoeStim, usbD, usbBEoeStim, usbBE, usbTXEn), --Inputs
		-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0000", x"00000000",'0',x"0");
	for I in 0 to 3 loop
		stimFT601AndCheck('0', x"F", rx_data_expected, '1', rx_beDecoded_expected, '1',
			'0', "0000", x"00000000",'0',x"0");
		rx_data_expected <= rx_data_expected+TX_STIM_INC;
		rx_be_expected <= rx_be_expected+TX_BYTES_STIM_INC;
	end loop;	
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'1', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"2", x"0000F000", '0', x"0", '0',
		'1', "0000", x"00000000",'0',x"0");
		
	-- Leemos la FIFO del canal 1
	waitForFIFONotEmpty(0);
	-- (channel, rxFifoRdreqStim), --Inputs
		--[rxFifoRremptyExpected, rxFifoQExpected] --Outpus
	for I in 0 to 3 loop
		stimRxChAndCheck(0, '1',
			'0', tx_be_stim, tx_data_stim);
		tx_data_stim <= tx_data_stim+TX_STIM_INC;
		tx_be_stim <= tx_be_stim+TX_BYTES_STIM_INC;
	end loop;
	stimRxChAndCheck(0, '0',
			'1', tx_be_stim, tx_data_stim);
			
			
	-- Recibimos datos de la PC
	stimFT601AndCheck('1', x"2", x"0000D000", '0', x"0", '0',
			'1', "0000", x"00000000",'0',x"0");
	-- Leemos el canal 1
	waitForUsbWRnLow(10);
	-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	checkFT601('0', "0001", x"00000002",'1',x"0");
	-- (usbRXFn, usbDoeStim, usbD, usbBEoeStim, usbBE, usbTXEn), --Inputs
		-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0000", x"00000000",'0',x"0");
	for I in 0 to 3 loop
		stimFT601AndCheck('0', x"F", rx_data_expected, '1', rx_beDecoded_expected, '1',
			'0', "0000", x"00000000",'0',x"0");
		rx_data_expected <= rx_data_expected+TX_STIM_INC;
		rx_be_expected <= rx_be_expected+TX_BYTES_STIM_INC;
	end loop;	
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'1', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"2", x"0000F000", '0', x"0", '0',
		'1', "0000", x"00000000",'0',x"0");
		
	-- Leemos la FIFO del canal 1
	waitForFIFONotEmpty(1);
	-- (channel, rxFifoRdreqStim), --Inputs
		--[rxFifoRremptyExpected, rxFifoQExpected] --Outpus
	for I in 0 to 3 loop
		stimRxChAndCheck(1, '1',
			'0', tx_be_stim, tx_data_stim);
		tx_data_stim <= tx_data_stim+TX_STIM_INC;
		tx_be_stim <= tx_be_stim+TX_BYTES_STIM_INC;
	end loop;
	stimRxChAndCheck(1, '0',
			'1', tx_be_stim, tx_data_stim);
                                       
			
	-- Puesta a prueba del round robin
	stimFT601AndCheck('1', x"2", x"0000E000", '0', x"0", '0',
			'1', "0000", x"00000000",'0',x"0");
	-- Leemos el canal 1
	waitForUsbWRnLow(10);
	-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	checkFT601('0', "0001", x"00000001",'1',x"0");
	-- (usbRXFn, usbDoeStim, usbD, usbBEoeStim, usbBE, usbTXEn), --Inputs
		-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0000", x"00000000",'0',x"0");
	for I in 0 to 7 loop
		stimFT601AndCheck('0', x"F", rx_data_expected, '1', rx_beDecoded_expected, '1',
			'0', "0000", x"00000000",'0',x"0");
		rx_data_expected <= rx_data_expected+TX_STIM_INC;
		rx_be_expected <= rx_be_expected+TX_BYTES_STIM_INC;
	end loop;	
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'1', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'1', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"2", x"0000E000", '0', x"0", '0',
		'1', "0000", x"00000000",'0',x"0");
	-- Leemos el canal 1
	waitForUsbWRnLow(10);
	-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	checkFT601('0', "0001", x"00000001",'1',x"0");
	-- (usbRXFn, usbDoeStim, usbD, usbBEoeStim, usbBE, usbTXEn), --Inputs
		-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0000", x"00000000",'0',x"0");
	for I in 0 to 3 loop
		stimFT601AndCheck('0', x"F", rx_data_expected, '1', rx_beDecoded_expected, '1',
			'0', "0000", x"00000000",'0',x"0");
		rx_data_expected <= rx_data_expected+TX_STIM_INC;
		rx_be_expected <= rx_be_expected+TX_BYTES_STIM_INC;
	end loop;	
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'1', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"2", x"0000F000", '0', x"0", '0',
		'1', "0000", x"00000000",'0',x"0");
		
		
	    WAIT;             
END PROCESS always;

decode_be: process (rx_be_expected)
begin
	case rx_be_expected is
		when "00"=>
			rx_beDecoded_expected <= "0001";
		when "01"=>
			rx_beDecoded_expected <= "0011";
		when "10"=>
			rx_beDecoded_expected <= "0111";
		when "11"=>
			rx_beDecoded_expected <= "1111";
		when others=>
			rx_beDecoded_expected <= "0000";
	end case;
end process decode_be;

rd_wr_fifo_clk_driver: FOR i IN 2**CHANNELS-1 DOWNTO 0 GENERATE
	txFifoWrclk(I) <= usrClk;
	rxFifoRdclk(I) <= usrClk;
END GENERATE rd_wr_fifo_clk_driver; 

reset_gen : PROCESS
begin
	reset <= '1';
	wait until rising_edge(usbClk);
	wait until rising_edge(usrClk);
	reset <= '0';	
	wait;
end process reset_gen;

ft601_clk_gen : PROCESS
begin
	usbClk <= '1';
	-- slave_clk <= TRANSPORT usb_clk AFTER 10 ns;
	loop
		-- La F real es de 60Mhz pero corremos la simulaci�n
		-- 66Mhz para facilitar los calculos y ver que con un poco m�s anda
		wait for FT601_CLK_PERIOD/2;
		usbClk <= not usbClk;
	end loop;
end process ft601_clk_gen;

usr_clk_gen : PROCESS
begin
	usrClk <= '1';
	-- slave_clk <= TRANSPORT usb_clk AFTER 10 ns;
	loop
		-- La F real es de 60Mhz pero corremos la simulaci�n
		-- 66Mhz para facilitar los calculos y ver que con un poco m�s anda
		wait for USR_CLK_PERIOD/2;
		usrClk <= not usrClk;
	end loop;
end process usr_clk_gen;

END usbFIFO_arch;

