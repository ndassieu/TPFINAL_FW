add wave -noupdate -group FT601_BUS -label i_usbClk /top_level_vhd_tst/usbClk
add wave -noupdate -group FT601_BUS -label i_usbWRn /top_level_vhd_tst/usbWRn
add wave -noupdate -group FT601_BUS -label i_usbRXn /top_level_vhd_tst/usbRXn
add wave -noupdate -group FT601_BUS -label i_usbDoe -radix hexadecimal /top_level_vhd_tst/usbDoe
add wave -noupdate -group FT601_BUS -label o_usbDout -radix hexadecimal /top_level_vhd_tst/usbDout
add wave -noupdate -group FT601_BUS -label i_usbDin -radix hexadecimal /top_level_vhd_tst/usbDin
add wave -noupdate -group FT601_BUS -label o_usbBEoe /top_level_vhd_tst/usbBEoe
add wave -noupdate -group FT601_BUS -label o_usbBEout -radix hexadecimal /top_level_vhd_tst/usbBEout
add wave -noupdate -group FT601_BUS -label o_usbBEin -radix hexadecimal /top_level_vhd_tst/usbBEin
add wave -noupdate -group FT601_BUS -label i_usbTXEn /top_level_vhd_tst/usbTXEn
add wave -noupdate -group FIFO -label o_fifoWrreq /top_level_vhd_tst/fifoWrreq
add wave -noupdate -group FIFO -label i_fifoWrfull /top_level_vhd_tst/fifoWrfull
add wave -noupdate -group FIFO -label i_fifoD -radix hexadecimal /top_level_vhd_tst/fifoD
add wave -noupdate -group ARBITRER -label o_busRequest /top_level_vhd_tst/busRequest
add wave -noupdate -group ARBITRER -label o_busInUse /top_level_vhd_tst/busInUse
add wave -noupdate -group ARBITRER -label i_busGranted /top_level_vhd_tst/busGranted
add wave -noupdate -group RESET -label i_reset /top_level_vhd_tst/reset

add wave -noupdate -group DBG -label state  /top_level_vhd_tst/i1/state 
add wave -noupdate -group DBG -label encodeBE  /top_level_vhd_tst/i1/encodeBE 
