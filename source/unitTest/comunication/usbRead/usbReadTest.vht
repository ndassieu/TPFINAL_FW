-- Copyright (C) 1991-2011 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "12/03/2012 13:50:45"
                                                            
-- Vhdl Test Bench template for design  :  usbWrite
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;  
USE ieee.numeric_std.all;   
use IEEE.STD_LOGIC_UNSIGNED.ALL;                           

ENTITY top_level_vhd_tst IS
GENERIC 
	(
		-- Periodo del clock 
		constant CLK_PERIOD : time := 10 ns;
		-- Inputs Min Hold Time 
		constant IN_MIN_HOLD_TIME : time := 3.5 ns;
		-- Outputs Min Setup Time
		constant OUT_MIN_SETUP_TIME : time := 1 ns
	);

END top_level_vhd_tst;
ARCHITECTURE usbWrite_arch OF top_level_vhd_tst IS
-- constants                                                 
-- signals      
SIGNAL usbClk	 	: std_logic;
SIGNAL usbWRn 	 	: std_logic;
SIGNAL usbRXn	 	: std_logic;
SIGNAL usbDoe 	 	: std_logic_vector (3 downto 0);
SIGNAL usbDout  	: std_logic_vector (31 downto 0);
SIGNAL usbDin	 	: std_logic_vector (31 downto 0);
SIGNAL usbBEoe	 	: std_logic;
SIGNAL usbBEout 	: std_logic_vector (3 downto 0);
SIGNAL usbBEin 	: std_logic_vector (3 downto 0);
SIGNAL usbTXEn	 	: std_logic;
-- se�ales de manejo de la FIFO
SIGNAL fifoWrreq	: std_logic;
SIGNAL fifoWrfull	: std_logic;
SIGNAL fifoD		: std_logic_vector (33 downto 0);
-- se�ales del arbitro de bus
SIGNAL busRequest	: std_logic;
SIGNAL busInUse		: std_logic;
SIGNAL busGranted	: std_logic;
-- se�al de reset
SIGNAL reset	 	: std_logic;
	
--alias test is
-- <<signal .i1.pendingData : std_logic>>;
 
COMPONENT usbRead
	PORT (
		usbClk	 	: in		std_logic;
		usbWRn 	 	: out		std_logic;
		usbRXn	 	: in 		std_logic;
		usbDoe 	 	: out 		std_logic_vector (3 downto 0);
		usbDout  	: out		std_logic_vector (31 downto 0);
		usbDin	 	: in		std_logic_vector (31 downto 0);
		usbBEoe	 	: out		std_logic;
		usbBEout 	: out		std_logic_vector (3 downto 0);
		usbBEin 	: in		std_logic_vector (3 downto 0);
		usbTXEn	 	: in		std_logic;
		-- se�ales de manejo de la FIFO
		fifoWrreq	: out		std_logic;
		fifoWrfull	: in		std_logic;
		fifoD		: out		std_logic_vector (33 downto 0);
		-- se�ales del arbitro de bus
		busRequest	: out		std_logic;
		busInUse	: out		std_logic;
		busGranted	: in		std_logic;
		-- se�al de reset
		reset	 	: in		std_logic
	);
END COMPONENT;
BEGIN
	i1 : usbRead -- placa master
	PORT MAP (
-- list connections usbbeinbetween master ports and signals
	usbClk => usbClk,
	usbWRn => usbWRn,
	usbRXn => usbRXn,
	usbDoe => usbDoe,
	usbDout => usbDout,
	usbDin => usbDin,
	usbBEoe => usbBEoe,
	usbBEout => usbBEout,
	usbBEin => usbBEin,
	usbTXEn => usbTXEn,
	fifoWrreq => fifoWrreq,
	fifoWrfull => fifoWrfull,
	fifoD => fifoD,
	busRequest => busRequest,
	busInUse => busInUse,
	busGranted => busGranted,
	reset => reset
	);

init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
        -- code that executes only once                      
WAIT;                                                       
END PROCESS init;                                                                                                                        


 
always : PROCESS 
	
	procedure stim(
		constant usbRXnStim	 	 : in 	std_logic;
		constant usbDinStim	 	 : in	std_logic_vector (31 downto 0);
		constant usbBEinStim	 : in	std_logic_vector (3 downto 0);
		constant usbTXEnStim	 : in	std_logic;
		constant fifoWrfullStim  : in	std_logic;
		constant busGrantedStim	 : in	std_logic;
		constant resetStim	 	 : in	std_logic
	) is
	begin
		usbRXn <= usbRXnStim;
		usbDin <= usbDinStim;
		usbBEin <= usbBEinStim;
		usbTXEn <= usbTXEnStim;
		fifoWrfull <= fifoWrfullStim;
		busGranted <= busGrantedStim;
		reset <= resetStim;
	end procedure stim;
     
	procedure stim_and_check(
		-- Entradas de estimulo
		constant usbRXnStim	 	 : in 	std_logic;
		constant usbDinStim	 	 : in	std_logic_vector (31 downto 0);
		constant usbBEinStim	 : in	std_logic_vector (3 downto 0);
		constant usbTXEnStim	 : in	std_logic;
		constant fifoWrfullStim : in	std_logic;
		constant busGrantedStim	 : in	std_logic;
		constant resetStim	 	 : in	std_logic;
		-- Salidas esperadas
		constant usbWRnExpected 	 	: in	std_logic;
		constant usbDoeExpected 	 	: in 	std_logic_vector (3 downto 0);
		constant usbDoutExpected  	: in	std_logic_vector (31 downto 0);
		constant usbBEoeExpected	 	: in	std_logic;
		constant usbBEoutExpected 	: in	std_logic_vector (3 downto 0);
		constant fifoDExpected   : in 	std_logic_vector (33 downto 0);
		constant fifoWrreqExpected	: in	std_logic;
		constant busRequestExpected	: in	std_logic;
		constant busInUseExpected	: in	std_logic
	) is 
	begin
		wait until rising_edge(usbClk);
		-- wait inputs hold time
		--wait for IN_MIN_HOLD_TIME;
		--stimulate inputs
		stim(usbRXnStim, usbDinStim, usbBEinStim, usbTXEnStim, fifoWrfullStim, busGrantedStim, resetStim);
		--wait until output setup time to next clock
		wait for CLK_PERIOD-IN_MIN_HOLD_TIME-OUT_MIN_SETUP_TIME;
		--check expected output
		assert usbWRn = usbWRnExpected 
		report
			"Unexpected result: " & "usbWRn = " & std_logic'image(usbWRn) &
			"; " & "expected = " & std_logic'image(usbWRnExpected)
		severity error;
		assert usbDoe = usbDoeExpected 
		report
			"Unexpected result: " & "usbDoe = " & integer'image(to_integer(unsigned(usbDoe))) &
			"; " & "Expected = " & integer'image(to_integer(unsigned(usbDoeExpected)))
		severity error;
		for I in 1 to 4 loop
			if (usbDoe(I-1) = '1') then
				assert usbDout(I*8-1 downto (I-1)*8) = usbDoutExpected(I*8-1 downto (I-1)*8) 
				report
					"Unexpected result: " & "usbDout = " & integer'image(to_integer(unsigned(usbDout(I*8-1 downto (I-1)*8)))) &
					"; " & "Expected = " & integer'image(to_integer(unsigned(usbDoutExpected(I*8-1 downto (I-1)*8) )))
				severity error;
			end if;
		end loop;
		assert usbBEoe = usbBEoeExpected 
		report
			"Unexpected result: " & "usbBEoe = " & std_logic'image(usbBEoe) &
			"; " & "Expected = " & std_logic'image(usbBEoeExpected)
		severity error;
		if (usbBEoe = '1') then
			assert usbBEout(3 downto 0) = usbBEoutExpected(3 downto 0) 
			report
				"Unexpected result: " & "usbBEout = " & integer'image(to_integer(unsigned(usbBEout(3 downto 0)))) &
				"; " & "Expected = " & integer'image(to_integer(unsigned(usbBEoutExpected(3 downto 0))))
			severity error;
		end if;
		assert fifoWrreq = fifoWrreqExpected 
		report
			"Unexpected result: " & "fifoWrreq = " & std_logic'image(fifoWrreq) &
			"; " & "Expected = " & std_logic'image(fifoWrreqExpected)
		severity error;
		if (fifoWrreq = '1') then
			assert fifoD(33 downto 0) = fifoDExpected(33 downto 0) 
			report
				"Unexpected result: " & "usbBEout = " & integer'image(to_integer(unsigned(fifoD(33 downto 0)))) &
				"; " & "Expected = " & integer'image(to_integer(unsigned(fifoDExpected(33 downto 0))))
			severity error;
		end if;
		assert busRequest = busRequestExpected 
		report
			"Unexpected result: " & "busRequest = " & std_logic'image(busRequest) &
			"; " & "Expected = " & std_logic'image(busRequestExpected)
		severity error;
		assert busInUse = busInUseExpected 
		report
			"Unexpected result: " & "busInUse = " & std_logic'image(busInUse) &
			"; " & "Expected = " & std_logic'image(busInUseExpected)
		severity error;
	end procedure stim_and_check;

	
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN
	-- Estado inicial
	-- (usbRXn, usbDin, usbBEin, usbTXEn, fifoWrfull, busGranted, reset), --Inputs
	stim('1', x"0000EF00", x"0", '0', '1', '0', '1');
	
	-- (usbRXn, usbDin, usbBEin, usbTXEn, fifoWrfull, busGranted, reset), --Inputs
		--[usbWRn, usbDoe, usbDout, usbBEoe, usbBEout, fifoD, fifoWrreq, busRequest, busInUse] -- expected outputs
	stim_and_check('1', x"0000EF00", x"0", '0', '1', '0', '0',
			'1',x"0",x"00000000",'0',x"0",(others=>'0'),'0','0','0');
	
	---- FIFO not full start - FIFO full stop (bus granted on request)
	-- (usbRXn, usbDin, usbBEin, usbTXEn, fifoWrfull, busGranted, reset), --Inputs
	--	[usbWRn, usbDoe, usbDout, usbBEoe, usbBEout, fifoD, fifoWrreq, busRequest, busInUse] -- expected outputs
	stim_and_check('1', x"0000EF00", x"0", '0', '1', '0', '0',
			'1',x"0",x"00000000",'0',x"0",(others=>'0'),'0','0','0');
	stim_and_check('1', x"0000EF00", x"0", '0', '0', '1', '0',
			'1',x"0",x"00000000",'0',x"0",(others=>'0'),'0','1','0');
	stim_and_check('1', x"0000EF00", x"0", '0', '0', '1', '0',
			'0',x"1",x"00000001",'1',x"0",(others=>'0'),'0','1','1');
	stim_and_check('1', x"00000000", x"0", '1', '0', '1', '0',
			'0',x"0",x"00000000",'0',x"0",(others=>'0'),'0','1','1');
	stim_and_check('1', x"00000000", x"0", '1', '0', '1', '0',
			'0',x"0",x"00000000",'0',x"0",(others=>'0'),'0','1','1');
	stim_and_check('0', x"80100801", x"F", '1', '0', '1', '0',
			'0',x"0",x"00000000",'0',x"0","11" & x"80100801",'1','1','1');
	stim_and_check('0', x"09029020", x"F", '1', '0', '1', '0',
			'0',x"0",x"00000000",'0',x"0","11" & x"09029020",'1','1','1');
	stim_and_check('0', x"A0300A03", x"3", '1', '0', '1', '0',
			'0',x"0",x"00000000",'0',x"0","01" & x"A0300A03",'1','1','1');
	stim_and_check('0', x"00000000", x"0", '1', '1', '1', '0',
			'1',x"0",x"00000000",'0',x"0",(others=>'0'),'0','1','1');
	stim_and_check('1', x"00000000", x"0", '1', '1', '0', '0',
			'1',x"0",x"00000000",'0',x"0",(others=>'0'),'0','0','0');
	stim_and_check('1', x"0000FF00", x"0", '0', '1', '0', '0',
			'1',x"0",x"00000000",'0',x"0",(others=>'0'),'0','0','0');

	-- FT601 not full start - FT601 full stop (bus granted on request)
	-- (usbRXn, usbDin, usbBEin, usbTXEn, fifoWrfull, busGranted, reset), --Inputs
	--	[usbWRn, usbDoe, usbDout, usbBEoe, usbBEout, fifoD, fifoWrreq, busRequest, busInUse] -- expected outputs
	stim_and_check('1', x"0000FF00", x"0", '0', '0', '0', '0',
			'1',x"0",x"00000000",'0',x"0",(others=>'0'),'0','0','0');
	stim_and_check('1', x"0000FF00", x"0", '0', '0', '0', '0',
			'1',x"0",x"00000000",'0',x"0",(others=>'0'),'0','0','0');
	stim_and_check('1', x"0000EF00", x"0", '0', '0', '1', '0',
			'1',x"0",x"00000000",'0',x"0",(others=>'0'),'0','1','0');
	stim_and_check('1', x"0000EF00", x"0", '0', '0', '1', '0',
			'0',x"1",x"00000001",'1',x"0",(others=>'0'),'0','1','1');
	stim_and_check('1', x"00000000", x"0", '1', '0', '1', '0',
			'0',x"0",x"00000000",'0',x"0",(others=>'0'),'0','1','1');
	stim_and_check('1', x"00000000", x"0", '1', '0', '1', '0',
			'0',x"0",x"00000000",'0',x"0",(others=>'0'),'0','1','1');
	stim_and_check('0', x"80100801", x"F", '1', '0', '1', '0',
			'0',x"0",x"00000000",'0',x"0","11" & x"80100801",'1','1','1');
	stim_and_check('0', x"09029020", x"F", '1', '0', '1', '0',
			'0',x"0",x"00000000",'0',x"0","11" & x"09029020",'1','1','1');
	stim_and_check('0', x"A0300A03", x"3", '1', '0', '1', '0',
			'0',x"0",x"00000000",'0',x"0","01" & x"A0300A03",'1','1','1');
	stim_and_check('1', x"00000000", x"0", '1', '0', '1', '0',
			'0',x"0",x"00000000",'0',x"0",(others=>'0'),'0','1','1');
	stim_and_check('1', x"0000FF00", x"0", '1', '0', '0', '0',
			'1',x"0",x"00000000",'0',x"0",(others=>'0'),'0','0','0');
	stim_and_check('1', x"0000FF00", x"0", '0', '0', '0', '0',
			'1',x"0",x"00000000",'0',x"0",(others=>'0'),'0','0','0');

	-- busGranted start after FT601 full stop - busGranted stop
	-- (usbRXn, usbDin, usbBEin, usbTXEn, fifoWrfull, busGranted, reset), --Inputs
	--	[usbWRn, usbDoe, usbDout, usbBEoe, usbBEout, fifoD, fifoWrreq, busRequest, busInUse] -- expected outputs
	stim_and_check('1', x"0000EF00", x"0", '0', '0', '0', '0',
			'1',x"0",x"00000000",'0',x"0",(others=>'0'),'0','1','0');
	stim_and_check('1', x"0000EF00", x"0", '0', '0', '0', '0',
			'1',x"0",x"00000000",'0',x"0",(others=>'0'),'0','1','0');
	stim_and_check('1', x"0000EF00", x"0", '0', '0', '1', '0',
			'1',x"0",x"00000000",'0',x"0",(others=>'0'),'0','1','0');
	stim_and_check('1', x"0000EF00", x"0", '0', '0', '1', '0',
			'0',x"1",x"00000001",'1',x"0",(others=>'0'),'0','1','1');
	stim_and_check('1', x"00000000", x"0", '1', '0', '1', '0',
			'0',x"0",x"00000000",'0',x"0",(others=>'0'),'0','1','1');
	stim_and_check('1', x"00000000", x"0", '1', '0', '1', '0',
			'0',x"0",x"00000000",'0',x"0",(others=>'0'),'0','1','1');
	stim_and_check('0', x"80100801", x"F", '1', '0', '1', '0',
			'0',x"0",x"00000000",'0',x"0","11" & x"80100801",'1','1','1');
	stim_and_check('0', x"09029020", x"F", '1', '0', '1', '0',
			'0',x"0",x"00000000",'0',x"0","11" & x"09029020",'1','1','1');
	stim_and_check('0', x"A0300A03", x"3", '1', '0', '1', '0',
			'0',x"0",x"00000000",'0',x"0","01" & x"A0300A03",'1','1','1');
	stim_and_check('0', x"00000000", x"0", '1', '0', '0', '0',
			'1',x"0",x"00000000",'0',x"0",(others=>'0'),'0','1','1');
	stim_and_check('1', x"00000000", x"0", '1', '0', '0', '0',
			'1',x"0",x"00000000",'0',x"0",(others=>'0'),'0','0','0');
	stim_and_check('1', x"0000FF00", x"0", '0', '0', '0', '0',
			'1',x"0",x"00000000",'0',x"0",(others=>'0'),'0','0','0');
	

WAIT;                                                        
END PROCESS always;

clk_gen : PROCESS
begin
	usbClk <= '1';
	-- slave_clk <= TRANSPORT usb_clk AFTER 10 ns;
	loop
		-- La F real es de 60Mhz pero corremos la simulaci�n
		-- 66Mhz para facilitar los calculos y ver que con un poco m�s anda
		wait for CLK_PERIOD/2;
		usbClk <= not usbClk;
	end loop;
end process clk_gen;


END usbWrite_arch;

