# compile all files
do add_files.do

# Compule signal simulation file
vcom -93 -work work {usbReadTest.vht}
vsim -t 1ps -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L cycloneiii -L rtl_work -L work -voptargs="+acc" top_level_vhd_tst

# Add all the signals
do add_waves.do

# Expands P_RW signals
add wave -expand -group FT601_BUS
add wave -expand -group FIFO
add wave -expand -group ARBITRER
add wave -expand -group RESET

view structure
view signals

run 450 ns
wave zoomfull
update