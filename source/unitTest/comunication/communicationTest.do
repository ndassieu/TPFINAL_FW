# compile all files
do add_files.do

# Compule signal simulation file
vcom -93 -work work {communicationTest.vht}
vsim -t 1ps -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L cycloneiii -L rtl_work -L work -voptargs="+acc" top_level_vhd_tst

# Add all the signals
do add_waves.do

# Expands P_RW signals
add wave -expand -group FT601
add wave -expand -group TXFIFO0
add wave -expand -group RXFIFO0
add wave -expand -group RESET

view structure
view signals

run 1.55 us
wave zoomfull
update