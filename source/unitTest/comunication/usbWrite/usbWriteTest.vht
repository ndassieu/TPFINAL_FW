-- Copyright (C) 1991-2011 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "12/03/2012 13:50:45"
                                                            
-- Vhdl Test Bench template for design  :  usbWrite
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;  
USE ieee.numeric_std.all;   
use IEEE.STD_LOGIC_UNSIGNED.ALL;                           

ENTITY top_level_vhd_tst IS
GENERIC 
	(
		-- Periodo del clock 
		constant CLK_PERIOD : time := 10 ns;
		-- Inputs Min Hold Time 
		constant IN_MIN_HOLD_TIME : time := 3.5 ns;
		-- Outputs Min Setup Time
		constant OUT_MIN_SETUP_TIME : time := 1 ns
	);

END top_level_vhd_tst;
ARCHITECTURE usbWrite_arch OF top_level_vhd_tst IS
-- constants                                                 
-- signals      
SIGNAL usbClk	 	: std_logic;
SIGNAL usbWRn 	 	: std_logic;
SIGNAL usbRXn	 	: std_logic;
SIGNAL usbDoe 	 	: std_logic_vector (3 downto 0);
SIGNAL usbDout  	: std_logic_vector (31 downto 0);
SIGNAL usbDin	 	: std_logic_vector (7 downto 0);
SIGNAL usbBEoe	 	: std_logic;
SIGNAL usbBEout 	: std_logic_vector (3 downto 0);
SIGNAL usbTXEn	 	: std_logic;
-- se�ales de manejo de la FIFO
SIGNAL fifoRdreq	: std_logic;
SIGNAL fifoRdempty	: std_logic;
SIGNAL fifoQ		: std_logic_vector (33 downto 0);
-- se�ales del arbitro de bus
SIGNAL busRequest	: std_logic;
SIGNAL busInUse		: std_logic;
SIGNAL busGranted	: std_logic;
-- se�al de reset
SIGNAL reset	 	: std_logic;
	
--alias test is
-- <<signal .i1.pendingData : std_logic>>;
 
COMPONENT usbWrite
	PORT (
		usbClk	 	: in		std_logic;
		usbWRn 	 	: out		std_logic;
		usbRXn	 	: in 		std_logic;
		usbDoe 	 	: out 		std_logic_vector (3 downto 0);
		usbDout  	: out		std_logic_vector (31 downto 0);
		usbDin	 	: in		std_logic_vector (7 downto 0);
		usbBEoe	 	: out		std_logic;
		usbBEout 	: out		std_logic_vector (3 downto 0);
		usbTXEn	 	: in		std_logic;
		-- se�ales de manejo de la FIFO
		fifoRdreq	: out		std_logic;
		fifoRdempty	: in		std_logic;
		fifoQ		: in		std_logic_vector (33 downto 0);
		-- se�ales del arbitro de bus
		busRequest	: out		std_logic;
		busInUse	: out		std_logic;
		busGranted	: in		std_logic;
		-- se�al de reset
		reset	 	: in		std_logic
	);
END COMPONENT;
BEGIN
	i1 : usbWrite -- placa master
	PORT MAP (
-- list connections between master ports and signals
	usbClk => usbClk,
	usbWRn => usbWRn,
	usbRXn => usbRXn,
	usbDoe => usbDoe,
	usbDout => usbDout,
	usbDin => usbDin,
	usbBEoe => usbBEoe,
	usbBEout => usbBEout,
	usbTXEn => usbTXEn,
	fifoRdreq => fifoRdreq,
	fifoRdempty => fifoRdempty,
	fifoQ => fifoQ,
	busRequest => busRequest,
	busInUse => busInUse,
	busGranted => busGranted,
	reset => reset
	);

init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
        -- code that executes only once                      
WAIT;                                                       
END PROCESS init;                                                                                                                        


 
always : PROCESS 
	
	procedure stim(
		constant usbRXnStim	 	 : in 	std_logic;
		constant usbDinStim	 	 : in	std_logic_vector (7 downto 0);
		constant usbTXEnStim	 : in	std_logic;
		constant fifoRdemptyStim : in	std_logic;
		constant fifoQStim		 : in	std_logic_vector (33 downto 0);
		constant busGrantedStim	 : in	std_logic;
		constant resetStim	 	 : in	std_logic
	) is
	begin
		usbRXn <= usbRXnStim;
		usbDin <= usbDinStim;
		usbTXEn <= usbTXEnStim;
		fifoRdempty <= fifoRdemptyStim;
		fifoQ <= fifoQStim;
		busGranted <= busGrantedStim;
		reset <= resetStim;
	end procedure stim;
     
	procedure stim_and_check(
		-- Entradas de estimulo
		constant usbRXnStim	 	 : in 	std_logic;
		constant usbDinStim	 	 : in	std_logic_vector (7 downto 0);
		constant usbTXEnStim	 : in	std_logic;
		constant fifoRdemptyStim : in	std_logic;
		constant fifoQStim		 : in	std_logic_vector (33 downto 0);
		constant busGrantedStim	 : in	std_logic;
		constant resetStim	 	 : in	std_logic;
		-- Salidas esperadas
		constant usbWRnExpeted 	 	: in	std_logic;
		constant usbDoeExpeted 	 	: in 	std_logic_vector (3 downto 0);
		constant usbDoutExpeted  	: in	std_logic_vector (31 downto 0);
		constant usbBEoeExpeted	 	: in	std_logic;
		constant usbBEoutExpeted 	: in	std_logic_vector (3 downto 0);
		constant fifoRdreqExpeted	: in	std_logic;
		constant busRequestExpeted	: in	std_logic;
		constant busInUseExpeted	: in	std_logic
	) is 
	begin
		wait until rising_edge(usbClk);
		-- wait inputs hold time
		--wait for IN_MIN_HOLD_TIME;
		--stimulate inputs
		stim(usbRXnStim, usbDinStim, usbTXEnStim, fifoRdemptyStim, fifoQStim, busGrantedStim, resetStim);
		--wait until output setup time to next clock
		wait for CLK_PERIOD-IN_MIN_HOLD_TIME-OUT_MIN_SETUP_TIME;
		--check expected output
		assert usbWRn = usbWRnExpeted 
		report
			"Unexpected result: " & "usbWRn = " & std_logic'image(usbWRn) &
			"; " & "expected = " & std_logic'image(usbWRnExpeted)
		severity error;
		assert usbDoe = usbDoeExpeted 
		report
			"Unexpected result: " & "usbDoe = " & integer'image(to_integer(unsigned(usbDoe))) &
			"; " & "expeted = " & integer'image(to_integer(unsigned(usbDoeExpeted)))
		severity error;
		for I in 1 to 4 loop
			if (usbDoe(I-1) = '1') then
				assert usbDout(I*8-1 downto (I-1)*8) = usbDoutExpeted(I*8-1 downto (I-1)*8) 
				report
					"Unexpected result: " & "usbDout = " & integer'image(to_integer(unsigned(usbDout(I*8-1 downto (I-1)*8)))) &
					"; " & "expeted = " & integer'image(to_integer(unsigned(usbDoutExpeted(I*8-1 downto (I-1)*8) )))
				severity error;
			end if;
		end loop;
		assert usbBEoe = usbBEoeExpeted 
		report
			"Unexpected result: " & "usbBEoe = " & std_logic'image(usbBEoe) &
			"; " & "expeted = " & std_logic'image(usbBEoeExpeted)
		severity error;
		if (usbBEoe = '1') then
			assert usbBEout(3 downto 0) = usbBEoutExpeted(3 downto 0) 
			report
				"Unexpected result: " & "usbBEout = " & integer'image(to_integer(unsigned(usbBEout(3 downto 0)))) &
				"; " & "expeted = " & integer'image(to_integer(unsigned(usbBEoutExpeted(3 downto 0))))
			severity error;
		end if;
		assert fifoRdreq = fifoRdreqExpeted 
		report
			"Unexpected result: " & "fifoRdreq = " & std_logic'image(fifoRdreq) &
			"; " & "expeted = " & std_logic'image(fifoRdreqExpeted)
		severity error;
		assert busRequest = busRequestExpeted 
		report
			"Unexpected result: " & "busRequest = " & std_logic'image(busRequest) &
			"; " & "expeted = " & std_logic'image(busRequestExpeted)
		severity error;
		assert busInUse = busInUseExpeted 
		report
			"Unexpected result: " & "busInUse = " & std_logic'image(busInUse) &
			"; " & "expeted = " & std_logic'image(busInUseExpeted)
		severity error;
	end procedure stim_and_check;

	
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN
	-- Estado inicial
	stim('1', x"FF", '0', '1', (others => '0'), '0', '1');
	-- (usbRXn, usbDin, usbTXEn, fifoRdempty, fifoQ, busGranted, reset), --Inputs
			--[usbWRn, usbDoe, usbDout, usbBEoe, usbBEout, fifoRdreq, busRequest, busInUse] -- expected outputs
	stim_and_check('1', x"FF", '0', '1', (others => '0'), '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','0','0');
	
	-- FIFO not empty start - FIFO empty stop (bus granted on request)
	-- (usbRXn, usbDin, usbTXEn, fifoRdempty, fifoQ, busGranted, reset), --Inputs
			--[usbWRn, usbDoe, usbDout, usbBEoe, usbBEout, fifoRdreq, busRequest, busInUse] -- expected outputs
	stim_and_check('1', x"FE", '0', '0', "11" & x"80100801", '1', '0',
			'1',x"0",x"00000000",'0',x"0",'0','1','0');
	stim_and_check('1', x"FE", '0', '0', "11" & x"80100801", '1', '0',
			'0',x"1",x"00000001",'1',x"1",'0','1','1');
	stim_and_check('1', x"00", '1', '0', "11" & x"80100801", '1', '0',
			'0',x"1",x"00000001",'1',x"1",'1','1','1');
	stim_and_check('0', x"00", '1', '0', "11" & x"09029020", '1', '0',
			'0',x"F",x"80100801",'1',x"F",'1','1','1');
	stim_and_check('0', x"00", '1', '0', "01" & x"A0300A03", '1', '0',
			'0',x"F",x"09029020",'1',x"F",'1','1','1');
	stim_and_check('0', x"00", '1', '1', "00" & x"00000000", '1', '0',
			'0',x"F",x"A0300A03",'1',x"3",'1','1','1');
	stim_and_check('0', x"00", '1', '1', "00" & x"00000000", '1', '0',
			'1',x"0",x"00000000",'0',x"0",'0','1','1');
	stim_and_check('1', x"00", '1', '1', "00" & x"00000000", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','0','0');
	stim_and_check('1', x"FE", '0', '1', "00" & x"00000000", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','0','0');

	-- FT601 not full start - FT601 full stop (bus granted on request)
	-- (usbRXn, usbDin, usbTXEn, fifoRdempty, fifoQ, busGranted, reset), --Inputs
		--[usbWRn, usbDoe, usbDout, usbBEoe, usbBEout, fifoRdreq, busRequest, busInUse] -- expected outputs
	stim_and_check('1', x"FF", '0', '1', "11" & x"00000000", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','0','0');
	stim_and_check('1', x"FF", '0', '0', "11" & x"80100801", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','0','0');
	stim_and_check('1', x"FF", '0', '0', "11" & x"80100801", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','0','0');
	stim_and_check('1', x"FE", '0', '0', "11" & x"80100801", '1', '0',
			'1',x"0",x"00000000",'0',x"0",'0','1','0');
	stim_and_check('1', x"FE", '0', '0', "11" & x"80100801", '1', '0',
			'0',x"1",x"00000001",'1',x"1",'0','1','1');
	stim_and_check('1', x"00", '1', '0', "11" & x"80100801", '1', '0',
			'0',x"1",x"00000001",'1',x"1",'1','1','1');
	stim_and_check('0', x"00", '1', '0', "11" & x"09029020", '1', '0',
			'0',x"F",x"80100801",'1',x"F",'1','1','1');
	stim_and_check('0', x"00", '1', '0', "01" & x"A0300A03", '1', '0',
			'0',x"F",x"09029020",'1',x"F",'1','1','1');
	stim_and_check('0', x"00", '1', '0', "11" & x"0B04B040", '1', '0',
			'0',x"F",x"A0300A03",'1',x"3",'1','1','1');
	stim_and_check('1', x"00", '1', '0', "10" & x"C0500C05", '1', '0',
			'0',x"0",x"00000000",'0',x"0",'0','1','1');
	stim_and_check('1', x"00", '1', '0', "10" & x"C0500C05", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','0','0');
	stim_and_check('1', x"FF", '0', '0', "10" & x"C0500C05", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','0','0');

	-- busGranted start after FT601 full stop - busGranted stop
	-- (usbRXn, usbDin, usbTXEn, fifoRdempty, fifoQ, busGranted, reset), --Inputs
		--[usbWRn, usbDoe, usbDout, usbBEoe, usbBEout, fifoRdreq, busRequest, busInUse] -- expected outputs
	stim_and_check('1', x"FF", '0', '0', "10" & x"C0500C05", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','0','0');
	stim_and_check('1', x"FE", '0', '0', "10" & x"C0500C05", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','1','0');
	stim_and_check('1', x"FE", '0', '0', "10" & x"C0500C05", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','1','0');
	stim_and_check('1', x"FE", '0', '0', "10" & x"C0500C05", '1', '0',
			'1',x"0",x"00000000",'0',x"0",'0','1','0');
	stim_and_check('1', x"FE", '0', '0', "10" & x"C0500C05", '1', '0',
			'0',x"1",x"00000001",'1',x"1",'0','1','1');
	stim_and_check('1', x"00", '1', '0', "10" & x"C0500C05", '1', '0',
			'0',x"1",x"00000001",'1',x"1",'0','1','1');
	stim_and_check('0', x"00", '1', '0', "10" & x"C0500C05", '1', '0',
			'0',x"F",x"0B04B040",'1',x"F",'1','1','1');
	stim_and_check('0', x"00", '1', '0', "11" & x"0D06D060", '1', '0',
			'0',x"F",x"C0500C05",'1',x"7",'1','1','1');
	stim_and_check('0', x"00", '1', '0', "11" & x"E0700E07", '0', '0',
			'0',x"F",x"0D06D060",'1',x"F",'0','1','1');
	stim_and_check('0', x"00", '1', '0', "11" & x"E0700E07", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','1','1');
	stim_and_check('1', x"00", '1', '0', "11" & x"E0700E07", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','0','0');
	stim_and_check('1', x"FF", '0', '0', "11" & x"E0700E07", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','0','0');
			
	-- Data pendinf start - fifo empty stop
	-- (usbRXn, usbDin, usbTXEn, fifoRdempty, fifoQ, busGranted, reset), --Inputs
	--[usbWRn, usbDoe, usbDout, usbBEoe, usbBEout, fifoRdreq, busRequest, busInUse] -- expected outputs
	-- Esto genera un data pending
	stim_and_check('1', x"FF", '0', '1', "11" & x"00000000", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','0','0');
	stim_and_check('1', x"FF", '0', '0', "11" & x"80100801", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','0','0');
	stim_and_check('1', x"FF", '0', '0', "11" & x"80100801", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','0','0');
	stim_and_check('1', x"FE", '0', '0', "11" & x"80100801", '1', '0',
			'1',x"0",x"00000000",'0',x"0",'0','1','0');
	stim_and_check('1', x"FE", '0', '0', "11" & x"80100801", '1', '0',
			'0',x"1",x"00000001",'1',x"1",'0','1','1');
	stim_and_check('1', x"00", '1', '0', "11" & x"80100801", '1', '0',
			'0',x"1",x"00000001",'1',x"1",'1','1','1');
	stim_and_check('0', x"00", '1', '0', "01" & x"09029020", '1', '0',
			'0',x"F",x"80100801",'1',x"F",'1','1','1');
	stim_and_check('1', x"00", '1', '1', "01" & x"A0300A03", '1', '0',
			'0',x"0",x"00000000",'0',x"0",'0','1','1');
	stim_and_check('1', x"00", '1', '1', "01" & x"A0300A03", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','0','0');
	stim_and_check('1', x"FF", '0', '1', "01" & x"A0300A03", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','0','0');
	-- Esto envia el ultimo byte pendiente
	stim_and_check('1', x"FF", '0', '1', "01" & x"A0300A03", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','0','0');
	stim_and_check('1', x"FE", '0', '1', "01" & x"A0300A03", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','0','0');
	stim_and_check('1', x"FE", '0', '1', "10" & x"A0300A03", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','1','0');
	stim_and_check('1', x"FE", '0', '1', "10" & x"A0300A03", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','1','0');
	stim_and_check('1', x"FE", '0', '1', "10" & x"A0300A03", '1', '0',
			'1',x"0",x"00000000",'0',x"0",'0','1','0');
	stim_and_check('1', x"FE", '0', '1', "10" & x"A0300A03", '1', '0',
			'0',x"1",x"00000001",'1',x"1",'0','1','1');
	stim_and_check('1', x"00", '1', '1', "10" & x"A0300A03", '1', '0',
			'0',x"1",x"00000001",'1',x"1",'0','1','1');		
	stim_and_check('0', x"00", '1', '1', "10" & x"A0300A03", '1', '0',
			'0',x"F",x"09029020",'1',x"3",'1','1','1');	
	stim_and_check('0', x"00", '1', '1', "10" & x"A0300A03", '1', '0',
			'1',x"0",x"00000000",'0',x"0",'0','1','1');
	stim_and_check('1', x"00", '1', '1', "10" & x"A0300A03", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','0','0');
	stim_and_check('1', x"FE", '0', '1', "10" & x"00000000", '0', '0',
			'1',x"0",x"00000000",'0',x"0",'0','0','0');
WAIT;                                                        
END PROCESS always;

clk_gen : PROCESS
begin
	usbClk <= '1';
	-- slave_clk <= TRANSPORT usb_clk AFTER 10 ns;
	loop
		-- La F real es de 60Mhz pero corremos la simulaci�n
		-- 66Mhz para facilitar los calculos y ver que con un poco m�s anda
		wait for CLK_PERIOD/2;
		usbClk <= not usbClk;
	end loop;
end process clk_gen;


END usbWrite_arch;

