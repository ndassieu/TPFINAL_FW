add wave -noupdate -group FT601_BUS -label i_usbClk /top_level_vhd_tst/usbClk
add wave -noupdate -group FT601_BUS -label o_chUsbWRn /top_level_vhd_tst/chUsbWRn
add wave -noupdate -group FT601_BUS -label i_chUsbRXFn /top_level_vhd_tst/chUsbRXFn
add wave -noupdate -group FT601_BUS -label io_usbD -radix hexadecimal /top_level_vhd_tst/usbD
add wave -noupdate -group FT601_BUS -label io_usbBE -radix hexadecimal /top_level_vhd_tst/usbBE
add wave -noupdate -group FT601_BUS -label i_usbTXEn /top_level_vhd_tst/usbTXEn

add wave -noupdate -group CHANNELS -label i_chUsbWRn /top_level_vhd_tst/chUsbWRn
add wave -noupdate -group CHANNELS -label o_chUsbRXFn /top_level_vhd_tst/chUsbRXFn
add wave -noupdate -group CHANNELS -label i_chUsbDoe /top_level_vhd_tst/chUsbDoe
add wave -noupdate -group CHANNELS -label i_chUsbDout -radix hexadecimal /top_level_vhd_tst/chUsbDout
add wave -noupdate -group CHANNELS -label o_chUsbDin -radix hexadecimal /top_level_vhd_tst/chUsbDin
add wave -noupdate -group CHANNELS -label i_chUsbBEoe /top_level_vhd_tst/chUsbBEoe
add wave -noupdate -group CHANNELS -label i_chUsbBEout -radix hexadecimal /top_level_vhd_tst/chUsbBEout
add wave -noupdate -group CHANNELS -label o_chUsbBEin -radix hexadecimal /top_level_vhd_tst/chUsbBEin
add wave -noupdate -group CHANNELS -label o_chUsbTXEn /top_level_vhd_tst/chUsbTXEn

add wave -noupdate -group ARBITRER -label i_chBusRequest /top_level_vhd_tst/chBusRequest
add wave -noupdate -group ARBITRER -label i_chBusInuse /top_level_vhd_tst/chBusInuse
add wave -noupdate -group ARBITRER -label o_chBusGranted /top_level_vhd_tst/chBusGranted

add wave -noupdate -group RESET -label i_reset /top_level_vhd_tst/reset

add wave -noupdate -group DBG -label state  /top_level_vhd_tst/i1/state
add wave -noupdate -group DBG -label grantedChannel  /top_level_vhd_tst/i1/grantedChannel 
add wave -noupdate -group DBG -label elapsedTime  /top_level_vhd_tst/i1/elapsedTime 
add wave -noupdate -group DBG -label nextChannel  /top_level_vhd_tst/i1/nextChannel 
