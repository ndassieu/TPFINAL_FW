-- Copyright (C) 1991-2011 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "12/03/2012 13:50:45"
                                                            
-- Vhdl Test Bench template for design  :  usbWrite
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;  
USE ieee.numeric_std.all;   
use IEEE.STD_LOGIC_UNSIGNED.ALL;                    

library work; 
use work.communicationStdLogicArray.all;

ENTITY top_level_vhd_tst IS
GENERIC 
	(
		-- Periodo del clock 
		constant CLK_PERIOD : time := 10 ns;
		-- Inputs Min Hold Time 
		constant IN_MIN_HOLD_TIME : time := 3.5 ns;
		-- Outputs Min Setup Time
		constant OUT_MIN_SETUP_TIME : time := 1 ns;
		
		constant CHANNELS: INTEGER := 2;
		constant TIMEOUT: INTEGER := 10
	);

END top_level_vhd_tst;
ARCHITECTURE usbWrite_arch OF top_level_vhd_tst IS
-- constants                                                 
-- FT601 signals     
SIGNAL usbClk	 	: std_logic;
SIGNAL usbWRn 	 	: std_logic;
SIGNAL usbRXFn	 	: std_logic;
SIGNAL usbD  		: std_logic_vector (31 downto 0);
SIGNAL usbBE 		: std_logic_vector (3 downto 0);
SIGNAL usbTXEn	 	: std_logic;
-- Se�ales de los canales a arbitrar
SIGNAL chUsbWRn		: std_logic_vector (CHANNELS-1 downto 0);
SIGNAL chUsbRXFn	: std_logic_vector (CHANNELS-1 downto 0);
SIGNAL chUsbDoe		: array_of_std_logic_vector_4(CHANNELS-1 downto 0);
SIGNAL chUsbDout	: array_of_std_logic_vector_32(CHANNELS-1 downto 0);
SIGNAL chUsbDin		: array_of_std_logic_vector_32(CHANNELS-1 downto 0);
SIGNAL chUsbBEoe	: std_logic_vector (CHANNELS-1 downto 0);
SIGNAL chUsbBEout	: array_of_std_logic_vector_4(CHANNELS-1 downto 0);
SIGNAL chUsbBEin	: array_of_std_logic_vector_4(CHANNELS-1 downto 0);
SIGNAL chUsbTXEn	: std_logic_vector (CHANNELS-1 downto 0);		
SIGNAL chBusRequest	: std_logic_vector (CHANNELS-1 downto 0);
SIGNAL chBusInuse  	: std_logic_vector (CHANNELS-1 downto 0);
SIGNAL chBusGranted	: std_logic_vector (CHANNELS-1 downto 0);
-- se�al de reset
SIGNAL reset	 	: std_logic;
	
SIGNAL dStim		: std_logic_vector (31 downto 0);
SIGNAL beStim		: std_logic_vector (3 downto 0);
SIGNAL chDoeStim	: std_logic_vector (3 downto 0);
SIGNAL chBEoeStim	: std_logic;

--alias test is
-- <<signal .i1.pendingData : std_logic>>;
 
COMPONENT usbArbitrer
	GENERIC 
	(
		-- Numero total de canales que maneja el bloque 
		CHANNELS: INTEGER := 2;
		-- Tiempo para correr la politica de Round-robin
		TIMEOUT: INTEGER := 1024
	);
	PORT (
		-- Se�ales de bus del FT601 a arbitrar
		usbClk	 	: in		std_logic;
		usbWRn 	 	: out		std_logic;
		usbRXFn	 	: in 		std_logic;
		usbD	  	: inout		std_logic_vector (31 downto 0);
		usbBE 		: inout		std_logic_vector (3 downto 0);
		usbTXEn	 	: in		std_logic;
		-- Se�ales de los canales a arbitrar
		chUsbWRn	: in	std_logic_vector (CHANNELS-1 downto 0);
		chUsbRXFn	: out	std_logic_vector (CHANNELS-1 downto 0);
		chUsbDoe	: in	array_of_std_logic_vector_4(CHANNELS-1 downto 0);
		chUsbDout	: in	array_of_std_logic_vector_32(CHANNELS-1 downto 0);
		chUsbDin	: out	array_of_std_logic_vector_32(CHANNELS-1 downto 0);
		chUsbBEoe	: in	std_logic_vector (CHANNELS-1 downto 0);
		chUsbBEout	: in	array_of_std_logic_vector_4(CHANNELS-1 downto 0);
		chUsbBEin	: out	array_of_std_logic_vector_4(CHANNELS-1 downto 0);
		chUsbTXEn	: out	std_logic_vector (CHANNELS-1 downto 0);			
		-- Se�ales de control de los canales
		chBusRequest: in	std_logic_vector (CHANNELS-1 downto 0);
		chBusInuse  : in	std_logic_vector (CHANNELS-1 downto 0);
		chBusGranted: out	std_logic_vector (CHANNELS-1 downto 0);
		-- se�al de reset
		reset	 	: in		std_logic
	);
END COMPONENT;

function isInUse(busInUse : std_logic_vector (CHANNELS-1 downto 0)) 
		return integer is
	variable result : integer := -1;
begin
	for i in 0 to (CHANNELS-1) loop
		if busInUse(i) = '1'  then
			result := i;
			exit;
		end if;
	end loop;
	return result;
end function;

BEGIN
	i1 : usbArbitrer -- placa master
	GENERIC MAP (
		CHANNELS => CHANNELS,
		TIMEOUT => TIMEOUT
	)
	PORT MAP (
-- list connections usbbeinbetween master ports and signals
		usbClk => usbClk,
		usbWRn => usbWRn,
		usbRXFn => usbRXFn,
		usbD => usbD,
		usbBE => usbBE,
		usbTXEn => usbTXEn,
		chUsbWRn => chUsbWRn,
		chUsbRXFn => chUsbRXFn,
		chUsbDoe => chUsbDoe,
		chUsbDout => chUsbDout,
		chUsbDin => chUsbDin,
		chUsbBEoe => chUsbBEoe,
		chUsbBEout => chUsbBEout,
		chUsbBEin => chUsbBEin,
		chUsbTXEn => chUsbTXEn,
		chBusRequest => chBusRequest,
		chBusInuse => chBusInuse,
		chBusGranted => chBusGranted,
		reset => reset
	);

init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
        -- code that executes only once                      
WAIT;                                                       
END PROCESS init;                                                                                                                        


 
always : PROCESS
	
	procedure stimArbitrer(
		constant chBusRequestStim: in 	std_logic_vector (CHANNELS-1 downto 0);
		constant chBusInuseStim	 : in	std_logic_vector (CHANNELS-1 downto 0);
		constant resetStim	 	 : in	std_logic
	) is
	begin
		chBusRequest <= chBusRequestStim;
		chBusInuse <= chBusInuseStim;
		reset <= resetStim;
	end procedure stimArbitrer;
  
	procedure stimAndChekArbitrer(
		-- Entradas de estimulo
		constant chBusRequestStim: in 	std_logic_vector (CHANNELS-1 downto 0);
		constant chBusInuseStim	 : in	std_logic_vector (CHANNELS-1 downto 0);
		constant resetStim	 	 : in	std_logic;
		-- Salidas esperadas
		constant chBusGrantedExpected	: in	std_logic_vector (CHANNELS-1 downto 0)
	) is 
	begin
		wait until rising_edge(usbClk);
		-- wait inputs hold time
		--wait for IN_MIN_HOLD_TIME;
		--stimulate inputs
		stimArbitrer(chBusRequestStim, chBusInuseStim, resetStim);
		--wait until output setup time to next clock
		wait for CLK_PERIOD-IN_MIN_HOLD_TIME-OUT_MIN_SETUP_TIME;
		--check expected output
		for I in 0 to CHANNELS-1 loop
			assert chBusGranted(I) = chBusGrantedExpected(I) 
			report
				"Unexpected result: " & "chBusGranted(" & integer'image(i) & 
				") = " & std_logic'image(chBusGranted(i)) &
				"; " & "expected = " & std_logic'image(chBusGrantedExpected(i))
			severity error;
		end loop;
	end procedure stimAndChekArbitrer;

	procedure waitForGranted(
		constant channel : in natural
	) is
	begin
		for I in 0 to (TIMEOUT+10)*(CHANNELS-1) loop
			if chBusGranted(channel) = '1' then
				exit;
			else
				wait until rising_edge(usbClk);
				wait for CLK_PERIOD-OUT_MIN_SETUP_TIME;
			end if;
		end loop;
		assert chBusGranted(channel) = '1' 
		report
			"Unexpected result: " & "chBusGranted(" & natural'image(channel) & 
			") = " & std_logic'image(chBusGranted(channel)) &
			"; " & "expected = " & std_logic'image('1')
		severity error;
	end procedure waitForGranted;
	
	procedure waitForRelease(
		constant channel : in natural
	) is
	begin
		for I in 0 to (TIMEOUT+10)*(CHANNELS-1) loop
			if chBusGranted(channel) = '0' then
				exit;
			else
				wait until rising_edge(usbClk);
				wait for CLK_PERIOD-OUT_MIN_SETUP_TIME;
			end if;
		end loop;
		assert chBusGranted(channel) = '0' 
		report
			"Unexpected result: " & "chBusGranted(" & natural'image(channel) & 
			") = " & std_logic'image(chBusGranted(channel)) &
			"; " & "expected = " & std_logic'image('0')
		severity error;
	end procedure waitForRelease;
	
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN
	-- Estado inicial
	-- (chBusRequestStim, chBusInuseStim, resetStim), --Inputs
	stimArbitrer(('0','0'), ('0','0'), '1');
	
	-- (chBusRequestStim, chBusInuseStim, resetStim), --Inputs
		--[chBusGranted] -- Outputs
	stimAndChekArbitrer(('0','0'), ('0','0'), '0',
		('0','0'));
	
	-- BusRequest start - BusInUse stop
	-- (chBusRequestStim, chBusInuseStim, resetStim), --Inputs
		--[chBusGranted] -- Outputs
	stimAndChekArbitrer(('0','1'), ('0','0'), '0',
		('0','0'));
	waitForGranted(0);
	stimAndChekArbitrer(('0','1'), ('0','1'), '0',
		('0','1'));
	stimAndChekArbitrer(('0','1'), ('0','1'), '0',
		('0','1'));
	stimAndChekArbitrer(('0','1'), ('0','1'), '0',
		('0','1'));
	stimAndChekArbitrer(('0','0'), ('0','0'), '0',
		('0','1'));
	stimAndChekArbitrer(('0','0'), ('0','0'), '0',
		('0','0'));
		
	-- Round-robin test
	stimAndChekArbitrer(('0','1'), ('0','0'), '0',
		('0','0'));
	waitForGranted(0);
	stimAndChekArbitrer(('1','1'), ('0','1'), '0',
		('0','1'));
	waitForRelease(0);
	stimAndChekArbitrer(('1','1'), ('0','0'), '0',
		('0','0'));
	waitForGranted(1);
	stimAndChekArbitrer(('1','1'), ('1','0'), '0',
		('1','0'));
	waitForRelease(1);
	stimAndChekArbitrer(('1','1'), ('0','0'), '0',
		('0','0'));
	waitForGranted(0);
	stimAndChekArbitrer(('1','1'), ('0','1'), '0',
		('0','1'));
	waitForRelease(0);
	stimAndChekArbitrer(('0','0'), ('0','0'), '0',
		('0','0'));
WAIT;                                                        
END PROCESS always;



stimMux: PROCESS (reset, usbClk)
	variable dVal : std_logic_vector (31 downto 0);
begin
	if (reset='1') then
		chUsbWRn <= ('0','1');
		chDoeStim <= "0000";
		chUsbDout <= (x"80808080",x"40404040");
		chBEoeStim <= '0';
		chUsbBEout <= ("0010", "1000");
		dStim <= (others =>'0');
		beStim <= (others => '0');
		usbWRn <= '0';
		usbRXFn <= '0';
		usbTXEn <= '0';
	elsif (rising_edge(usbClk)) then
		dStim <= dStim+1;
		beStim <= beStim+1;
		usbWRn <= not usbWRn;
		usbRXFn <= not usbRXFn;
		usbTXEn <= not usbTXEn;
		chDoeStim <= chDoeStim+1;
		chBEoeStim <= not chBEoeStim;
		for I in 0 to (CHANNELS-1) loop
			chUsbWRn(I) <= not chUsbWRn(I);
			chUsbDout(I) <= chUsbDout(I)+1;
			chUsbBEout(I) <= chUsbBEout(I)+1;
		end loop;	
	end if;
end process stimMux;

stimIOs: process (chUsbDoe, chBusInuse, chDoeStim, chBEoeStim, dStim, beStim)
	variable inUse : integer;
begin
	inUse := isInUse(chBusInuse);
	for I in 0 to (CHANNELS-1) loop
		if (inUse = I) then
			chUsbDoe(I) <= chDoeStim;
			chUsbBEoe(I) <= chBEoeStim;
		else 
			chUsbDoe(I) <= (others => '0');
			chUsbBEoe(I) <= '0';
		end if;
	end loop;
	if (inUse = -1) then
		usbD <= dStim;
		usbBE <= beStim;
	else 
		if (chUsbBEoe(inUse)='1') then
			usbBE <= (others => 'Z');
		else
			usbBE <= beStim;
		end if;
		for I in 0 to 3 loop
			if (chUsbDoe(inUse)(I)='1') then
				usbD((I+1)*8-1 downto I*8) <= (others => 'Z');
			else
				usbD((I+1)*8-1 downto I*8) <= dStim((I+1)*8-1 downto I*8);
			end if;
		end loop;
	end if;
end process stimIOs;




checkDrivers : PROCESS
begin 
	wait until rising_edge(usbClk);
	--wait until output setup time to next clock
	wait for CLK_PERIOD-OUT_MIN_SETUP_TIME;
	for I in 0 to (CHANNELS-1) loop
		assert chUsbDin(I) = usbD 
		report
			"Unexpected result: " & "chUsbDin(" & natural'image(I) & 
			") = " & integer'image(to_integer(unsigned(chUsbDin(I)))) &
			"; " & "expected = " & integer'image(to_integer(unsigned(usbD)))
		severity error;
		assert chUsbBEin(I) = usbBE 
		report
			"Unexpected result: " & "chUsbBEin(" & natural'image(I) & 
			") = " & integer'image(to_integer(unsigned(chUsbBEin(I)))) &
			"; " & "expected = " & integer'image(to_integer(unsigned(usbBE)))
		severity error;
		assert chUsbRXFn(I) = usbRXFn 
		report
			"Unexpected result: " & "chusbRXFn(" & natural'image(I) & 
			") = " & std_logic'image(chUsbRXFn(I)) &
			"; " & "expected = " & std_logic'image(usbRXFn)
		severity error;
		assert chUsbTXEn(I) = usbTXEn 
		report
			"Unexpected result: " & "chUsbTXEn(" & natural'image(I) & 
			") = " & std_logic'image(chUsbTXEn(I)) &
			"; " & "expected = " & std_logic'image(usbTXEn)
		severity error;
	end loop;
end process checkDrivers;

clk_gen : PROCESS
begin
	usbClk <= '1';
	-- slave_clk <= TRANSPORT usb_clk AFTER 10 ns;
	loop
		-- La F real es de 60Mhz pero corremos la simulaci�n
		-- 66Mhz para facilitar los calculos y ver que con un poco m�s anda
		wait for CLK_PERIOD/2;
		usbClk <= not usbClk;
	end loop;
end process clk_gen;


END usbWrite_arch;

