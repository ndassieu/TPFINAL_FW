add wave -noupdate -group WRITE -label i_wrclk /top_level_vhd_tst/wrclk
add wave -noupdate -group WRITE -label i_wrreq /top_level_vhd_tst/wrreq
add wave -noupdate -group WRITE -label o_wrfull /top_level_vhd_tst/wrfull
add wave -noupdate -group WRITE -label i_data -radix hexadecimal /top_level_vhd_tst/data

add wave -noupdate -group READ -label i_rdclk /top_level_vhd_tst/rdclk
add wave -noupdate -group READ -label i_rdreq /top_level_vhd_tst/rdreq
add wave -noupdate -group READ -label o_rdempty /top_level_vhd_tst/rdempty
add wave -noupdate -group READ -label o_q -radix hexadecimal /top_level_vhd_tst/q

add wave -noupdate -group RESET -label i_aclr /top_level_vhd_tst/aclr

