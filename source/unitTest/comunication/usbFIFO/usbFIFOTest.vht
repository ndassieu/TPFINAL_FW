-- Copyright (C) 1991-2011 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "12/03/2012 13:50:45"
                                                            
-- Vhdl Test Bench template for design  :  usbWrite
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;  
USE ieee.numeric_std.all;   
use IEEE.STD_LOGIC_UNSIGNED.ALL;                           

ENTITY top_level_vhd_tst IS
GENERIC 
	(
		-- Periodo del clock de lectura 
		constant RD_CLK_PERIOD : time := 10 ns;
		-- Periodo del clock de escritura
		constant WR_CLK_PERIOD : time := 25 ns;
		-- Inputs Min Hold Time 
		constant IN_MIN_HOLD_TIME : time := 3.5 ns;
		-- Outputs Min Setup Time
		constant OUT_MIN_SETUP_TIME : time := 1 ns;
		
		constant STIM_INC : std_logic_vector(33 downto 0) := "00" & x"04030201"
	);

END top_level_vhd_tst;
ARCHITECTURE usbFIFO_arch OF top_level_vhd_tst IS
-- constants                                                 
-- signals  
SIGNAL aclr		: STD_LOGIC  := '0';
SIGNAL data		: STD_LOGIC_VECTOR (33 DOWNTO 0);
SIGNAL rdclk	: STD_LOGIC ;
SIGNAL rdreq	: STD_LOGIC ;
SIGNAL wrclk	: STD_LOGIC ;
SIGNAL wrreq	: STD_LOGIC ;
SIGNAL q		: STD_LOGIC_VECTOR (33 DOWNTO 0);
SIGNAL rdempty	: STD_LOGIC ;
SIGNAL wrfull	: STD_LOGIC ;

SIGNAL wrData   : STD_LOGIC_VECTOR(33 downto 0);

SIGNAL rdData   : STD_LOGIC_VECTOR(33 downto 0);
	
--alias test is
-- <<signal .i1.pendingData : std_logic>>;
 
COMPONENT usbFIFO
	PORT (
		aclr		: IN STD_LOGIC  := '0';
		data		: IN STD_LOGIC_VECTOR (33 DOWNTO 0);
		rdclk		: IN STD_LOGIC ;
		rdreq		: IN STD_LOGIC ;
		wrclk		: IN STD_LOGIC ;
		wrreq		: IN STD_LOGIC ;
		q			: OUT STD_LOGIC_VECTOR (33 DOWNTO 0);
		rdempty		: OUT STD_LOGIC ;
		wrfull		: OUT STD_LOGIC 
	);
END COMPONENT;
BEGIN
	i1 : usbFIFO -- placa master
	PORT MAP (
-- list connections usbbeinbetween master ports and signals
	aclr => aclr,
	data => data,
	rdclk => rdclk,
	rdreq => rdreq,
	wrclk => wrclk,
	wrreq => wrreq,
	q => q,
	rdempty => rdempty,
	wrfull => wrfull
	);

init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
        -- code that executes only once                      
WAIT;                                                       
END PROCESS init;                                                                                                                        


 
always : PROCESS 
	procedure stim(
		constant wrreqStim 	: in	std_logic;
		constant dataStim 	: in	std_logic_vector (33 downto 0);
		constant rdreqStim 	: in	std_logic;
		constant aclrStim 	: in 	std_logic
	) is
	begin
		wrreq <= wrreqStim;
		data <= dataStim;
		rdreq <= rdreqStim;
		aclr <= aclrStim;
	end procedure stim;
     
	procedure stim_and_check_wr(
		-- Entradas de estimulo
		constant wrreqStim 	: in	std_logic;
		constant dataStim 	: in	std_logic_vector (33 downto 0);
		constant rdreqStim 	: in	std_logic;
		constant aclrStim 	: in 	std_logic;
		-- Salidas esperadas
		constant wrfullExpected	: in STD_LOGIC
	) is 
	begin
		wait until rising_edge(wrClk);
		-- wait inputs hold time
		--wait for IN_MIN_HOLD_TIME;
		--stimulate inputs
		stim(wrreqStim, dataStim, rdreqStim, aclrStim);
		--wait until output setup time to next clock
		wait for WR_CLK_PERIOD-IN_MIN_HOLD_TIME-OUT_MIN_SETUP_TIME;
		--check expected output
		assert wrfull = wrfullExpected 
		report
			"Unexpected result: " & "wrfull = " & std_logic'image(wrfull) &
			"; " & "expected = " & std_logic'image(wrfullExpected)
		severity error;
	end procedure stim_and_check_wr;
	
	procedure stim_and_check_rd(
		-- Entradas de estimulo
		constant wrreqStim 	: in	std_logic;
		constant dataStim 	: in	std_logic_vector (33 downto 0);
		constant rdreqStim 	: in	std_logic;
		constant aclrStim 	: in 	std_logic;
		-- Salidas esperadas
		constant rdemptyExpected	: in STD_LOGIC;
		constant qExpected 	: in	std_logic_vector (33 downto 0)
	) is 
	begin
		wait until rising_edge(rdClk);
		-- wait inputs hold time
		--wait for IN_MIN_HOLD_TIME;
		--stimulate inputs
		stim(wrreqStim, dataStim, rdreqStim, aclrStim);
		--wait until output setup time to next clock
		wait for RD_CLK_PERIOD-IN_MIN_HOLD_TIME-OUT_MIN_SETUP_TIME;
		--check expected output
		assert rdempty = rdemptyExpected 
		report
			"Unexpected result: " & "rdempty = " & std_logic'image(rdempty) &
			"; " & "expected = " & std_logic'image(rdempty)
		severity error;
		if (rdempty='0') then
			assert q(33 downto 0) = qExpected(33 downto 0) 
			report
				"Unexpected result: " & "q = " & integer'image(to_integer(unsigned(q(33 downto 0)))) &
				"; " & "Expected = " & integer'image(to_integer(unsigned(qExpected(33 downto 0))))
			severity error;
		end if;
	end procedure stim_and_check_rd;

	procedure waitForNotFull is
	begin
		for I in 0 to 10 loop
			if wrfull = '0' then
				exit;
			else
				wait until rising_edge(wrclk);
				wait for WR_CLK_PERIOD-OUT_MIN_SETUP_TIME;
			end if;
		end loop;
		assert wrfull = '0' 
		report
			"Unexpected result: " & "wrfull  = " & std_logic'image(wrfull ) &
			"; " & "Expected = " & std_logic'image('0')
		severity error;
	end procedure waitForNotFull;
	
	procedure waitNotEmpty is
	begin
		for I in 0 to 10 loop
			if rdEmpty = '0' then
				exit;
			else
				wait until rising_edge(rdclk);
				wait for RD_CLK_PERIOD-OUT_MIN_SETUP_TIME;
			end if;
		end loop;
		assert rdEmpty = '0' 
		report
			"Unexpected result: " & "rdEmpty  = " & std_logic'image(rdEmpty ) &
			"; " & "Expected = " & std_logic'image('0')
		severity error;
	end procedure waitNotEmpty;
	
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN

	wrData <= STIM_INC;
	rdData <= STIM_INC;

	-- Estado inicial
	-- (wrreq, data, rdreq, aclr), --Inputs
	stim('0', (others => '0'), '0', '0');
	
	-- Esperamos a que se libere el FIFO wrFull para empezar a escrbir
	waitForNotFull;

	-- (wrreq, data, rdreq, aclr), --Inputs
		--[wrfull] -- expected outputs
	for I in 0 to 255 loop
		stim_and_check_wr('1', wrData, '0', '0',
			'0');
		wrData<=wrData+STIM_INC;
	end loop;
	-- Testeamos que no se rompa la fifo por escribirla en full
	for I in 0 to 10 loop
		stim_and_check_wr('1', wrData, '0', '0',
				'1');
		wrData<=wrData+STIM_INC;
	end loop;
	stim_and_check_wr('0', wrData, '0', '0',
			'1');
		
	waitNotEmpty;
	
	-- (wrreq, data, rdreq, aclr), --Inputs
		--[rdempty, q] -- expected outputs
	stim_and_check_rd('0', wrData, '0', '0',
			'0',rdData);
	for I in 0 to 255 loop
		stim_and_check_rd('0', wrData, '1', '0',
				'0',rdData);
		rdData<=rdData+STIM_INC;
	end loop;
	-- Seguimos leyendo para ver que la fifo no se corrompa
	for I in 0 to 10 loop
		stim_and_check_rd('0', wrData, '1', '0',
				'1',rdData);
		rdData<=rdData+STIM_INC;
	end loop;
	stim_and_check_rd('0', wrData, '0', '0',
				'1',rdData);
	stim_and_check_wr('0', wrData, '0', '0',
				'0');

	-- ecribimos un dato m�s 
	stim_and_check_wr('1', wrData, '0', '0',
				'0');
	wrData<=wrData+STIM_INC;
	stim_and_check_wr('0', wrData, '0', '0',
				'0');
				
	-- leemos un dato m�s
	waitNotEmpty;
	stim_and_check_rd('0', wrData, '1', '0',
				'0',rdData);
	rdData<=rdData+STIM_INC;
	
	stim_and_check_rd('0', wrData, '0', '0',
				'1',rdData);
	stim_and_check_wr('0', wrData, '0', '0',
				'0');
	
WAIT;                                                        
END PROCESS always;

wr_clk_gen : PROCESS
begin
	wrclk <= '1';
	-- slave_clk <= TRANSPORT usb_clk AFTER 10 ns;
	loop
		-- La F real es de 60Mhz pero corremos la simulaci�n
		-- 66Mhz para facilitar los calculos y ver que con un poco m�s anda
		wait for WR_CLK_PERIOD/2;
		wrclk <= not wrclk;
	end loop;
end process wr_clk_gen;

rd_clk_gen : PROCESS
begin
	rdclk <= '1';
	-- slave_clk <= TRANSPORT usb_clk AFTER 10 ns;
	loop
		-- La F real es de 60Mhz pero corremos la simulaci�n
		-- 66Mhz para facilitar los calculos y ver que con un poco m�s anda
		wait for RD_CLK_PERIOD/2;
		rdclk <= not rdclk;
	end loop;
end process rd_clk_gen;

END usbFIFO_arch;

