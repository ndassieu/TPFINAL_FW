Este bloque es el encargado de sacar los datos de la FIFO de transmisión hacia 
la PC y escribirlos en el FT601.  
[Ver diseño detallado de este bloque](https://gitlab.com/ndassieu/MSE_TPFINAL_FW/wikis/firmware/diseño/detallado/communication/usbwrite.vhd)