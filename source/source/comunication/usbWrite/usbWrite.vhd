-- Este modulo es el encargado de enviar los datos de la FIFO de
-- transmisi�n y colocarlos en el buffer de salida del FT601
-- para descripci�n sobre los estados consultar el archivo readme.md


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity usbWrite is
	GENERIC 
	(
		-- Numero de canal que maneja el bloque. Rango [1:4]
		CH: INTEGER range 4 downto 1 := 1
	);
	
	port
	(
		-- Se�ales de bus del FT601 
		usbClk	 	: in		std_logic;
		usbWRn 	 	: out		std_logic;
		usbRXn	 	: in 		std_logic;
		usbDoe 	 	: out 		std_logic_vector (3 downto 0);
		usbDout  	: out		std_logic_vector (31 downto 0);
		usbDin	 	: in		std_logic_vector (7 downto 0);
		usbBEoe	 	: out		std_logic;
		usbBEout 	: out		std_logic_vector (3 downto 0);
		usbTXEn	 	: in		std_logic;
		-- se�ales de manejo de la FIFO
		fifoRdreq	: buffer	std_logic;
		fifoRdempty	: in		std_logic;
		fifoQ		: in		std_logic_vector (33 downto 0);
		-- se�ales del arbitro de bus
		busRequest	: out		std_logic;
		busInUse	: out		std_logic;
		busGranted	: in		std_logic;
		-- se�al de reset
		reset	 	: in		std_logic
	);
	
end entity;

architecture rtl of usbWrite is

	-- Build an enumerated type for the state machine
	type state_type is (sIdle, sWaiting, sStart, sRdStart, sPutLast, sSending, sSopt, sHold, sFTfull);
	
	-- Register to hold the current state
	signal state : state_type;
	
	-- Attribute "safe" implements a safe state machine.
	-- This is a state machine that can recover from an
	-- illegal state (by returning to the reset state).
	attribute syn_encoding : string;
	attribute syn_encoding of state_type : type is "safe";

	-- Se�ales internas
	signal lastFifoQ 	: std_logic_vector (33 downto 0);
	signal decodeBE		: std_logic_vector (3 downto 0);
	signal pendingData	: std_logic;
begin
	-- Guarda ultimo valor leido de la FIFO
	process (usbClk, reset)
	begin
		if reset = '1' then
			lastFifoQ(33 downto 0) <=  (others => '0');
		elsif rising_edge(usbClk) then
			if fifoRdreq = '1' then
				lastFifoQ(33 downto 0) <= fifoQ;
			end if;
		end if;
	end process;

	-- Maneja el decodificador de la se�al de BE
	process (lastFifoQ)
	begin
		case lastFifoQ(33 downto 32) is
			when "00"=>
				decodeBE(3 downto 0) <= "0001";
			when "01"=>
				decodeBE(3 downto 0) <= "0011";
			when "10"=>
				decodeBE(3 downto 0) <= "0111";
			when "11"=>
				decodeBE(3 downto 0) <= "1111";
			when others=>
				decodeBE(3 downto 0) <= "0000";
		end case;
	end process;

	-- Maquina de estados
	process (usbClk, reset)
	begin
		if reset = '1' then
			state <= sIdle;
			pendingData <= '0';
		elsif (rising_edge(usbClk)) then
			-- Determine the next state synchronously, based on
			-- the current state and the input
			case state is
				when sIdle =>
					if (usbTXEn='0') and (usbDin(CH-1)='0') and
							((fifoRdempty='0') or (pendingData='1')) and
							(busGranted='0') then
						state <= sWaiting;
					elsif (usbTXEn='0') and (usbDin(CH-1)='0') and
							((fifoRdempty='0') or (pendingData='1')) and 
							(busGranted='1') then
						state <= sStart;
					end if;
				when sWaiting =>
					if (busGranted='1') then
						state <= sStart;
					end if;
				when sStart =>
					if (pendingData='0') then
						state <= sRdStart;
					else
						state <= sPutLast;
					end if;
				when sRdStart =>
					state <= sSending;
				when sPutLast =>
					state <= sSending;
				when sSending =>
					if (usbRXn='1') then
						state <= sFTfull;
					elsif (fifoRdempty='1') or (busGranted='0') then
						state <= sSopt;
					end if;
				when sSopt =>
					pendingData <= '0';
					state  <= sHold;
				when sHold =>
					state  <= sIdle;
				when sFTfull =>
					pendingData <= '1';
					state  <= sIdle;
				when others =>
					state  <= sIdle;
			end case;
			
		end if;
	end process;
	
	-- Determine the output based only on the current state
	-- and the input (do not wait for a clock edge).
	process (state, usbDin, usbRXn, decodeBE, lastFifoQ, fifoRdEmpty, busGranted, usbTXEn)
	begin
		case state is
			when sIdle=>
				usbWRn <= '1';
				usbDoe(3 downto 0) <=  x"0";
				usbDout(31 downto 0) <= (others => '0');
				usbBEoe <= '0';
				usbBEout(3 downto 0) <=  x"0";
				fifoRdreq <= '0';
				busRequest <= (not usbDin(CH-1)) and (not usbTXEn) and (not fifoRdEmpty);
				busInUse <= '0';
			when sWaiting=>
				usbWRn <= '1';
				usbDoe(3 downto 0) <=  x"0";
				usbDout(31 downto 0) <= (others => '0');
				usbBEoe <= '0';
				usbBEout(3 downto 0) <=  x"0";
				fifoRdreq <= '0';
				busRequest <= '1';
				busInUse <= '0';
			when sStart=>
				usbWRn <= '0';
				usbDoe(3 downto 0) <=  x"1";
				usbDout(31 downto 8) <= (others => '0');
				usbDout(7 downto 0) <= std_logic_vector(to_unsigned(CH,8));
				usbBEoe <= '1';
				usbBEout(3 downto 0) <=  x"1";
				fifoRdreq <= '0';
				busRequest <= '1';
				busInUse <= '1';
			when sRdStart=>
				usbWRn <= '0';
				usbDoe(3 downto 0) <=  x"1";
				usbDout(31 downto 8) <= (others => '0');
				usbDout(7 downto 0) <= std_logic_vector(to_unsigned(CH,8));
				usbBEoe <= '1';
				usbBEout(3 downto 0) <=  x"1";
				fifoRdreq <= '1';
				busRequest <= '1';
				busInUse <= '1';
			when sPutLast=>
				usbWRn <= '0';
				usbDoe(3 downto 0) <=  x"1";
				usbDout(31 downto 8) <= (others => '0');
				usbDout(7 downto 0) <= std_logic_vector(to_unsigned(CH,8));
				usbBEoe <= '1';
				usbBEout(3 downto 0) <=  x"1";
				fifoRdreq <= '0';
				busRequest <= '1';
				busInUse <= '1';
			when sSending=>
				usbWRn <= '0';
				if (usbRXn='0') then
					usbDoe(3 downto 0) <=  x"F";
				else 
					usbDoe(3 downto 0) <=  x"0";
				end if;
				usbDout(31 downto 0) <= lastFifoQ(31 downto 0);
				usbBEoe <= '1' and (not usbRXn);
				usbBEout(3 downto 0) <=  decodeBE(3 downto 0);
				fifoRdreq <= (not usbRXn) and (busGranted);
				busRequest <= '1';
				busInUse <= '1';
			when sSopt =>
				usbWRn <= '1';
				usbDoe(3 downto 0) <=  x"0";
				usbDout(31 downto 0) <= (others => '0');
				usbBEoe <= '0';
				usbBEout(3 downto 0) <=  (others => '0');
				fifoRdreq <= '0';
				busRequest <= '1';
				busInUse <= '1';
			when sHold | sFTfull =>
				usbWRn <= '1';
				usbDoe(3 downto 0) <=  x"0";
				usbDout(31 downto 0) <= (others => '0');
				usbBEoe <= '0';
				usbBEout(3 downto 0) <=  (others => '0');
				fifoRdreq <= '0';
				busRequest <= '0';
				busInUse <= '0';
		end case;
	end process;
	
end rtl;
