library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work; 
use work.communicationArray.all;
use work.communicationStdLogicArray.all;

entity communication is
	GENERIC 
	(
		-- Numero total de canales que maneja el bloque 
		CHANNELS: INTEGER range 2 downto 0 := 0;
		-- Tiempo para correr la politica de Round-robin
		ROUND_ROBIN_COUNTS: INTEGER := 1024
	);
	
	port
	(
		-- Se�ales de bus del FT601 a arbitrar
		usbClk	 	: in		std_logic;
		usbWRn 	 	: out		std_logic;
		usbRXFn	 	: in 		std_logic;
		usbD	  		: inout		std_logic_vector (31 downto 0);
		usbBE 		: inout		std_logic_vector (3 downto 0);
		usbTXEn	 	: in		std_logic;
		usbOEn	 	: out		std_logic;
		usbRDn	 	: out		std_logic;
		usbSIWUn	 	: out		std_logic;
		-- Se�ales de las fifo's de transmisi�n
		txFifoWrclk	: in	std_logic_vector (2**CHANNELS-1 downto 0);
		txFifoWrreq	: in	std_logic_vector (2**CHANNELS-1 downto 0);
		txFifoWrfull: out	std_logic_vector (2**CHANNELS-1 downto 0);
		txFifoData	: in	comm_array_of_std_logic_vector_34(2**CHANNELS-1 downto 0);
		-- Se�ales de las fifo's de recepcici�n
		rxFifoRdclk	: in	std_logic_vector (2**CHANNELS-1 downto 0);
		rxFifoRdreq	: in	std_logic_vector (2**CHANNELS-1 downto 0);
		rxFifoRdempty: out	std_logic_vector (2**CHANNELS-1 downto 0);
		rxFifoQ		: out	comm_array_of_std_logic_vector_34(2**CHANNELS-1 downto 0);
		-- se�al de reset
		reset	 	: in		std_logic
	);
	
end entity;

architecture rtl of communication is
	
	signal chUsbWRn		: std_logic_vector (2**(CHANNELS+1)-1 downto 0);
	signal chUsbRXFn	: std_logic_vector (2**(CHANNELS+1)-1 downto 0);
	signal chUsbDoe		: array_of_std_logic_vector_4(2**(CHANNELS+1)-1 downto 0);
	signal chUsbDout	: array_of_std_logic_vector_32(2**(CHANNELS+1)-1 downto 0);
	signal chUsbDin		: array_of_std_logic_vector_32(2**(CHANNELS+1)-1 downto 0);
	signal chUsbBEoe	: std_logic_vector (2**(CHANNELS+1)-1 downto 0);
	signal chUsbBEout	: array_of_std_logic_vector_4(2**(CHANNELS+1)-1 downto 0);
	signal chUsbBEin	: array_of_std_logic_vector_4(2**(CHANNELS+1)-1 downto 0);
	signal chUsbTXEn	: std_logic_vector (2**(CHANNELS+1)-1 downto 0);			
	signal chBusRequest	: std_logic_vector (2**(CHANNELS+1)-1 downto 0);
	signal chBusInuse  	: std_logic_vector (2**(CHANNELS+1)-1 downto 0);
	signal chBusGranted	: std_logic_vector (2**(CHANNELS+1)-1 downto 0);
	
	signal txChFifoRdreq : std_logic_vector (2**CHANNELS-1 downto 0);
	signal txChFifoRdempty : std_logic_vector (2**CHANNELS-1 downto 0);
	signal txChFifoQ 	: comm_array_of_std_logic_vector_34(2**CHANNELS-1 downto 0);
	
	signal rxChFifoWrreq	: std_logic_vector (2**CHANNELS-1 downto 0);
	signal rxChFifoWrfull	: std_logic_vector (2**CHANNELS-1 downto 0);
	signal rxChFifoD		: comm_array_of_std_logic_vector_34(2**CHANNELS-1 downto 0);
	
	signal usbResetStage1 : std_logic := '1';
	signal usbResetStage2 : std_logic := '1';
	signal usbReset : std_logic := '1';
	
	component usbArbitrer is
		GENERIC 
		(
			-- Numero total de canales que maneja el bloque 
			CHANNELS: INTEGER := 2;
			-- Tiempo para correr la politica de Round-robin
			TIMEOUT: INTEGER := 1024
		);
		port
		(
			-- Se�ales de bus del FT601 a arbitrar
			usbClk	 	: in		std_logic;
			usbWRn 	 	: out		std_logic;
			usbRXFn	 	: in 		std_logic;
			usbD	  	: inout		std_logic_vector (31 downto 0);
			usbBE 		: inout		std_logic_vector (3 downto 0);
			usbTXEn	 	: in		std_logic;
			-- Se�ales de los canales a arbitrar
			chUsbWRn	: in	std_logic_vector (CHANNELS-1 downto 0);
			chUsbRXFn	: out	std_logic_vector (CHANNELS-1 downto 0);
			chUsbDoe	: in	array_of_std_logic_vector_4(CHANNELS-1 downto 0);
			chUsbDout	: in	array_of_std_logic_vector_32(CHANNELS-1 downto 0);
			chUsbDin	: out	array_of_std_logic_vector_32(CHANNELS-1 downto 0);
			chUsbBEoe	: in	std_logic_vector (CHANNELS-1 downto 0);
			chUsbBEout	: in	array_of_std_logic_vector_4(CHANNELS-1 downto 0);
			chUsbBEin	: out	array_of_std_logic_vector_4(CHANNELS-1 downto 0);
			chUsbTXEn	: out	std_logic_vector (CHANNELS-1 downto 0);			
			-- Se�ales de control de los canales
			chBusRequest: in	std_logic_vector (CHANNELS-1 downto 0);
			chBusInuse  : in	std_logic_vector (CHANNELS-1 downto 0);
			chBusGranted: out	std_logic_vector (CHANNELS-1 downto 0);
			-- se�al de reset
			reset	 	: in		std_logic
		);
	end component;
	
	component usbWrite is
	GENERIC 
		(
			-- Numero de canal que maneja el bloque. Rango [1:4]
			CH: INTEGER range 4 downto 1 := 1
		);
		port
		(
			-- Se�ales de bus del FT601 
			usbClk	 	: in		std_logic;
			usbWRn 	 	: out		std_logic;
			usbRXn	 	: in 		std_logic;
			usbDoe 	 	: out 		std_logic_vector (3 downto 0);
			usbDout  	: out		std_logic_vector (31 downto 0);
			usbDin	 	: in		std_logic_vector (7 downto 0);
			usbBEoe	 	: out		std_logic;
			usbBEout 	: out		std_logic_vector (3 downto 0);
			usbTXEn	 	: in		std_logic;
			-- se�ales de manejo de la FIFO
			fifoRdreq	: buffer	std_logic;
			fifoRdempty	: in		std_logic;
			fifoQ		: in		std_logic_vector (33 downto 0);
			-- se�ales del arbitro de bus
			busRequest	: out		std_logic;
			busInUse	: out		std_logic;
			busGranted	: in		std_logic;
			-- se�al de reset
			reset	 	: in		std_logic
		);
	end component;
	
	component usbRead is
		GENERIC 
		(
			-- Numero de canal que maneja el bloque. Rango [1:4]
			CH: INTEGER range 4 downto 1 := 1
		);
		
		port
		(
			-- Se�ales de bus del FT601 
			usbClk	 	: in		std_logic;
			usbWRn 	 	: out		std_logic;
			usbRXn	 	: in 		std_logic;
			usbDoe 	 	: out 		std_logic_vector (3 downto 0);
			usbDout  	: out		std_logic_vector (31 downto 0);
			usbDin	 	: in		std_logic_vector (31 downto 0);
			usbBEoe	 	: out		std_logic;
			usbBEout 	: out		std_logic_vector (3 downto 0);
			usbBEin 	: in		std_logic_vector (3 downto 0);
			usbTXEn	 	: in		std_logic;
			-- se�ales de manejo de la FIFO
			fifoWrreq	: buffer	std_logic;
			fifoWrfull	: in		std_logic;
			fifoD		: out		std_logic_vector (33 downto 0);
			-- se�ales del arbitro de bus
			busRequest	: out		std_logic;
			busInUse	: out		std_logic;
			busGranted	: in		std_logic;
			-- se�al de reset
			reset	 	: in		std_logic
		);
		
	end component;
	
	component usbFIFO IS
		PORT
		(
			aclr		: IN STD_LOGIC  := '0';
			data		: IN STD_LOGIC_VECTOR (33 DOWNTO 0);
			rdclk		: IN STD_LOGIC ;
			rdreq		: IN STD_LOGIC ;
			wrclk		: IN STD_LOGIC ;
			wrreq		: IN STD_LOGIC ;
			q			: OUT STD_LOGIC_VECTOR (33 DOWNTO 0);
			rdempty		: OUT STD_LOGIC ;
			wrfull		: OUT STD_LOGIC 
		);
	END component;

begin
	usbOEn	<= '1';
	usbRDn	<= '1';
	usbSIWUn	<= '1';
		
	-- Syncroniza se�al de reset de la FPGA con el FT601
	process (usbClk)
	begin
		if (rising_edge(usbClk)) then
			usbResetStage1 <= reset;
			usbResetStage2 <= usbResetStage1;
			usbReset <= usbResetStage2;
		end if;
	end process;
	
	-- Instanciamos el arbitro de bus
	comUsbArbitrer : usbArbitrer 
	GENERIC MAP (
		CHANNELS => 2**(CHANNELS+1),
		TIMEOUT => ROUND_ROBIN_COUNTS
	)
	PORT MAP (
		usbClk => usbClk,
		usbWRn => usbWRn,
		usbRXFn => usbRXFn,
		usbD => usbD,
		usbBE => usbBE,
		usbTXEn => usbTXEn,		
		chUsbWRn => chUsbWRn,
		chUsbRXFn => chUsbRXFn,
		chUsbDoe => chUsbDoe,
		chUsbDout => chUsbDout,
		chUsbDin => chUsbDin,
		chUsbBEoe => chUsbBEoe,
		chUsbBEout => chUsbBEout,
		chUsbBEin => chUsbBEin,
		chUsbTXEn => chUsbTXEn,		
		chBusRequest => chBusRequest,
		chBusInuse => chBusInuse,
		chBusGranted => chBusGranted,		
		reset => usbReset
	);
	
	-- Instanciamos los bloques usbWrite
	comUsbWrite: 
	for I in 0 to 2**CHANNELS-1 generate
		comUsbWrite : usbWrite 
		generic map (
			CH => I+1
		)
		port map
		(
			usbClk => usbClk,
			usbWRn => chUsbWRn(I),
			usbRXn => chUsbRXFn(I),
			usbDoe => chUsbDoe(I),
			usbDout => chUsbDout(I),
			usbDin => chUsbDin(I)(15 downto 8),
			usbBEoe => chUsbBEoe(I),
			usbBEout => chUsbBEout(I),
			usbTXEn => chUsbTXEn(I),
			fifoRdreq => txChFifoRdreq(I),
			fifoRdempty => txChFifoRdempty(I),
			fifoQ => txChFifoQ(I),
			busRequest => chBusRequest(I),
			busInUse => chBusInuse(I),
			busGranted => chBusGranted(I),
			reset => usbReset
		);
	end generate comUsbWrite;
	
	-- Instanciamos las fifos de transmisi�n
	comTxUsbFIFO: 
	for I in 0 to 2**CHANNELS-1 generate
		comTxUsbFIFO : usbFIFO 
		port map
		(
			wrclk => txFifoWrclk(I),
			wrreq => txFifoWrreq(I),
			wrfull => txFifoWrfull(I),
			data => txFifoData(I),
			rdclk => usbClk,
			rdreq => txChFifoRdreq(I),
			rdempty => txChFifoRdempty(I),
			q => txChFifoQ(I),
			aclr => reset 
		);
	end generate comTxUsbFIFO;

	-- Instanciamos los bloques usbRead
	comUsbRead: 
	for I in 0 to 2**CHANNELS-1 generate
		comUsbRead : usbRead 
		generic map (
			CH => I+1
		)
		port map
		(
			usbClk => usbClk,
			usbWRn => chUsbWRn(I+2**CHANNELS),
			usbRXn => chUsbRXFn(I+2**CHANNELS),
			usbDoe => chUsbDoe(I+2**CHANNELS),
			usbDout => chUsbDout(I+2**CHANNELS),
			usbDin => chUsbDin(I+2**CHANNELS),
			usbBEoe => chUsbBEoe(I+2**CHANNELS),
			usbBEout => chUsbBEout(I+2**CHANNELS),
			usbBEin => chUsbBEin(I+2**CHANNELS),
			usbTXEn => chUsbTXEn(I+2**CHANNELS),
			fifoWrreq => rxChFifoWrreq(I),
			fifoWrfull => rxChFifoWrfull(I),
			fifoD => rxChFifoD(I) ,
			busRequest => chBusRequest(I+2**CHANNELS),
			busInUse => chBusInuse(I+2**CHANNELS),
			busGranted => chBusGranted(I+2**CHANNELS),
			reset => usbReset
		);
	end generate comUsbRead;

	-- Instanciamos las fifos de recepci�n
	comRxUsbFIFO: 
	for I in 0 to 2**CHANNELS-1 generate
		comRxUsbFIFO : usbFIFO 
		port map
		(
			wrclk => usbClk,
			wrreq => rxChFifoWrreq(I),
			wrfull => rxChFifoWrfull(I),
			data => rxChFifoD(I),
			rdclk => rxFifoRdclk(I),
			rdreq => rxFifoRdreq(I),
			rdempty => rxFifoRdempty(I),
			q => rxFifoQ(I),
			aclr => usbReset
		);
	end generate comRxUsbFIFO;
end rtl;