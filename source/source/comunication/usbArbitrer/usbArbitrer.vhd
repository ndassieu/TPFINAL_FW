-- Este modulo es el encargado de recibir los datos del FT601
-- y colocarlos en la FIFO de recepci�n.
-- para descripci�n sobre los estados consultar el archivo readme.md


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work; 
use work.communicationStdLogicArray.all;

entity usbArbitrer is
	GENERIC 
	(
		-- Numero total de canales que maneja el bloque 
		CHANNELS: INTEGER := 2;
		-- Tiempo para correr la politica de Round-robin
		TIMEOUT: INTEGER := 1024
	);
	
	port
	(
		-- Se�ales de bus del FT601 a arbitrar
		usbClk	 	: in		std_logic;
		usbWRn 	 	: out		std_logic;
		usbRXFn	 	: in 		std_logic;
		usbD	  	: inout		std_logic_vector (31 downto 0);
		usbBE 		: inout		std_logic_vector (3 downto 0);
		usbTXEn	 	: in		std_logic;
		-- Se�ales de los canales a arbitrar
		chUsbWRn	: in	std_logic_vector (CHANNELS-1 downto 0);
		chUsbRXFn	: out	std_logic_vector (CHANNELS-1 downto 0);
		chUsbDoe	: in	array_of_std_logic_vector_4(CHANNELS-1 downto 0);
		chUsbDout	: in	array_of_std_logic_vector_32(CHANNELS-1 downto 0);
		chUsbDin	: out	array_of_std_logic_vector_32(CHANNELS-1 downto 0);
		chUsbBEoe	: in	std_logic_vector (CHANNELS-1 downto 0);
		chUsbBEout	: in	array_of_std_logic_vector_4(CHANNELS-1 downto 0);
		chUsbBEin	: out	array_of_std_logic_vector_4(CHANNELS-1 downto 0);
		chUsbTXEn	: out	std_logic_vector (CHANNELS-1 downto 0);			
		-- Se�ales de control de los canales
		chBusRequest: in	std_logic_vector (CHANNELS-1 downto 0);
		chBusInuse  : in	std_logic_vector (CHANNELS-1 downto 0);
		chBusGranted: out	std_logic_vector (CHANNELS-1 downto 0);
		-- se�al de reset
		reset	 	: in		std_logic
	);
	
end entity;

architecture rtl of usbArbitrer is

	-- Build an enumerated type for the state machine
	type state_type is (sReleased, sGranting, sGranted, sReleasing);
	
	-- Register to hold the current state
	signal state : state_type;
	
	-- Attribute "safe" implements a safe state machine.
	-- This is a state machine that can recover from an
	-- illegal state (by returning to the reset state).
	attribute syn_encoding : string;
	attribute syn_encoding of state_type : type is "safe";

	-- Se�ales internas
	signal grantedChannel : integer range 0 to CHANNELS-1 :=0;
	signal nextChannel	  : integer range 0 to CHANNELS := 0;
	signal elapsedTime	  : integer range 0 to TIMEOUT+1:=0;
	signal usbDoe 		  : std_logic_vector (3 downto 0);
	signal usbDout 		  : std_logic_vector (31 downto 0);
	signal usbBEoe 		  : std_logic;
	signal usbBEout		  : std_logic_vector (3 downto 0);
begin
	-- Maneja el mux de las se�ales de salida
	usbWRn <= chUsbWRn(grantedChannel);
	usbDoe <= chUsbDoe(grantedChannel);
	usbDout <= chUsbDout(grantedChannel);
	usbBEoe <= chUsbBEoe(grantedChannel);
	usbBEout <= chUsbBEout(grantedChannel);

	-- Maneja la direcci�n de los puertos D y BE y que poner en ellos.
	usbD(31 downto 24) <= usbDout(31 downto 24) when (usbDoe(3) = '1') else (others=>'Z');
	usbD(23 downto 16) <= usbDout(23 downto 16) when (usbDoe(2) = '1') else (others=>'Z');
	usbD(15 downto 8) <= usbDout(15 downto 8) when (usbDoe(1) = '1') else (others=>'Z');
	usbD(7 downto 0) <= usbDout(7 downto 0) when (usbDoe(0) = '1') else (others=>'Z');
	usbBE(3 downto 0) <= usbBEout(3 downto 0) when (usbBEoe = '1') else (others=>'Z');
	
	-- Driver de las se�ales
	driver: FOR i IN CHANNELS-1 DOWNTO 0 GENERATE
		chUsbRXFn(i) <= usbRXFn;
		chUsbDin(i) <= usbD;
		chUsbBEin(i) <= usbBE;
		chUsbTXEn(i) <= usbTXEn;
	END GENERATE driver; 	

	
	-- Maquina de estados
	process (usbClk, reset)
	begin
		if reset = '1' then
			state <= sReleased;
			grantedChannel <= 0;
			nextChannel <= 0;
			elapsedTime <= 0;
		elsif (rising_edge(usbClk)) then
			-- Determine the next state synchronously, based on
			-- the current state and the input
			case state is
				when sReleased =>
					elapsedTime <= 0;
					if (chBusRequest(nextChannel) = '1') then
						state <= sGranting;
						grantedChannel <= nextChannel;
					end if;
					if (nextChannel = (CHANNELS-1)) then
						nextChannel <= 0;
					else 
						nextChannel <= nextChannel+1;
					end if;
				when sGranting =>
					state <= sGranted;
				when sGranted =>
					elapsedTime <= elapsedTime+1;
					if (elapsedTime>=TIMEOUT) or 
							(chBusInUse(grantedChannel)='0') then
						state <= sReleasing;
					end if;
				when sReleasing =>
					if (chBusInUse(grantedChannel)='0') then
						state  <= sReleased;
					end if;
				when others =>
					state  <= sReleased;
			end case;
			
		end if;
	end process;
	
	-- Determine the output based only on the current state
	-- and the input (do not wait for a clock edge).
	process (grantedChannel, state)
	begin
		chBusGranted <= (others => '0');
		case state is
			when sReleased =>
			when sGranting | sGranted=>
				chBusGranted(grantedChannel) <= '1';
			when sReleasing=>
		end case;
	end process;
	
end rtl;
