-- Este modulo es el encargado de recibir los datos del FT601
-- y colocarlos en la FIFO de recepci�n.
-- para descripci�n sobre los estados consultar el archivo readme.md


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity usbRead is
	GENERIC 
	(
		-- Numero de canal que maneja el bloque. Rango [1:4]
		CH: INTEGER range 4 downto 1 := 1
	);
	
	port
	(
		-- Se�ales de bus del FT601 
		usbClk	 	: in		std_logic;
		usbWRn 	 	: out		std_logic;
		usbRXn	 	: in 		std_logic;
		usbDoe 	 	: out 		std_logic_vector (3 downto 0);
		usbDout  	: out		std_logic_vector (31 downto 0);
		usbDin	 	: in		std_logic_vector (31 downto 0);
		usbBEoe	 	: out		std_logic;
		usbBEout 	: out		std_logic_vector (3 downto 0);
		usbBEin 	: in		std_logic_vector (3 downto 0);
		usbTXEn	 	: in		std_logic;
		-- se�ales de manejo de la FIFO
		fifoWrreq	: buffer	std_logic;
		fifoWrfull	: in		std_logic;
		fifoD		: out		std_logic_vector (33 downto 0);
		-- se�ales del arbitro de bus
		busRequest	: out		std_logic;
		busInUse	: out		std_logic;
		busGranted	: in		std_logic;
		-- se�al de reset
		reset	 	: in		std_logic
	);
	
end entity;

architecture rtl of usbRead is

	-- Build an enumerated type for the state machine
	type state_type is (sIdle, sWaitbus, sStart, sWait1, sWait2, sReceiving, sSopt);
	
	-- Register to hold the current state
	signal state : state_type;
	
	-- Attribute "safe" implements a safe state machine.
	-- This is a state machine that can recover from an
	-- illegal state (by returning to the reset state).
	attribute syn_encoding : string;
	attribute syn_encoding of state_type : type is "safe";

	-- Se�ales internas
	signal encodeBE		: std_logic_vector (1 downto 0);
begin
	-- Maneja el codificador de la se�al de BE
	process (usbBEin)
	begin
		case usbBEin(3 downto 0) is
			when "0001" =>
				encodeBE(1 downto 0) <= "00";
			when "0011" =>
				encodeBE(1 downto 0) <= "01";
			when "0111" =>
				encodeBE(1 downto 0) <= "10";
			when others =>
				encodeBE(1 downto 0) <= "11";
		end case;
	end process;

	-- Maquina de estados
	process (usbClk, reset)
	begin
		if reset = '1' then
			state <= sIdle;
		elsif (rising_edge(usbClk)) then
			-- Determine the next state synchronously, based on
			-- the current state and the input
			case state is
				when sIdle =>
					if (usbTXEn='0') and (usbDin(CH+11)='0') and
							(fifoWrfull='0') and (busGranted='0') then
						state <= sWaitbus;
					elsif (usbTXEn='0') and (usbDin(CH+11)='0') and
							(fifoWrfull='0') and (busGranted='1') then
						state <= sStart;
					end if;
				when sWaitbus =>
					if (busGranted='1') then
						state <= sStart;
					end if;
				when sStart =>
					state <= sWait1;
				when sWait1 =>
					state <= sWait2;
				when sWait2 =>
					state <= sReceiving;
				when sReceiving =>
					if (usbRXn='1') or (fifoWrfull ='1') or
							(busGranted='0') then
						state <= sSopt;
					end if;
				when sSopt =>
					state  <= sIdle;
				when others =>
					state  <= sIdle;
			end case;
			
		end if;
	end process;
	
	-- Determine the output based only on the current state
	-- and the input (do not wait for a clock edge).
	process (state, usbDin, usbRXn, encodeBE, fifoWrfull, busGranted, usbTXEn)
	begin
		fifoD(33 downto 32) <= encodeBE(1 downto 0);
		fifoD(31 downto 0) <= usbDin;
		usbBEout(3 downto 0) <=  x"0";
		usbDout(31 downto 8) <= (others => '0');
		case state is
			when sIdle=>
				usbWRn <= '1';
				usbDoe(3 downto 0) <=  x"0";
				usbDout(7 downto 0) <= (others => '0');
				usbBEoe <= '0';
				fifoWrreq <= '0';
				busRequest <= (not usbDin(CH+11)) and (not usbTXEn) and (not fifoWrfull);
				busInUse <= '0';
			when sWaitbus=>
				usbWRn <= '1';
				usbDoe(3 downto 0) <=  x"0";
				usbDout(7 downto 0) <= (others => '0');
				usbBEoe <= '0';
				fifoWrreq <= '0';
				busRequest <= '1';
				busInUse <= '0';
			when sStart=>
				usbWRn <= '0';
				usbDoe(3 downto 0) <=  x"1";
				usbDout(7 downto 0) <= std_logic_vector(to_unsigned(CH,8));
				usbBEoe <= '1';
				fifoWrreq <= '0';
				busRequest <= '1';
				busInUse <= '1';
			when sWait1 | sWait2=>
				usbWRn <= '0';
				usbDoe(3 downto 0) <=  x"0";
				usbDout(7 downto 0) <= (others => '0');
				usbBEoe <= '0';
				fifoWrreq <= '0';
				busRequest <= '1';
				busInUse <= '1';
			when sReceiving=>
				usbWRn <= fifoWrfull or (not busGranted);
				usbDoe(3 downto 0) <=  x"0";
				usbDout(7 downto 0) <= (others => '0');
				usbBEoe <= '0';
				fifoWrreq <= (not usbRXn) and (busGranted) and (not fifoWrFull);
				busRequest <= '1';
				busInUse <= '1';
			when sSopt =>
				usbWRn <= '1';
				usbDoe(3 downto 0) <=  x"0";
				usbDout(7 downto 0) <= (others => '0');
				usbBEoe <= '0';
				fifoWrreq <= '0';
				busRequest <= '0';
				busInUse <= '0';
		end case;
	end process;
	
end rtl;
