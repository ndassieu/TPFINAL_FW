Este bloque es el encargado de sacar los datos del FT601 y escribirlos en la 
FIFO de recepción de datos de la FPGA.  
[Ver diseño detallado de este bloque](https://gitlab.com/ndassieu/MSE_TPFINAL_FW/wikis/firmware/diseño/detallado/communication/usbread.vhd)