library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package communicationArray is
  type comm_array_of_std_logic_vector_34 is array(natural range <>) of std_logic_vector(33 downto 0);
end package;