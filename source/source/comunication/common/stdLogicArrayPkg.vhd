library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package communicationStdLogicArray is
  type array_of_std_logic_vector_4 is array(natural range <>) of std_logic_vector(3 downto 0);
  type array_of_std_logic_vector_32 is array(natural range <>) of std_logic_vector(31 downto 0);
end package;