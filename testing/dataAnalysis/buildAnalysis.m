function [ speedTestsStats, bufferTestStats ] = buildAnalysis( folder )
%BUILDANALYSIS Summary of this function goes here
%   Detailed explanation goes here

testDescriptions = getTestDescriptions(folder);

speedTestsStats = buildSpeedTestStats(testDescriptions);

bufferTestStats = buildBufferTestStats(testDescriptions);
end

