function compare = CompareChannels(path, folder1Ch, folder2Ch, folder3Ch )
%COMPARECHANNELS Summary of this function goes here
%   Detailed explanation goes here

%CompareChannels('..\javaTB\testOutputs\','2018-06-24-buffer300s-1ch\','2018-06-24-buffer300s-2ch\','2018-06-24-buffer300s-4ch\');

compare{1}.channelFolders = [path folder1Ch];
compare{1}.channelString = '1 canal';
compare{2}.channelFolders = [path folder2Ch];
compare{2}.channelString = '2 canales';
compare{3}.channelFolders = [path folder3Ch];
compare{3}.channelString = '4 canales';

for i=1:length(compare)
    [ speedTestsStats, bufferTestStats ] = buildAnalysis( compare{i}.channelFolders );
    compare{i}.speedTestStats=speedTestsStats;
    compare{i}.bufferTestStats=bufferTestStats;
end

%showCompare(compare);
end

