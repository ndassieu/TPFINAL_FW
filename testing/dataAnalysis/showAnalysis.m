function showAnalysis(folder)
%testDescriptions = getTestDescriptions('..\javaTB\testOutputs\2018-06-03\'); 
%testDescriptions = getTestDescriptions('..\javaTB\testOutputs\');

[ speedTestsStats, bufferTestStats ] = buildAnalysis( folder );

plotSpeedTestStats(speedTestsStats);

plotBufferTestStats(bufferTestStats);