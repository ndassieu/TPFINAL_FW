function [ ] = plotBufferTestStats( bufferStats )
%PLOTBUFFERTESTSTATS Summary of this function goes here
%   Detailed explanation goes here
if (~bufferStats(1).valid)
    return;
end
for i=1:length(bufferStats)
    figure; hold all;
    for j=1:bufferStats(i).points
        plot(bufferStats(i).samplesInBuffer(j,:),bufferStats(i).bufferOverrunProbability(j,:));
        legendStr{j} = num2str(bufferStats(i).packetSize(j));
    end
    title(bufferStats(i).expectedMbps);
    legend(legendStr);
    axis tight;
    ylim([0 1/10000]);
    %set(gca, 'YScale', 'log');
end

