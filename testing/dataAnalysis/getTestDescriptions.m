function [ testDescriptions ] = getTestDescriptions( folder )
    %GETTESTDESCRIPTIONS Summary of this function goes here
    %   Detailed explanation goes here
    listing = dir(folder);
    for i=3:length(listing)
        testDescriptions(i-2) = fillDescription(listing(i).name, folder);
    end
end

function [description] = fillDescription(name, folder)
    %buffer0_4194304B_1716Mbps_10s_Buffer.csv
    k = strfind(name,'_'); % obtenemos los separadores
    ext = strfind(name,'.csv');
    description.packetSize = str2num(name(k(1)+1:k(2)-2));
    description.expectedMbps = str2num(name(k(2)+1:k(3)-5));
    description.statType = name(k(4)+1:ext(1)-1);
    description.name = name;
    description.folder = folder;
    description.isRampTest = strcmp(name(1:k(1)-2),'ramp');
end

