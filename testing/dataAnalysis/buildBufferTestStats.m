function [ bufferTestStats ] = buildBufferTestStats( descriptions )
%BUILDbufferTestStats Summary of this function goes here
%   Detailed explanation goes here
    j = 0;
    for i=1:length(descriptions)
        if (strcmp(descriptions(i).statType,'Buffer'))
            if (j==0)
                curExp = 0;
            else    
                curExp =  sameExpectedSpeed(bufferTestStats, descriptions(i).expectedMbps);
            end
            if (curExp == 0) 
                j =  j +1;
                curExp = j;
                bufferTestStats(curExp).name = descriptions(i).name;
                bufferTestStats(curExp).folder = descriptions(i).folder;
                bufferTestStats(curExp).expectedMbps = descriptions(i).expectedMbps;
                bufferTestStats(curExp).points = 0;
            end
            % lo ponemos en el lugar correcto
            place = bufferTestStats(curExp).points+1;
            for k=1:bufferTestStats(curExp).points
                if ( descriptions(i).packetSize > bufferTestStats(curExp).packetSize(k))
                    place = k;
                    break;
                end    
            end
            for k=bufferTestStats(curExp).points:-1:place
                bufferTestStats(curExp).ocurrences(k+1,:) = bufferTestStats(curExp).ocurrences(k,:);
                bufferTestStats(curExp).packetSize(k+1) = bufferTestStats(curExp).packetSize(k);
            end
            bufferTestStats(1).valid = true;
            bufferTestStats(curExp).points = bufferTestStats(curExp).points+1;
            M = csvread([bufferTestStats(curExp).folder descriptions(i).name]);
            bufferTestStats(curExp).ocurrences(place,:) = M;
            bufferTestStats(curExp).packetSize(place) = descriptions(i).packetSize;
        end
    end
    if (j==0)
        bufferTestStats(1).valid = false;
        return;
    end
    
    for i=1:length(bufferTestStats)
        for j=1:bufferTestStats(i).points
            bufferTestStats(i).samplesInBuffer(j,:)=1:length(bufferTestStats(i).ocurrences(j,:));
            % Ahora construimos la funciona densidad de probabilidad
            bufferTestStats(i).psf(j,:) = bufferTestStats(i).ocurrences(j,:)...
                    /sum(bufferTestStats(i).ocurrences(j,:));
            bufferTestStats(i).bufferOverrunProbability(j,:) = zeros(1,length(bufferTestStats(i).psf(j,:)));
            a = cumsum(bufferTestStats(i).psf(j,end:-1:1));
            bufferTestStats(i).bufferOverrunProbability(j,:) = a(end:-1:1);
%             for k=length(bufferTestStats(i).psf(j,:)):-1:1
%                 if (k~=length(bufferTestStats(i).psf(j,:)))
%                     bufferTestStats(i).bufferOverrunProbability(j,k) = bufferTestStats(i).psf(j,k)+...
%                         bufferTestStats(i).bufferOverrunProbability(j,k+1);
%                 else
%                     bufferTestStats(i).bufferOverrunProbability(j,k) = bufferTestStats(i).psf(j,k);
%                 end
%             end
        end
    end

end

function index = sameExpectedSpeed(bufferTestStats, expectedSpeed)
    index = 0;
    for i=1:length(bufferTestStats)
        if (bufferTestStats(i).expectedMbps == expectedSpeed)
            index = i;
            break;
        end
    end
end

