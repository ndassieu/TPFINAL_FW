function [ ] = plotSpeedTestStats( speedTestStats )
%PLOTSPEEDTESTSTATS Summary of this function goes here
%   Detailed explanation goes here
figure; hold all;
absMaxSpeed = 66*8*4;
for i=1:length(speedTestStats)
    plot(speedTestStats(i).packetSize,speedTestStats(i).speedMbps/absMaxSpeed*100);
    legendStr{i} = num2str(speedTestStats(i).expectedMbps);   
end
legend(legendStr);
title('% of Abs. Max. Speed');
set(gca, 'XScale', 'log');
axis tight
ylim([0 110]);

figure; hold all;
for i=1:length(speedTestStats)
    plot(speedTestStats(i).packetSize,(speedTestStats(i).packetSize)./(speedTestStats(i).speedMbps./8));
    legendStr{i} = ['Real Packet Time (us)' num2str(speedTestStats(i).expectedMbps)];
end
legend(legendStr);
title('Real Packet Time vs Packet Length');
set(gca, 'XScale', 'log');
set(gca, 'YScale', 'log');
axis tight
end

