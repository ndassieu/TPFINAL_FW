function [ output_args ] = showCompare( compare )
%SHOWCOMPARE Summary of this function goes here
%   Detailed explanation goes here
maxMbps = 66*8*4;

figure; hold all;
l=0;
for i=1:length(compare)
    for j=1:length(compare{i}.speedTestStats)
        if (compare{i}.speedTestStats(j).isRampTest)
            plot(compare{i}.speedTestStats(j).packetSize/1000000, compare{i}.speedTestStats(j).speedMbps);
            l = l+1;
            legendStr{l} = num2str(compare{i}.channelString); 
        end
    end
end
axis tight
title('Velocidad maxima vs largo de paquetes');
set(gca, 'XScale', 'log');
ylabel('Velocidad m�xima [Mbps]')
xlabel('Largo de paquetes [MB]')
legend(legendStr);
ylim([0 maxMbps]);


for i=1:length(compare)
    figure; hold all;
    for j=1:length(compare{i}.bufferTestStats)
        [~,maxBuffer] = min(compare{i}.bufferTestStats(j).bufferOverrunProbability');
        plot(compare{i}.bufferTestStats(j).packetSize, maxBuffer.*(4/1000));
        legendStr{j} = [num2str(compare{i}.bufferTestStats(j).expectedMbps) ' Mbps']; 
    end
    axis tight
    title(['Largo de buffers vs largo de paquete para ' compare{i}.channelString]);
    set(gca, 'XScale', 'log');
    %set(gca, 'YScale', 'log');
    ylabel('Largo de buffers [kB]')
    xlabel('Largo de paquetes [MB]')
    ylim([0 70*4]);
    legend(legendStr);
end

legendStr = {};
for i=1:length(compare)
    figure; hold all;
    for j=1:length(compare{i}.bufferTestStats)
        [~,maxBuffer] = min(compare{i}.bufferTestStats(j).bufferOverrunProbability');
        plot(compare{i}.bufferTestStats(j).packetSize, maxBuffer*4*8./compare{i}.bufferTestStats(j).expectedMbps);
        legendStr{j} = [num2str(compare{i}.bufferTestStats(j).expectedMbps) ' Mbps']; 
    end
    axis tight
    title(['Tiempo de buffer vs largo de paquete para ' compare{i}.channelString]);
    set(gca, 'XScale', 'log');
    %set(gca, 'YScale', 'log');
    ylabel('Tiempo de buffers [us]')
    xlabel('Largo de paquetes [MB]')
    ylim([0 1600]);
    legend(legendStr);
end

end

