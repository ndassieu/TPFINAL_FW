function [ speedTestStats ] = buildSpeedTestStats( descriptions )
%BUILDSPEEDTESTSTATS Summary of this function goes here
%   Detailed explanation goes here
    j = 0;
    for i=1:length(descriptions)
        if (strcmp(descriptions(i).statType,'Speed'))
            if (j==0)
                curExp = 0;
            else    
                curExp =  sameExpectedSpeed(speedTestStats, descriptions(i).expectedMbps);
            end
            if (curExp == 0) 
                j =  j +1;
                curExp = j;
                speedTestStats(curExp).name = descriptions(i).name;
                speedTestStats(curExp).folder = descriptions(i).folder;
                speedTestStats(curExp).expectedMbps = descriptions(i).expectedMbps;
                speedTestStats(curExp).points = 0;
                speedTestStats(curExp).isRampTest = descriptions(i).isRampTest;
            end
            % lo ponemos en el lugar correcto
            place = speedTestStats(curExp).points+1;
            for k=1:speedTestStats(curExp).points
                if ( descriptions(i).packetSize > speedTestStats(curExp).packetSize(k))
                    place = k;
                    break;
                end    
            end
            for k=speedTestStats(curExp).points:-1:place
                speedTestStats(curExp).speedMbps(k+1) = speedTestStats(curExp).speedMbps(k);
                speedTestStats(curExp).packetSize(k+1) = speedTestStats(curExp).packetSize(k);
            end
            speedTestStats(curExp).points = speedTestStats(curExp).points+1;
            M = csvread([speedTestStats(curExp).folder descriptions(i).name]);
            speedTestStats(curExp).speedMbps(place) = (1000*8*M(1))/M(2);
            speedTestStats(curExp).packetSize(place) = descriptions(i).packetSize;
        end
    end

end

function index = sameExpectedSpeed(speedTestStats, expectedSpeed)
    index = 0;
    for i=1:length(speedTestStats)
        if (speedTestStats(i).expectedMbps == expectedSpeed)
            index = i;
            break;
        end
    end
end

