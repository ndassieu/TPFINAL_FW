#include <stdio.h>
#include <windows.h>  // for windows specific keywords in ftd2xx.h
#include "FTD3XX.h"
#include "testBench.h"

#include <stdlib.h>


UCHAR txBuffer[4*MAX_RAMP_PACKET+HEADER_LEN];
UCHAR expectedBuffer[4*MAX_RAMP_PACKET+HEADER_LEN];
UCHAR rxBuffer[4*MAX_RAMP_PACKET+HEADER_LEN];

FT_HANDLE ftHandle = NULL;

int main(void) {
	FT_STATUS ftStatus; 
	DWORD numDevs = 0; 
	
	ftStatus = FT_CreateDeviceInfoList(&numDevs); 
	if (!FT_FAILED(ftStatus) && numDevs > 0) 
	{ 
		DWORD Flags = 0; 
		DWORD Type = 0; 
		DWORD ID = 0; 
		char SerialNumber[16] = { 0 }; 
		char Description[32] = { 0 }; 
	 
		printf("List of Connected Devices!\n\n"); 
		for (DWORD i = 0; i < numDevs; i++) { 
			ftStatus = FT_GetDeviceInfoDetail(i, &Flags, &Type, &ID, NULL,  
							SerialNumber, Description, &ftHandle); 
			if (!FT_FAILED(ftStatus)) { 
				printf("Device[%lu]\n", i); 
				printf("\tFlags: 0x%x %s | Type: %d | ID: 0x%08X | ftHandle=0x%p\n", 
				(int)Flags, 
				Flags & FT_FLAGS_SUPERSPEED ? "[USB 3]" : 
				Flags & FT_FLAGS_HISPEED ? "[USB 2]" : 
				Flags & FT_FLAGS_OPENED ? "[OPENED]" : "", 
				(int)Type, 
				(int)ID, 
				ftHandle); 
				printf("\tSerialNumber=%s\n", SerialNumber); 
				printf("\tDescription=%s\n", Description); 
			} else {
				printf("Error %d leyendo datos\n", (int)ftStatus);
				return ftStatus;
			}
		} 
		
		printf("Opening device FTDI SuperSpeed-FIFO Bridge\n"); 
		ftStatus = FT_Create("FTDI SuperSpeed-FIFO Bridge", FT_OPEN_BY_DESCRIPTION, &ftHandle);
		if (FT_FAILED(ftStatus)) {
			printf("Error %d abriendo el dispositivo\n", (int)ftStatus);
			return ftStatus;
		}
		
		printf("Runing Ramp Test\n"); 
		ftStatus = runRampTest();
		if (FT_FAILED(ftStatus)) {
			printf("Error %d corriendo el test de rampa\n", (int)ftStatus);
		}
		
		printf("\nClosing device 0\n"); 
		ftStatus = FT_Close(ftHandle);
		if (FT_FAILED(ftStatus)) {
			printf("Error %d cerrando el dispositivo\n", (int)ftStatus);
			return ftStatus;
		}
	} else if (!FT_FAILED(ftStatus) && numDevs == 0) {
		printf("No hay dispositivos conectados\n");
	} else {
		printf("No se pudo usar la librer�a del FTD3XX, error %d\n", (int)ftStatus);
	}
	
	return ftStatus;
}

FT_STATUS runRampTest() {
	FT_STATUS ftStatus;
	printf("runRampTest\n");
	ftStatus = FT_SetStreamPipe(ftHandle, FALSE, FALSE, getRxPipeID(RUN_RAMP_CHANNEL), 4*RUN_RAMP_PACKET_LEN+HEADER_LEN);
	if (FT_FAILED(ftStatus)) {
		printf("Error %d seteando Stream pipe\n", (int)ftStatus);
		return ftStatus;
	}
	clock_t startTime;
	clock_t readPipeTime = 0;
	long rxBytesCounter = 0;
	// creamos la rampa espera de respuesta
	buildExpectedRamp(expectedBuffer, RUN_RAMP_PACKET_LEN, RUN_RAMP_INCREMENT);
	// enviamos el packete para pedir una rampa
	ftStatus = runMode(RUN_RAMP_CHANNEL, RUN_RAMP_PACKET_LEN, RUN_RAMP_INCREMENT, 0);
	if (FT_FAILED(ftStatus)) {
		printf("Error %d entrando en runMode\n", (int)ftStatus);
		return ftStatus;
	}
	// data processing;
	startTime = clock();
	double deltaTseconds = 0;
	while (((deltaTseconds)<(double)(RUN_RAMP_TEST_TIME)) && FT_SUCCESS(ftStatus)) {
		// recibimos la rampa actual y la chequeamos
		ftStatus = receiveAndCheckRamp(RUN_RAMP_CHANNEL, expectedBuffer, RUN_RAMP_PACKET_LEN, &readPipeTime);
		// actualizamos el tiempo de corrida
		deltaTseconds = (double)(clock() - startTime) / CLOCKS_PER_SEC;
		// actualizamos el contador de bytes
		rxBytesCounter += RUN_RAMP_PACKET_LEN*4+8;
	}
	// guardamos el tiempo que tard�
	printf("Rx Bytes: %lu in %f ns\n", rxBytesCounter, deltaTseconds);
	printf("Tiempo en readPipe: %f ns\n", ((double)readPipeTime)/ CLOCKS_PER_SEC);
	// Detenemos la corrida
	ftStatus = stopMode(RUN_RAMP_CHANNEL, RUN_RAMP_PACKET_LEN, RUN_RAMP_INCREMENT, 0);
	if (FT_FAILED(ftStatus)) {
		printf("Error %d deteniendo la rampa\n", (int)ftStatus);
		return ftStatus;
	}
	// Limpiamos lo que queda
	ftStatus = flushLeftPackets(RUN_RAMP_CHANNEL, RUN_RAMP_PACKET_LEN, 1);
	if (FT_FAILED(ftStatus)) {
		printf("Error %d flusheando el pipe\n", (int)ftStatus);
		return ftStatus;
	}
	// calculamos las estadisticas
	double avgMBps = ((double)(rxBytesCounter/deltaTseconds))/(1024*1024);
	printf("Promedio de recepci�n: %f Mbps = %g MBps\n", avgMBps*8, avgMBps);
	ftStatus = FT_ClearStreamPipe(ftHandle, FALSE, FALSE, getRxPipeID(RUN_RAMP_CHANNEL));
	if (FT_FAILED(ftStatus)) {
		printf("Error %d limpiando el pipe\n", (int)ftStatus);
		return ftStatus;
	}
	printf("runRampTest Passed\n");
	return ftStatus;
}

FT_STATUS flushLeftPackets(int channel, int rampLength, long stimeout) {
	FT_STATUS ftStatus = FT_OK;
	ULONG expectedPacketLen = 4*rampLength+HEADER_LEN;
	ULONG countData;
	clock_t startTime = clock();
	double timeElapsed = 0;
	while ((timeElapsed<(double)stimeout) && (FT_SUCCESS(ftStatus))) {
		ftStatus = FT_ReadPipe(ftHandle, getRxPipeID(channel), rxBuffer, expectedPacketLen, &countData, NULL);
		timeElapsed = (double)(clock() - startTime) / CLOCKS_PER_SEC;
	}
	return FT_AbortPipe(ftHandle, getRxPipeID(RUN_RAMP_CHANNEL));
}

FT_STATUS receiveAndCheckRamp(int channel, PUCHAR expectedBuffer, int rampLength, clock_t *readPipeTime) {
	FT_STATUS ans;
	ULONG expectedPacketLen = 4*rampLength+HEADER_LEN;
	ULONG countData;
	clock_t readPipeStart;
	// recibimos el resultado
	readPipeStart = clock();
	ans = FT_ReadPipe(ftHandle, getRxPipeID(channel), rxBuffer, expectedPacketLen, &countData, NULL);
	*readPipeTime += (clock()-readPipeStart);
	if (FT_FAILED(ans)) {
		return ans;
	}
	ans = assertEqualsULONG("Bytes leidos distintos de bytes a leer", countData, expectedPacketLen); 
	if (FT_FAILED(ans)) {
		return ans;
	}
	
	// Checkeamos vs el esperado
	return checkPacket(expectedBuffer, rxBuffer, expectedPacketLen);
	//return FT_OK;
}

FT_STATUS checkPacket(PUCHAR expectedPacket, PUCHAR actualPacket, int length) {
	return checkData(expectedPacket, 0, length, actualPacket);
}

FT_STATUS checkData(PUCHAR expected, int expectedDataOffset, int length , PUCHAR actual) {
	FT_STATUS ftStatus = FT_OK;
	for (int i=expectedDataOffset; i<expectedDataOffset+length; i++) {
		ftStatus = assertEqualsUCHAR("Valor leido difiere del esperado", expected[i], actual[i]);
		if (FT_FAILED(ftStatus)) {
			break;
		}
	}
	return ftStatus;
}

FT_STATUS runMode(int channel, int length, int rampIncrement, int div) {
	FT_STATUS ans;
	ans = configTxMode(channel, length, rampIncrement, div, FALSE, FALSE);
	if (FT_FAILED(ans)) {
		return ans;
	}
	return configTxMode(channel, length, rampIncrement, div, TRUE, FALSE);
}
	
FT_STATUS stopMode(int channel, int length, int rampIncrement, int div) {
	return configTxMode(channel, length, rampIncrement, div, FALSE, FALSE);
}
	
FT_STATUS configTxMode(int channel, int length, int rampIncrement, int div, 
	BOOLEAN run, BOOLEAN step) {
	FT_STATUS ans;
	buildHeader(txBuffer, TEST_CFG_ADDR, 3);
	int offset  = HEADER_LEN/4;
	int cfg = (run)?1<<7:0;
	cfg += (rampIncrement!=0)?1<<1:0;
	cfg += (step)?1:0;
	addIntegerToBuffer(txBuffer, offset, length-1);
	addIntegerToBuffer(txBuffer, offset+1, rampIncrement);
	addIntegerToBuffer(txBuffer, offset+2, div);
	addIntegerToBuffer(txBuffer, offset+3, cfg);
	ULONG countData;
	ans = FT_WritePipe(ftHandle, getTxPipeID(channel), txBuffer, HEADER_LEN+4*4, &countData, NULL);
	if (FT_FAILED(ans)) {
		return ans;
	}
	return assertEqualsULONG("Bytes escritos distintos de bytes a escribir", countData, HEADER_LEN+4*4); 
}

FT_STATUS assertEqualsUCHAR(char *info, UCHAR expected, UCHAR actual) {
	if (expected != actual) {
		printf(info);
		printf("Expected %c, Actual %c", expected, actual);
		return FT_OTHER_ERROR;
	}
	return FT_OK;
}

FT_STATUS assertEqualsULONG(char *info, ULONG expected, ULONG actual) {
	if (expected != actual) {
		printf(info);
		printf("Expected %lu, Actual %lu", expected, actual);
		return FT_OTHER_ERROR;
	}
	return FT_OK;
}
	
void buildExpectedRamp(PUCHAR expectedBuffer, int rampLength, int rampIncrement) {
	int rampValue = 0;
	buildHeader(expectedBuffer, RAMP_RX_ADDR, rampLength-1);
	for (int i=HEADER_LEN/4; i< rampLength+HEADER_LEN/4; i++) {
		addIntegerToBuffer(expectedBuffer, i, rampValue);
		rampValue += rampIncrement;
	}
}

void addIntegerToBuffer(PUCHAR buffer, int wordOffset, int data) {
	buffer[4*wordOffset] = (UCHAR)((data>>24)&0xFF);
	buffer[4*wordOffset+1] = (UCHAR)((data>>16)&0xFF);
	buffer[4*wordOffset+2] = (UCHAR)((data>>8)&0xFF);
	buffer[4*wordOffset+3] = (UCHAR)(data&0xFF);
}
	
//private static boolean checkHeader(int expectedHeader, long expectedPacketLength, PUCHAR actual) {
//	boolean notFail = true;
//	buildHeader(expectedArray, expectedHeader, expectedPacketLength);
//	
//	for (int i=0; i<HEADER_LEN; i++) {
//		if (expectedArray[i]!=actual[i]) {
//			System.out.println("Header( " +i+ ") = " + actual[i] + 
//						" valor esperado:" + expectedArray[i]);
//			notFail = false;
//		}
//	}
//	return notFail;
//}

void buildHeader(PUCHAR data, int header, long packetlength) {
	data[0] = (UCHAR)((header>>8)&0xFF);
	data[1] = (UCHAR)(header&0xFF);
	data[2] = (UCHAR)((packetlength>>24)&0xFF);
	data[3] = (UCHAR)((packetlength>>16)&0xFF);
	data[4] = (UCHAR)((packetlength>>8)&0xFF);
	data[5] = (UCHAR)(packetlength&0xFF);
	int crc = computeCrc(data);
	data[6] = (UCHAR)((crc>>8)&0xFF);	
	data[7] = (UCHAR)(crc&0xFF);
}

int computeCrc(PUCHAR data) {
	int accum=0;
	for (int i=0; i<6; i++) {
		accum+=((int)data[i])&0xFF;
	}
	accum = ~accum;
	return (accum&0xFFFF);
}
	
UCHAR getRxPipeID(int channel) {
	return (UCHAR) (0x80 | getTxPipeID(channel));
}

UCHAR getTxPipeID(int channel) {
	return (UCHAR) (channel+2);
}