
#include <time.h>

#define HEADER_LEN 8
#define LOOBACK_TX_ADDR 0x0001
#define LOOBACK_RX_ADDR 0x8001
#define RAMP_TX_ADDR 0x0002
#define RAMP_RX_ADDR 0x8002
#define BUFFERSIZE_RX_ADDR 0x8003
#define TEST_CFG_ADDR 0x0004
#define FIRMWARE_CHANNELS 1
#define MAX_RAMP_PACKET 2500000

#define RX_PIPE_MS_TIMEOUT 1000
// Tiempo del test de rampa en segundos
#define RUN_RAMP_CHANNEL 0
#define RUN_RAMP_TEST_TIME 1
#define RUN_RAMP_PACKET_LEN 2000000//MAX_RAMP_PACKET;
#define RUN_RAMP_INCREMENT 1//(int)((0xFFFFFFFFL)/(RUN_RAMP_PACKET_LEN-1));
	
FT_STATUS runRampTest();

FT_STATUS flushLeftPackets(int channel, int rampLength, long stimeout);

FT_STATUS receiveAndCheckRamp(int channel, PUCHAR expectedBuffer, int rampLength, clock_t *readPipeTime);

FT_STATUS checkPacket(PUCHAR expectedPacket, PUCHAR actualPacket, int length);

FT_STATUS checkData(PUCHAR expected, int expectedDataOffset, int length , PUCHAR actual);

FT_STATUS runMode(int channel, int length, int rampIncrement, int div);

FT_STATUS stopMode(int channel, int length, int rampIncrement, int div);

FT_STATUS configTxMode(int channel, int length, int rampIncrement, int div, 
	BOOLEAN run, BOOLEAN step);
	
FT_STATUS assertEqualsUCHAR(char *info, UCHAR expected, UCHAR actual);

FT_STATUS assertEqualsULONG(char *info, ULONG expected, ULONG actual);

void buildExpectedRamp(PUCHAR expectedBuffer, int rampLength, int rampIncrement);

void addIntegerToBuffer(PUCHAR buffer, int wordOffset, int data);	

void buildHeader(PUCHAR data, int header, long packetlength);

int computeCrc(PUCHAR data);

UCHAR getRxPipeID(int channel);

UCHAR getTxPipeID(int channel);
