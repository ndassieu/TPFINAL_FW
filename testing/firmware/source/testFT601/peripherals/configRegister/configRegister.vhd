-- Este modulo es el encargado de enviar los datos de la FIFO de
-- transmisi�n y colocarlos en el buffer de salida del FT601
-- para descripci�n sobre los estados consultar el archivo readme.md

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity configRegister is
	generic (
		PERIPH_ADDR 		: std_logic_vector(15 downto 0) := x"0004"
	);
	port
	(
		clk	 			: in		std_logic;
		-- Se�ales del bus de perifericos, se usa en modo loopback
		periphAddr		: in		std_logic_vector (15 downto 0);
		periphWordsLeft	: in		std_logic_vector (31 downto 0);
		periphAck		: buffer	std_logic;
		periphEn	 	: in 		std_logic;
		periphRdreq		: buffer	std_logic;
		periphRdempty	: in		std_logic;
		periphQ			: in		std_logic_vector (31 downto 0);
		-- Se�ales de control de los modos rampa y buffer
		packetFreqDC 	: out		std_logic_vector (4 downto 0);
		packetLength	: out		std_logic_vector (31 downto 0);
		packetIsRamp	: out		std_logic; --modo rampa =1, modo buffer =0
		packetRampInc	: out 		std_logic_vector (31 downto 0);
		packetRun		: out		std_logic;
		packetStepMode	: out		std_logic;
		-- se�al de reset
		reset	 		: in		std_logic
	);
	
end entity;

architecture rtl of configRegister is

	-- Build an enumerated type for the state machine
	type state_type is (sIdle, sPacketLength, sPacketRampInc, sPacketFreqDiv, sFlags, sWait);
	
	-- Register to hold the current state
	signal state : state_type;
	
	-- Attribute "safe" implements a safe state machine.
	-- This is a state machine that can recover from an
	-- illegal state (by returning to the reset state).
	attribute syn_encoding : string;
	attribute syn_encoding of state_type : type is "safe";

	signal bytesLeft : std_logic_vector (31 downto 0);
begin	
	-- genera se�al periphAck
	process (periphEn, periphAddr)
	begin
		if (periphEn = '1') and (PERIPH_ADDR = periphAddr) then
			periphAck <= '1';
		else 
			periphAck <= '0';
		end if;
	end process;

	-- Maquina de estados
	process (clk, reset)
	begin
		if reset = '1' then
			state <= sIdle;
			packetFreqDC <= (others => '0');
			packetLength <= (others => '0');
			packetIsRamp <= '0';
			packetRampInc <= (others => '0');
			packetRun <= '0';
			packetStepMode <= '0';
		elsif (rising_edge(clk)) then
			-- Determine the next state synchronously, based on
			-- the current state and the input
			case state is
				when sIdle => 
					if (periphAck='1') then
						state <= sPacketLength;
					end if;
				when sPacketLength =>
					if (periphRdempty = '0') then
						packetLength(31 downto 24) <= periphQ(7 downto 0);
						packetLength(23 downto 16) <= periphQ(15 downto 8);
						packetLength(15 downto 8) <= periphQ(23 downto 16);
						packetLength(7 downto 0) <= periphQ(31 downto 24);
						if (periphWordsLeft = (periphWordsLeft'range => '0')) then
							state <= sWait;
						else
							state <=sPacketRampInc;
						end if;
					end if;
				when sPacketRampInc =>
					if (periphRdempty = '0') then
						PacketRampInc(31 downto 24) <= periphQ(7 downto 0);
						PacketRampInc(23 downto 16) <= periphQ(15 downto 8);
						PacketRampInc(15 downto 8) <= periphQ(23 downto 16);
						PacketRampInc(7 downto 0) <= periphQ(31 downto 24);
						if (periphWordsLeft = (periphWordsLeft'range => '0')) then
							state <= sWait;
						else
							state <=sPacketFreqDiv;
						end if;
					end if;
				when sPacketFreqDiv =>
					if (periphRdempty = '0') then
						packetFreqDC(4 downto 0) <= periphQ(28 downto 24);
						if (periphWordsLeft = (periphWordsLeft'range => '0')) then
							state <= sWait;
						else
							state <=sFlags;
						end if;
					end if;
				when sFlags =>
					if (periphRdempty = '0') then
						packetRun		<= periphQ(31);
						packetIsRamp	<= periphQ(25);
						packetStepMode  <= periphQ(24);
						state <= sWait;
					end if; 
				when sWait =>
					-- Waits periphAck false to restart
					if (periphAck='0') then
						state <= sIdle;
					end if;
				when others =>
					state  <= sIdle;
			end case;
		end if;
	end process;
	
	-- Determine the output based only on the current state
	-- and the input (do not wait for a clock edge).
	process (state, periphRdempty)
		variable chkSum : std_logic_vector (15 downto 0);
	begin
		case state is
			when sIdle =>
				periphRdreq <= '0';
			when sPacketLength =>
				periphRdreq <= (not periphRdempty);
			when sPacketRampInc => 
				periphRdreq <= (not periphRdempty);
			when sPacketFreqDiv=>
				periphRdreq <= (not periphRdempty);
			when sFlags=>
				periphRdreq <= (not periphRdempty);
			when sWait=>
				periphRdreq <= (not periphRdempty);
		end case;
	end process;
	
end rtl;
