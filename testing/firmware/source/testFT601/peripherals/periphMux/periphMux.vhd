
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity periphMux is
	generic (
		PERIPH_COUNT 	: positive := 1
	);
	port
	(
		-- Se�ales del bus de perifericos, se usa en modo loopback
		muxedAck		: out	std_logic;
		muxedRdreq		: out	std_logic;
		-- Se�ales de control de los modos rampa y buffer
		perifAck	 	: in	std_logic_vector (PERIPH_COUNT-1 downto 0);
		perifRdreq		: in	std_logic_vector (PERIPH_COUNT-1 downto 0)
	);
end entity;

architecture rtl of periphMux is
begin

	process (perifAck, perifRdreq)
	begin
		muxedAck <= '0';
		muxedRdreq <= '0';
		for i in 0 to PERIPH_COUNT-1 loop
			if (perifAck(i) = '1') then
				muxedAck <= perifAck(i);
				muxedRdreq <= perifRdreq(i);
				exit;
			end if;
		end loop;
	end process;

end rtl;