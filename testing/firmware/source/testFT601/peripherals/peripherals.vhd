LIBRARY ieee;
USE ieee.std_logic_1164.all; 

ENTITY peripherals IS 
	GENERIC 
	(
		-- Numero total de Perifericos 
		PERIPH_COUNT: NATURAL := 2;
		-- Configuraci�n de direcciones de los perifericos
		TX_CONTROLLER_ADDR 			: std_logic_vector(15 downto 0) := x"0001";
		CONFIG_REGISTER_ADDR 		: std_logic_vector(15 downto 0) := x"0004";
		-- Configuraiciones adicionales de los pereifericos
		TX_CONTROLLER_LOOPBACK_ADDR	: std_logic_vector(15 downto 0) := x"8001";
		TX_CONTROLLER_RAMP_ADDR		: std_logic_vector(15 downto 0) := x"8002";
		TX_CONTROLLER_BUFFERSIZE_ADDR: std_logic_vector(15 downto 0) := x"8003"
	);
	PORT
	(
		clk	 			: in		std_logic;
		-- Se�ales para todos los perifericos
		periphAddr		: in		std_logic_vector (15 downto 0);
		periphWordsLeft	: in		std_logic_vector (31 downto 0);
		periphAck		: out		std_logic;
		periphEn	 	: in 		std_logic;
		periphRdreq		: out		std_logic;
		periphRdempty	: in		std_logic;
		periphQ			: in		std_logic_vector (31 downto 0);	
		-- Se�ales del periferico que maneja la TX fifo
		fifoWrreq		: out		std_logic;
		fifoWrfull	 	: in		std_logic;
		fifoData  		: out		std_logic_vector (33 downto 0);
		-- Otras se�ales de IO otros perifericos
		
		-- se�al de reset
		reset	 		: in		std_logic
	);
END peripherals;

ARCHITECTURE peripherals_arch OF peripherals IS
	signal periphNAck		: std_logic_vector (PERIPH_COUNT-1 downto 0);
	signal periphNRdreq		: std_logic_vector (PERIPH_COUNT-1 downto 0);
	
	-- Se�ales para unir el transmitController con el configRegister
	signal packetFreqDC 	: std_logic_vector  (4 downto 0);
	signal packetLength		: std_logic_vector (31 downto 0);
	signal packetIsRamp		: std_logic;
	signal packetRampInc	: std_logic_vector (31 downto 0);
	signal packetRun		: std_logic;
	signal packetStepMode	: std_logic;
	
	
	component transmitController is
		generic (
			PERIPH_ADDR 		: std_logic_vector(15 downto 0) := x"0001";
			LOOPBACK_ADDR		: std_logic_vector(15 downto 0) := x"8001";
			RAMP_ADDR			: std_logic_vector(15 downto 0) := x"8002";
			BUFFERSIZE_ADDR		: std_logic_vector(15 downto 0) := x"8003"
		);
		port (
			clk	 			: in		std_logic;
			fifoWrreq		: out		std_logic;
			fifoWrfull	 	: in		std_logic;
			fifoData  		: out		std_logic_vector (33 downto 0);
			periphAddr		: in		std_logic_vector (15 downto 0);
			periphWordsLeft	: in		std_logic_vector (31 downto 0);
			periphAck		: buffer	std_logic;
			periphEn	 	: in 		std_logic;
			periphRdreq		: out		std_logic;
			periphRdempty	: in		std_logic;
			periphQ			: in		std_logic_vector (31 downto 0);
			packetFreqDC 	: in		std_logic_vector (4 downto 0);
			packetLength	: in		std_logic_vector (31 downto 0);
			packetIsRamp	: in		std_logic; --modo rampa =1, modo buffer =0
			packetRampInc	: in 		std_logic_vector (31 downto 0);
			packetRun		: in		std_logic;
			packetStepMode	: in		std_logic;
			reset	 		: in		std_logic
		);
	end component;
	
	component configRegister is
	generic (
			PERIPH_ADDR 		: std_logic_vector(15 downto 0) := x"0004"
		);
		port
		(
			clk	 			: in		std_logic;
			periphAddr		: in		std_logic_vector (15 downto 0);
			periphWordsLeft	: in		std_logic_vector (31 downto 0);
			periphAck		: buffer	std_logic;
			periphEn	 	: in 		std_logic;
			periphRdreq		: buffer	std_logic;
			periphRdempty	: in		std_logic;
			periphQ			: in		std_logic_vector (31 downto 0);
			packetFreqDC 	: out		std_logic_vector (4 downto 0);
			packetLength	: out		std_logic_vector (31 downto 0);
			packetIsRamp	: out		std_logic; --modo rampa =1, modo buffer =0
			packetRampInc	: out 		std_logic_vector (31 downto 0);
			packetRun		: out		std_logic;
			packetStepMode	: out		std_logic;
			reset	 		: in		std_logic
		);	
	end component;
	
	component periphMux is
	generic (
			PERIPH_COUNT 	: positive := 1
		);
		port
		(
			muxedAck		: out	std_logic;
			muxedRdreq		: out	std_logic;
			perifAck	 	: in	std_logic_vector (PERIPH_COUNT-1 downto 0);
			perifRdreq		: in	std_logic_vector (PERIPH_COUNT-1 downto 0)
		);
	end component;
	
BEGIN 

	-- Instanciamos el mux de perifericos
	periphMux1 : periphMux 
	generic map (
		PERIPH_COUNT => PERIPH_COUNT
	)
	port map
	(
		muxedAck => periphAck,
		muxedRdreq => periphRdreq,
		perifAck => periphNAck,
		perifRdreq => periphNRdreq
	);
	
	-- Instanciamos los bloques transmitController
	transmitController1 : transmitController 
	generic map (
		PERIPH_ADDR 	=> TX_CONTROLLER_ADDR,
		LOOPBACK_ADDR 	=> TX_CONTROLLER_LOOPBACK_ADDR,
		RAMP_ADDR 		=> TX_CONTROLLER_RAMP_ADDR,
		BUFFERSIZE_ADDR => TX_CONTROLLER_BUFFERSIZE_ADDR
	)
	port map
	(
		clk => clk,
		fifoWrreq => fifoWrreq,
		fifoWrfull => fifoWrfull,
		fifoData => fifoData,
		periphAddr => periphAddr,
		periphWordsLeft => periphWordsLeft,
		periphAck => periphNAck(0),
		periphEn => periphEn,
		periphRdreq => periphNRdreq(0),
		periphRdempty => periphRdempty,
		periphQ => periphQ,
		packetFreqDC => packetFreqDC,
		packetLength => packetLength,
		packetIsRamp => packetIsRamp,
		packetRampInc => packetRampInc,
		packetRun => packetRun,
		packetStepMode => packetStepMode,
		reset => reset
	);
	
	-- Instanciamos el registro de configuraci�n
	configRegister1 : configRegister
	generic map (
		PERIPH_ADDR => CONFIG_REGISTER_ADDR
	)
	port map
	(
		clk => clk,
		periphAddr => periphAddr,
		periphWordsLeft => periphWordsLeft,
		periphAck => periphNAck(1),
		periphEn => periphEn,
		periphRdreq => periphNRdreq(1),
		periphRdempty => periphRdempty,
		periphQ => periphQ,
		packetFreqDC => packetFreqDC,
		packetLength => packetLength,
		packetIsRamp => packetIsRamp,
		packetRampInc => packetRampInc,
		packetRun => packetRun,
		packetStepMode => packetStepMode,
		reset => reset
	);
END peripherals_arch;
