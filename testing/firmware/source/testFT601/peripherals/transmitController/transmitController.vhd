-- Este modulo es el encargado de enviar los datos de la FIFO de
-- transmisi�n y colocarlos en el buffer de salida del FT601
-- para descripci�n sobre los estados consultar el archivo readme.md

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity transmitController is
	generic (
		PERIPH_ADDR 		: std_logic_vector(15 downto 0) := x"0001";
		LOOPBACK_ADDR		: std_logic_vector(15 downto 0) := x"8001";
		RAMP_ADDR			: std_logic_vector(15 downto 0) := x"8002";
		BUFFERSIZE_ADDR		: std_logic_vector(15 downto 0) := x"8003"
	);
	port
	(
		clk	 			: in		std_logic;
		-- Se�ales manejo de la fifo de transmisi�n 
		fifoWrreq		: out		std_logic;
		fifoWrfull	 	: in		std_logic;
		fifoData  		: out		std_logic_vector (33 downto 0);
		-- Se�ales del bus de perifericos, se usa en modo loopback
		periphAddr		: in		std_logic_vector (15 downto 0);
		periphWordsLeft	: in		std_logic_vector (31 downto 0);
		periphAck		: buffer	std_logic;
		periphEn	 	: in 		std_logic;
		periphRdreq		: buffer	std_logic;
		periphRdempty	: in		std_logic;
		periphQ			: in		std_logic_vector (31 downto 0);
		-- Se�ales de control de los modos rampa y buffer
		packetFreqDC 	: in		std_logic_vector (4 downto 0);
		packetLength	: in		std_logic_vector (31 downto 0);
		packetIsRamp	: in		std_logic; --modo rampa =1, modo buffer =0
		packetRampInc	: in 		std_logic_vector (31 downto 0);
		packetRun		: in		std_logic;
		packetStepMode	: in		std_logic;
		-- se�al de reset
		reset	 		: in		std_logic
	);
	
end entity;

architecture rtl of transmitController is

	-- Build an enumerated type for the state machine
	type state_type is (sIdle, sLBHeader1, sLBHeader2, sLBData, sLBWait, 
			sPacketHeader1, sPacketHeader2, sPacketData);
	
	-- Register to hold the current state
	signal state : state_type;
	
	-- Attribute "safe" implements a safe state machine.
	-- This is a state machine that can recover from an
	-- illegal state (by returning to the reset state).
	attribute syn_encoding : string;
	attribute syn_encoding of state_type : type is "safe";
	
	signal enable : std_logic;
	signal divCount : natural range 0 to 31;
	signal pendingCount : std_logic_vector(15 downto 0);
	signal bytesLeft : std_logic_vector (31 downto 0);
	signal waitingReload : std_logic;
	signal rampValue : std_logic_vector (31 downto 0);
	signal sendingPacket : std_logic;
	signal addSample : std_logic;
begin	
	process (clk, reset)
	begin
		if reset = '1' then
			addSample <= '0';
			divCount <= 0; 
		elsif (rising_edge(clk)) then
			if (divCount < 31) then
				divCount <= divCount+1;
			else
				divCount <= 0;
			end if;
			if (divCount<to_integer(unsigned(packetFreqDC))) then
				addSample <= '0';
			else
				addSample <= '1';
			end if;
		end if;
	end process;
	-- genera la se�al de enable para dividir la frecuencia de datos
	process (clk, reset)
	begin
		if reset = '1' then
			enable <= '0'; 
			pendingCount <= (pendingCount'range => '0');
		elsif (rising_edge(clk)) then
			if (state = sIdle) then
				pendingCount <= (pendingCount'range => '0');
			elsif (addSample = '1') and (sendingPacket = '1') and (fifoWrfull='1') then
				if (pendingCount/=(pendingCount'range => '1')) then
					pendingCount <= std_logic_vector(unsigned(pendingCount)+1);
				end if;
			elsif (sendingPacket = '1') and (fifoWrFull='0') then
				if ((pendingCount /= (pendingCount'range => '0')) and (addSample = '0'))  then
					pendingCount <= std_logic_vector(unsigned(pendingCount)-1);
				end if;
			end if;
			if ((addSample = '1') or (pendingCount /= (pendingCount'range => '0'))) and (fifoWrfull= '0') then
				enable <= '1';
			else 
				enable <= '0';
			end if;
		end if;
	end process;
	
	-- genera se�al periphAck
	process (periphEn, periphAddr)
	begin
		if (periphEn = '1') and (PERIPH_ADDR = periphAddr) then
			periphAck <= '1';
		else 
			periphAck <= '0';
		end if;
	end process;

	process (clk, reset, state) 
	begin
		if reset = '1' then
			waitingReload <= '1';
		elsif (rising_edge(clk)) then
			if (packetRun = '0') then
				waitingReload <= '0';
			elsif (state = sPacketHeader1) and (packetStepMode = '1') then
				waitingReload <= '1';
			end if;
		end if;
	end process;

	-- Maquina de estados
	process (clk, reset)
	begin
		if reset = '1' then
			state <= sIdle;
			bytesLeft <= (others => '0');
			rampValue <= (others => '0');
		elsif (rising_edge(clk)) then
			-- Determine the next state synchronously, based on
			-- the current state and the input
			case state is
				when sIdle => 
					if (periphAck='1') then
						state <= sLBHeader1;
					elsif (packetRun='1') and (waitingReload='0') then
						state <= sPacketHeader1;
					end if;
				when sPacketHeader1 =>
					rampValue <= (others => '0');
					bytesLeft <= packetLength;
					if (enable = '1') and (fifoWrfull= '0') then
						state <= sPacketHeader2;
					end if;
				when sPacketHeader2 =>
					if (enable = '1') and (fifoWrfull= '0') then
						state <= sPacketData;
					end if;
				when sPacketData =>
					if (enable = '1') and (fifoWrfull= '0') then
						if (bytesLeft=(bytesLeft'range => '0')) then
							if (periphAck='1') then 
								state <= sLBHeader1;
							elsif (waitingReload = '0') and (packetRun='1') then
								state <= sPacketHeader1;
							else 
								state <= sIdle;
							end if;
						else
							rampValue <= std_logic_vector(unsigned(rampValue) + unsigned(packetRampInc));
							bytesLeft <= std_logic_vector(unsigned(bytesLeft)-1);
						end if;
					end if;
				when sLBHeader1 =>
					if (periphRdempty = '0') and (fifoWrfull= '0') then
						state <= sLBHeader2;
					end if;
				when sLBHeader2 =>
					if (periphRdempty = '0') and (fifoWrfull= '0') then
						state <= sLBData;
					end if;
				when sLBData =>
					if (periphRdempty = '0') and (fifoWrfull= '0') and (periphWordsLeft = (periphWordsLeft'range => '0')) then
						state <= sLBWait;
					end if;
				when sLBWait =>
					-- Waits periphAck false to restart
					if (periphAck='0') then
						state <= sIdle;
					end if;
				when others =>
					state  <= sIdle;
			end case;
		end if;
	end process;
	
	-- Determine the output based only on the current state
	-- and the input (do not wait for a clock edge).
	process (state, pendingCount, rampValue, periphWordsLeft, periphRdempty, fifoWrfull, periphQ, enable, packetIsRamp, packetLength)
		variable chkSum : std_logic_vector (15 downto 0);
	begin
		-- siempre se mandan bloque de 4 bytes
		fifoData(33 downto 32) <= "11";
		case state is
			when sIdle =>
				fifoData <= (others => '0');
				fifoWrreq <= '0';
				periphRdreq <= '0';
				sendingPacket <= '0';
			when sPacketHeader1 =>
				if (packetIsRamp = '1') then
					fifoData(7 downto 0) <= RAMP_ADDR(15 downto 8);
					fifoData(15 downto 8) <= RAMP_ADDR(7 downto 0);
				else 
					fifoData(7 downto 0) <= BUFFERSIZE_ADDR(15 downto 8);
					fifoData(15 downto 8) <= BUFFERSIZE_ADDR(7 downto 0);
				end if;
				fifoData(23 downto 16) <= packetLength(31 downto 24);
				fifoData(31 downto 24) <= packetLength(23 downto 16);
				fifoWrreq <= (not fifoWrfull) and (enable);
				periphRdreq <= '0';
				sendingPacket <= '1';
			when sPacketHeader2 => 
				fifoData(7 downto 0) <= packetLength(15 downto 8);
				fifoData(15 downto 8) <= packetLength(7 downto 0);
				if (packetIsRamp = '1') then
					chkSum := not (std_logic_vector(
						resize(unsigned(RAMP_ADDR(15 downto 8)),16) + 
						resize(unsigned(RAMP_ADDR(7 downto 0)),16) + 
						resize(unsigned(packetLength(31 downto 24)),16) + 
						resize(unsigned(packetLength(23 downto 16)),16) + 
						resize(unsigned(packetLength(15 downto 8)),16) + 
						resize(unsigned(packetLength(7 downto 0)),16) ));
				else 
					chkSum := not (std_logic_vector(
						resize(unsigned(BUFFERSIZE_ADDR(15 downto 8)),16) + 
						resize(unsigned(BUFFERSIZE_ADDR(7 downto 0)),16) + 
						resize(unsigned(packetLength(31 downto 24)),16) + 
						resize(unsigned(packetLength(23 downto 16)),16) + 
						resize(unsigned(packetLength(15 downto 8)),16) + 
						resize(unsigned(packetLength(7 downto 0)),16) ));
				end if;
				fifoData(23 downto 16) <= chkSum(15 downto 8);
				fifoData(31 downto 24) <= chkSum(7 downto 0);
				fifoWrreq <= (not fifoWrfull) and (enable);
				periphRdreq <= '0';
				sendingPacket <= '1';
			when sPacketData=>
				if (packetIsRamp = '1') then
					fifoData(7 downto 0) <= rampValue(31 downto 24);
					fifoData(15 downto 8) <= rampValue(23 downto 16);
					fifoData(23 downto 16) <= rampValue(15 downto 8);
					fifoData(31 downto 24) <= rampValue(7 downto 0);
				else
					fifoData(7 downto 0) <=  (others => '0');
					fifoData(15 downto 8) <= (others => '0');
					fifoData(23 downto 16) <=  pendingCount(15 downto 8);
					fifoData(31 downto 24) <= pendingCount(7 downto 0);
				end if;
				fifoWrreq <= (not fifoWrfull) and (enable);
				periphRdreq <= '0';
				sendingPacket <= '1';
			when sLBHeader1=>
				fifoData(7 downto 0) <= LOOPBACK_ADDR(15 downto 8);
				fifoData(15 downto 8) <= LOOPBACK_ADDR(7 downto 0);
				fifoData(23 downto 16) <= periphWordsLeft(31 downto 24);
				fifoData(31 downto 24) <= periphWordsLeft(23 downto 16);
				fifoWrreq <= (not periphRdempty) and (not fifoWrfull);
				periphRdreq <= '0';
				sendingPacket <= '0';
			when sLBHeader2=>
				fifoData(7 downto 0) <= periphWordsLeft(15 downto 8);
				fifoData(15 downto 8) <= periphWordsLeft(7 downto 0);
				chkSum := not (std_logic_vector(
					resize(unsigned(LOOPBACK_ADDR(15 downto 8)),16) + 
					resize(unsigned(LOOPBACK_ADDR(7 downto 0)),16) + 
					resize(unsigned(periphWordsLeft(31 downto 24)),16) + 
					resize(unsigned(periphWordsLeft(23 downto 16)),16) + 
					resize(unsigned(periphWordsLeft(15 downto 8)),16) + 
					resize(unsigned(periphWordsLeft(7 downto 0)),16) ));
				fifoData(23 downto 16) <= chkSum(15 downto 8);
				fifoData(31 downto 24) <= chkSum(7 downto 0);
				fifoWrreq <= (not periphRdempty) and (not fifoWrfull);
				periphRdreq <= '0';
				sendingPacket <= '0';
			when sLBData=>
				fifoData(31 downto 0) <= periphQ;
				fifoWrreq <= (not periphRdempty) and (not fifoWrfull);
				periphRdreq <= (not periphRdempty) and (not fifoWrfull);
				sendingPacket <= '0';
			when sLBWait=>
				fifoData(31 downto 0) <= (others => '0');
				fifoWrreq <= '0';
				periphRdreq <= '0';
				sendingPacket <= '0';
		end case;
	end process;
	
end rtl;
