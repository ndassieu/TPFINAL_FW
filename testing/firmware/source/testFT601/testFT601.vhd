
LIBRARY ieee;
USE ieee.std_logic_1164.all; 

LIBRARY work;
package testFT601StdLogicArray is
	type array_of_std_logic_vector_8 is array(natural range <>) of std_logic_vector(7 downto 0);
	type array_of_std_logic_vector_16 is array(natural range <>) of std_logic_vector(15 downto 0);
	type array_of_std_logic_vector_32 is array(natural range <>) of std_logic_vector(31 downto 0);
end package;


LIBRARY ieee;
USE ieee.std_logic_1164.all; 

use work.testFT601StdLogicArray.all;
use work.communicationArray.all;

ENTITY testFT601 IS 
	GENERIC 
	(
		-- Numero total de canales que maneja el bloque 
		-- 2^CHANNELS de RX y 2^CHANNELS de TX 
		CHANNELS: INTEGER := 2;
		PERIPH_COUNT: INTEGER := 2
	);
	PORT
	(
		userClk	 	: in		std_logic;
		-- Se�ales de las fifo's de transmisi�n
		txFifoWrclk	: out	std_logic_vector (2**CHANNELS-1 downto 0);
		txFifoWrreq	: out	std_logic_vector (2**CHANNELS-1 downto 0);
		txFifoWrfull: in	std_logic_vector (2**CHANNELS-1 downto 0);
		txFifoData	: out	comm_array_of_std_logic_vector_34(2**CHANNELS-1 downto 0);
		-- Se�ales de las fifo's de recepcici�n
		rxFifoRdclk	: out	std_logic_vector (2**CHANNELS-1 downto 0);
		rxFifoRdreq	: buffer	std_logic_vector (2**CHANNELS-1 downto 0);
		rxFifoRdempty: in	std_logic_vector (2**CHANNELS-1 downto 0);
		rxFifoQ		: in	comm_array_of_std_logic_vector_34(2**CHANNELS-1 downto 0);
		-- se�al de reset
		reset	 		: in		std_logic
	);
END testFT601;

ARCHITECTURE testFT601_arch OF testFT601 IS
	-- Se�ales para conectar deserializer con packetController
	signal chDeserSyncMode	: std_logic_vector (2**CHANNELS-1 downto 0);
	signal chDeserRdreq		: std_logic_vector (2**CHANNELS-1 downto 0);
	signal chDeserRdempty	: std_logic_vector (2**CHANNELS-1 downto 0);
	signal chDeserQ			: array_of_std_logic_vector_32(2**CHANNELS-1 downto 0);
	-- Se�ales para unir el packetController con el transmitController
	signal chPeriphAddr		: array_of_std_logic_vector_16 (2**CHANNELS-1 downto 0);
	signal chPeriphWordsLeft: array_of_std_logic_vector_32 (2**CHANNELS-1 downto 0);
	signal chPeriphAck		: std_logic_vector (2**CHANNELS-1 downto 0);
	signal chPeriphEn		: std_logic_vector (2**CHANNELS-1 downto 0);
	signal chPeriphRdreq	: std_logic_vector (2**CHANNELS-1 downto 0);
	signal chPeriphRdempty	: std_logic_vector (2**CHANNELS-1 downto 0);
	signal chPeriphQ		: array_of_std_logic_vector_32 (2**CHANNELS-1 downto 0);

	component deserializer is	
		port (
			clk	 			: in		std_logic;
			deserSyncMode	: in		std_logic;
			deserRdreq	 	: in 		std_logic;
			deserRdempty	: out 		std_logic;
			deserQ  		: out		std_logic_vector (31 downto 0);
			fifoRdreq		: buffer	std_logic;
			fifoRdempty		: in		std_logic;
			fifoQ			: in		std_logic_vector (33 downto 0);
			reset	 		: in		std_logic
		);
	end component;
	
	component packetController is
		port (
			clk	 			: in		std_logic;
			deserSyncMode	: buffer	std_logic;
			deserRdreq	 	: buffer	std_logic;
			deserRdempty	: in		std_logic;
			deserQ  		: in		std_logic_vector (31 downto 0);
			periphAddr		: out		std_logic_vector (15 downto 0);
			periphWordsLeft	: buffer	std_logic_vector (31 downto 0);
			periphAck		: in		std_logic;
			periphEn	 	: out 		std_logic;
			periphRdreq		: in		std_logic;
			periphRdempty	: out		std_logic;
			periphQ			: out		std_logic_vector (31 downto 0);
			reset	 		: in		std_logic
		);	
	end component;

	component peripherals IS 
		GENERIC 
		(
			-- Numero total de Perifericos 
			PERIPH_COUNT: NATURAL := 2;
			-- Configuraci�n de direcciones de los perifericos
			TX_CONTROLLER_ADDR 			: std_logic_vector(15 downto 0) := x"0001";
			CONFIG_REGISTER_ADDR 		: std_logic_vector(15 downto 0) := x"0004";
			-- Configuraiciones adicionales de los pereifericos
			TX_CONTROLLER_LOOPBACK_ADDR	: std_logic_vector(15 downto 0) := x"8001";
			TX_CONTROLLER_RAMP_ADDR		: std_logic_vector(15 downto 0) := x"8002";
			TX_CONTROLLER_BUFFERSIZE_ADDR: std_logic_vector(15 downto 0) := x"8003"
		);
		PORT
		(
			clk	 			: in		std_logic;
			periphAddr		: in		std_logic_vector (15 downto 0);
			periphWordsLeft	: in		std_logic_vector (31 downto 0);
			periphAck		: buffer	std_logic;
			periphEn	 	: in 		std_logic;
			periphRdreq		: out		std_logic;
			periphRdempty	: in		std_logic;
			periphQ			: in		std_logic_vector (31 downto 0);	
			fifoWrreq		: out		std_logic;
			fifoWrfull	 	: in		std_logic;
			fifoData  		: out		std_logic_vector (33 downto 0);
			reset	 		: in		std_logic
		);
	END component;
	
BEGIN 
	-- Conecta el clock de usuario con los clocks de la FIFO's
	clkDriver: FOR i IN 2**CHANNELS-1 DOWNTO 0 GENERATE
		txFifoWrclk(i) <= userClk;
		rxFifoRdclk(i) <= userClk;
	END GENERATE clkDriver; 

	-- Instanciamos los bloques deserializer
	testDeserializer: 
	for I in 0 to 2**CHANNELS-1 generate
		testDeserializer : deserializer 
		port map
		(
			clk => userClk,
			deserSyncMode => chdeserSyncMode(I),
			deserRdreq => chdeserRdreq(I),
			deserRdempty => chdeserRdempty(I),
			deserQ => chdeserQ(I),
			fifoRdreq => rxFifoRdreq(I),
			fifoRdempty => rxFifoRdempty(I),
			fifoQ => rxFifoQ(I),
			reset => reset
		);
	end generate testDeserializer;

	-- Instaciamos los bloques PacketController
	testPacketController: 
	for I in 0 to 2**CHANNELS-1 generate
		testPacketController : packetController 
		port map
		(
			clk => userClk,
			deserSyncMode => chDeserSyncMode(I),
			deserRdreq => chDeserRdreq(I),
			deserRdempty => chDeserRdempty(I),
			deserQ => chDeserQ(I),
			periphAddr => chPeriphAddr(I),
			periphWordsLeft => chPeriphWordsLeft(I),
			periphAck => chPeriphAck(I),
			periphEn => chPeriphEn(I),
			periphRdreq => chPeriphRdreq(I),
			periphRdempty => chPeriphRdempty(I),
			periphQ => chPeriphQ(I),
			reset => reset
		);
	end generate testPacketController;
	
	-- Instaciamos los bloques de perifericos
	testPeripherals: 
	for I in 0 to 2**CHANNELS-1 generate
		testPeripherals : peripherals 
		port map
		(
			clk => userClk,
			periphAddr => chPeriphAddr(I),
			periphWordsLeft => chPeriphWordsLeft(I),
			periphAck => chPeriphAck(I),
			periphEn => chPeriphEn(I),
			periphRdreq => chPeriphRdreq(I),
			periphRdempty => chPeriphRdempty(I),
			periphQ => chPeriphQ(I),
			fifoWrreq => txFifoWrreq(I),
			fifoWrfull => txFifoWrfull(I),
			fifoData => txFifoData(I),
			reset => reset
		);
	end generate testPeripherals;
	
	
END testFT601_arch;
