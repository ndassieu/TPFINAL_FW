-- Este modulo es el encargado de enviar los datos de la FIFO de
-- transmisi�n y colocarlos en el buffer de salida del FT601
-- para descripci�n sobre los estados consultar el archivo readme.md


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package packetControllerStdLogicArray is
  type array_of_std_logic_vector_8 is array(natural range <>) of std_logic_vector(7 downto 0);
end package;
use work.packetControllerStdLogicArray.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity packetController is
	port
	(
		clk	 			: in		std_logic;
		-- Se�ales de bloque del control 
		deserSyncMode	: buffer	std_logic;
		deserRdreq	 	: buffer	std_logic;
		deserRdempty	: in		std_logic;
		deserQ  		: in		std_logic_vector (31 downto 0);
		-- Se�ales para los perifericos
		periphAddr		: out		std_logic_vector (15 downto 0);
		periphWordsLeft	: buffer	std_logic_vector (31 downto 0);
		periphAck		: in		std_logic;
		periphEn	 	: out 		std_logic;
		periphRdreq		: in		std_logic;
		periphRdempty	: out		std_logic;
		periphQ			: out		std_logic_vector (31 downto 0);
		-- se�al de reset
		reset	 	: in		std_logic
	);
	
end entity;

architecture rtl of packetController is

	-- Build an enumerated type for the state machine
	type state_type is (sHeader1, sHeader2, sDesync, sPeripheral, sFlushData);
	
	-- Register to hold the current state
	signal state : state_type;
	
	-- Attribute "safe" implements a safe state machine.
	-- This is a state machine that can recover from an
	-- illegal state (by returning to the reset state).
	attribute syn_encoding : string;
	attribute syn_encoding of state_type : type is "safe";

	-- Se�ales internas
	signal last8Bytes 		: array_of_std_logic_vector_8 (7 downto 0);
	signal nextLast8Bytes 	: array_of_std_logic_vector_8 (7 downto 0);
	signal nextChkSum		: std_logic_vector (15 downto 0);
	signal chkSumOk			: std_logic;	
begin
	-- Se�ales con conecci�n directa
	periphQ <= deserQ;
	periphRdempty <= deserRdempty;
	
	-- genera periphWordsLeft
	process (clk, reset)
	begin
		if reset = '1' then
			periphWordsLeft <=  (others => '0');
		elsif rising_edge(clk) then
			if ((state=sDesync) or (state=sHeader2)) and 
					((chkSumOk='1') and (deserRdreq='1')) then
				periphWordsLeft(31 downto 24) <= nextLast8Bytes(5);
				periphWordsLeft(23 downto 16) <= nextLast8Bytes(4);
				periphWordsLeft(15 downto 8) <= nextLast8Bytes(3);
				periphWordsLeft(7 downto 0) <= nextLast8Bytes(2);
			elsif ((state=sPeripheral) or (state=sFlushData)) and
					(deserRdreq='1') and (periphWordsLeft/= (periphWordsLeft'range => '0')) then
				--periphWordsLeft <= periphWordsLeft-1;
				periphWordsLeft <= std_logic_vector(unsigned(periphWordsLeft) - 1);
			end if;
		end if;
	end process;
	
	-- genera periphAddr
	process (clk, reset)
	begin
		if reset = '1' then
			periphAddr <=  (others => '0');
		elsif rising_edge(clk) then
			if ((state=sDesync) or (state=sHeader2)) and 
					((chkSumOk='1') and (deserRdreq='1')) then
				periphAddr(15 downto 8) <= nextLast8Bytes(7);
				periphAddr(7 downto 0) <= nextLast8Bytes(6);
			end if;
		end if;
	end process;

	-- genera nextChkSym
	process (nextLast8Bytes)
	begin
		nextChkSum <= not (std_logic_vector(
				resize(unsigned(nextLast8Bytes(7)),nextChkSum'length) + 
				resize(unsigned(nextLast8Bytes(6)),nextChkSum'length) + 
				resize(unsigned(nextLast8Bytes(5)),nextChkSum'length) + 
				resize(unsigned(nextLast8Bytes(4)),nextChkSum'length) + 
				resize(unsigned(nextLast8Bytes(3)),nextChkSum'length) + 
				resize(unsigned(nextLast8Bytes(2)),nextChkSum'length) ));
	end process;
	
	-- genera se�al chkSumOk
	process (nextChkSum, nextLast8Bytes) 
	begin
		if (nextChkSum(15 downto 8) = nextLast8Bytes(1)) and 
				(nextChkSum(7 downto 0) = nextLast8Bytes(0)) then
			chkSumOk <= '1';
		else
			chkSumOk <= '0';
		end if;
	end process;
	
	-- genera se�al nextLast8Bytes
	process (deserSyncMode, last8bytes, deserQ) 
	begin
		if (deserSyncMode = '1') then
			nextLast8Bytes(7)<=last8Bytes(3);
			nextLast8Bytes(6)<=last8Bytes(2);
			nextLast8Bytes(5)<=last8Bytes(1);
			nextLast8Bytes(4)<=last8Bytes(0);
			nextLast8Bytes(3)<=deserQ(7 downto 0);
			nextLast8Bytes(2)<=deserQ(15 downto 8);
			nextLast8Bytes(1)<=deserQ(23 downto 16);
			nextLast8Bytes(0)<=deserQ(31 downto 24);
		else 
			nextLast8Bytes(7)<=last8Bytes(6);
			nextLast8Bytes(6)<=last8Bytes(5);
			nextLast8Bytes(5)<=last8Bytes(4);
			nextLast8Bytes(4)<=last8Bytes(3);
			nextLast8Bytes(3)<=last8Bytes(2);
			nextLast8Bytes(2)<=last8Bytes(1);
			nextLast8Bytes(1)<=last8Bytes(0);
			nextLast8Bytes(0)<=deserQ(7 downto 0);
		end if;
	end process;
	
	-- genera la se�al last8bytes
	process (clk, reset)
	begin
		if reset = '1' then
			last8bytes(7) <=  (others => '0');
			last8bytes(6) <=  (others => '0');
			last8bytes(5) <=  (others => '0');
			last8bytes(4) <=  (others => '0');
			last8bytes(3) <=  (others => '0');
			last8bytes(2) <=  (others => '0');
			last8bytes(1) <=  (others => '0');
			last8bytes(0) <=  (others => '0');
		elsif rising_edge(clk) then
			if deserRdreq = '1' then
				last8bytes <= nextLast8Bytes;
			end if;
		end if;
	end process;

	-- Maquina de estados
	process (clk, reset)
	begin
		if reset = '1' then
			state <= sHeader1; 
		elsif (rising_edge(clk)) then
			-- Determine the next state synchronously, based on
			-- the current state and the input
			case state is
				when sHeader1 =>
					if (deserRdreq = '1') then
						state <= sHeader2;
					end if;
				when sHeader2 =>
					if (deserRdreq = '1') and (chkSumOk='1') then
						state <= sPeripheral;
					elsif (deserRdreq = '1') and (chkSumOk='0') then
						state <= sDesync;
					end if;
				when sDesync =>
					if (deserRdreq = '1') and (chkSumOk='1') then
						state <= sPeripheral;
					end if;
				when sPeripheral =>
					if (periphAck = '0') then
						state <= sFlushData;
					elsif (periphWordsLeft = (periphWordsLeft'range => '0')) and
							(deserRdreq = '1') then
						state <= sHeader1;
					end if;
				when sFlushData =>
					if (periphWordsLeft = (periphWordsLeft'range => '0')) and 
							(deserRdreq = '1') then
						state <= sHeader1;
					end if;
				when others =>
					state  <= sHeader1;
			end case;
		end if;
	end process;
	
	-- Determine the output based only on the current state
	-- and the input (do not wait for a clock edge).
	process (state, deserRdempty, periphRdreq)
	begin
		case state is
			when sHeader1=>
				deserSyncMode <= '1';
				deserRdreq <=  not deserRdempty;
				periphEn <= '0';
			when sHeader2=>
				deserSyncMode <= '1';
				deserRdreq <=  not deserRdempty;
				periphEn <= '0';
			when sDesync=>
				deserSyncMode <= '0';
				deserRdreq <=  not deserRdempty;
				periphEn <= '0';
			when sPeripheral=>
				deserSyncMode <= '1';
				deserRdreq <=  periphRdreq;
				periphEn <= '1';
			when sFlushData=>
				deserSyncMode <= '1';
				deserRdreq <=  not deserRdempty;
				periphEn <= '0';
		end case;
	end process;
	
end rtl;
