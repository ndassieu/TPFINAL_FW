-- Este modulo es el encargado de enviar los datos de la FIFO de
-- transmisi�n y colocarlos en el buffer de salida del FT601
-- para descripci�n sobre los estados consultar el archivo readme.md
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package deserStdLogicArray is
  type array_of_std_logic_vector_8 is array(natural range <>) of std_logic_vector(7 downto 0);
end package;
use work.deserStdLogicArray.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity deserializer is	
	port
	(
		clk	 			: in		std_logic;
		-- Se�ales del bloque de control
		deserSyncMode	: in		std_logic;
		deserRdreq	 	: in 		std_logic;
		deserRdempty	: out 		std_logic;
		deserQ  		: out		std_logic_vector (31 downto 0);
		-- se�ales de manejo de la FIFO
		fifoRdreq		: buffer	std_logic;
		fifoRdempty		: in		std_logic;
		fifoQ			: in		std_logic_vector (33 downto 0);
		-- se�al de reset
		reset	 	: in		std_logic
	);
end entity;

architecture rtl of deserializer is
	-- Se�ales internas
	signal byteCount	 : integer range 0 to 7;
	signal NextByteCount : integer range -4 to 11;
	signal deserRdN		 : integer range 1 to 4;
	signal deserBuffer 	: array_of_std_logic_vector_8(6 downto 0);
begin
	-- genera se�al deserRdN
	process (deserSyncMode)
	begin
		if deserSyncMode = '1' then
			deserRdN <= 4;
		else 
			deserRdN <= 1;
		end if;
	end process;
	
	-- genera se�al NextByteCount
	process (fifoRdReq, deserRdN, deserRdReq, byteCount,fifoQ)
	begin
		if (fifoRdReq='1') and (deserRdReq='1') then
			NextByteCount <= byteCount+to_integer(unsigned(fifoQ(33 downto 32)))+1-deserRdN;
		elsif (fifoRdReq='1') then
			NextByteCount <= byteCount+to_integer(unsigned(fifoQ(33 downto 32)))+1;
		elsif (deserRdReq='1') then
			NextByteCount <= byteCount-deserRdN;
		else
			NextByteCount <= byteCount;
		end if;
	end process;
	
	-- genera la se�al byteCount y fifoQ
	process (clk, reset)
	begin
		if reset = '1' then
			byteCount <= 0;
			deserBuffer(6) <=  (others => '0');
			deserBuffer(5) <=  (others => '0');
			deserBuffer(4) <=  (others => '0');
			deserBuffer(3) <=  (others => '0');
			deserBuffer(2) <=  (others => '0');
			deserBuffer(1) <=  (others => '0');
			deserBuffer(0) <=  (others => '0');
			deserBuffer(0) <=  (others => '0');
		elsif rising_edge(clk) then
			byteCount <= NextByteCount;
			if (fifoRdreq = '1') and (fifoQ(33 downto 32)="00") then
				deserBuffer(6) <= deserBuffer(5);
				deserBuffer(5) <= deserBuffer(4);
				deserBuffer(4) <= deserBuffer(3);
				deserBuffer(3) <= deserBuffer(2);
				deserBuffer(2) <= deserBuffer(1);
				deserBuffer(1) <= deserBuffer(0);
				deserBuffer(0) <= fifoQ(7 downto 0);
			elsif (fifoRdreq = '1') and (fifoQ(33 downto 32)="01") then
				deserBuffer(6) <= deserBuffer(4);
				deserBuffer(5) <= deserBuffer(3);
				deserBuffer(4) <= deserBuffer(2);
				deserBuffer(3) <= deserBuffer(1);
				deserBuffer(2) <= deserBuffer(0);
				deserBuffer(1) <= fifoQ(7 downto 0);
				deserBuffer(0) <= fifoQ(15 downto 8);
			elsif (fifoRdreq = '1') and (fifoQ(33 downto 32)="10") then
				deserBuffer(6) <= deserBuffer(3);
				deserBuffer(5) <= deserBuffer(2);
				deserBuffer(4) <= deserBuffer(1);
				deserBuffer(3) <= deserBuffer(0);
				deserBuffer(2) <= fifoQ(7 downto 0);
				deserBuffer(1) <= fifoQ(15 downto 8);
				deserBuffer(0) <= fifoQ(23 downto 16);
			elsif (fifoRdreq = '1') and (fifoQ(33 downto 32)="11") then
				deserBuffer(6) <= deserBuffer(2);
				deserBuffer(5) <= deserBuffer(1);
				deserBuffer(4) <= deserBuffer(0);
				deserBuffer(3) <= fifoQ(7 downto 0);
				deserBuffer(2) <= fifoQ(15 downto 8);
				deserBuffer(1) <= fifoQ(23 downto 16);
				deserBuffer(0) <= fifoQ(31 downto 24);
			end if;
		end if;
	end process;
	
	-- Se�al deserEmpty
	process (byteCount, deserRdN) 
	begin
		if (byteCount>=deserRdN) then
			deserRdempty <= '0';
		else
			deserRdempty <= '1';
		end if;
	end process;
	
	-- Se�al fifoRdReq
	process (fifoRdempty, deserRdN, deserRdReq, byteCount,fifoQ)
	begin
		if (fifoRdempty = '1') then
			fifoRdreq <= '0';
		else
			if (deserRdReq = '1') then
				if (byteCount+to_integer(unsigned(fifoQ(33 downto 32)))+1-deserRdN<8) then
					fifoRdreq <= '1';
				else
					fifoRdreq <= '0';
				end if;
			else
				if (byteCount+to_integer(unsigned(fifoQ(33 downto 32)))+1<8) then
					fifoRdReq <= '1';
				else
					fifoRdreq <= '0';
				end if;
			end if;
		end if;
	end process;
	
	-- Se�al deserQ
	process (deserBuffer, byteCount) 
	begin
		if (byteCount >= 1) then
			deserQ(7 downto 0) <= deserBuffer(byteCount-1);
		else 
			deserQ(7 downto 0) <= (others => '0');
		end if;
		if (byteCount >= 2) then
			deserQ(15 downto 8) <= deserBuffer(byteCount-2);
		else 
			deserQ(15 downto 8) <= (others => '0');
		end if;
		if (byteCount >= 3) then
			deserQ(23 downto 16) <= deserBuffer(byteCount-3);
		else 
			deserQ(23 downto 16) <= (others => '0');
		end if;
		if (byteCount >= 4) then
			deserQ(31 downto 24) <= deserBuffer(byteCount-4);
		else 
			deserQ(31 downto 24) <= (others => '0');
		end if;
	end process;
end rtl;
