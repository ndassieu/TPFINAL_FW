
LIBRARY ieee;
USE ieee.std_logic_1164.all; 

LIBRARY work;
use work.communicationArray.all;

ENTITY top IS 
	GENERIC 
	(
		-- Numero total de canales que maneja el bloque 
		CHANNELS: INTEGER := 2;
		-- Tiempo para correr la politica de Round-robin
		ROUND_ROBIN_COUNTS: INTEGER := 8192
	);
	PORT
	(
		--Se�ales del UMFT601A
		CLK:		IN 	STD_LOGIC;
		RXF_N:	IN 	STD_LOGIC;
		WR_N:		OUT 	STD_LOGIC;
		DATA:		INOUT	STD_LOGIC_VECTOR (31 DOWNTO 0);
		BE:		INOUT STD_LOGIC_VECTOR ( 3 DOWNTO 0);
		TXE_N:  	IN  	STD_LOGIC;
		SRST_N:	IN		STD_LOGIC;
		WAKEUP_N:IN		STD_LOGIC;
		OE_N:  	OUT  	STD_LOGIC;
		RD_N:  	OUT  	STD_LOGIC;
		SIWU_N:  OUT  	STD_LOGIC;
		-- Reset por Hardware
		HRST_N:	IN		STD_LOGIC
	);
END top;

ARCHITECTURE top_arch OF top IS
	signal reset 			: std_logic;
	
	signal txFifoWrclk 	: std_logic_vector (2**CHANNELS-1 downto 0);
	signal txFifoWrreq	: std_logic_vector (2**CHANNELS-1 downto 0);
	signal txFifoWrfull	: std_logic_vector (2**CHANNELS-1 downto 0);
	signal txFifoData		: comm_array_of_std_logic_vector_34(2**CHANNELS-1 downto 0);
	signal rxFifoRdclk	: std_logic_vector (2**CHANNELS-1 downto 0);
	signal rxFifoRdreq	: std_logic_vector (2**CHANNELS-1 downto 0);
	signal rxFifoRdempty	: std_logic_vector (2**CHANNELS-1 downto 0);
	signal rxFifoQ			: comm_array_of_std_logic_vector_34(2**CHANNELS-1 downto 0);		

	component communication is
		GENERIC 
		(
			-- Numero total de canales que maneja el bloque 
			CHANNELS: INTEGER range 2 downto 0 := 0;
			-- Tiempo para correr la politica de Round-robin
			ROUND_ROBIN_COUNTS: INTEGER := 1024
		);
		
		port
		(
			-- Se�ales de bus del FT601 a arbitrar
			usbClk	 	: in		std_logic;
			usbWRn 	 	: out		std_logic;
			usbRXFn	 	: in 		std_logic;
			usbD	  		: inout		std_logic_vector (31 downto 0);
			usbBE 		: inout		std_logic_vector (3 downto 0);
			usbTXEn	 	: in		std_logic;
			usbOEn	 	: out		std_logic;
			usbRDn	 	: out		std_logic;
			usbSIWUn	 	: out		std_logic;
			-- Se�ales de las fifo's de transmisi�n
			txFifoWrclk	: in	std_logic_vector (2**CHANNELS-1 downto 0);
			txFifoWrreq	: in	std_logic_vector (2**CHANNELS-1 downto 0);
			txFifoWrfull: out	std_logic_vector (2**CHANNELS-1 downto 0);
			txFifoData	: in	comm_array_of_std_logic_vector_34(2**CHANNELS-1 downto 0);
			-- Se�ales de las fifo's de recepcici�n
			rxFifoRdclk	: in	std_logic_vector (2**CHANNELS-1 downto 0);
			rxFifoRdreq	: in	std_logic_vector (2**CHANNELS-1 downto 0);
			rxFifoRdempty: out	std_logic_vector (2**CHANNELS-1 downto 0);
			rxFifoQ		: out	comm_array_of_std_logic_vector_34(2**CHANNELS-1 downto 0);
			-- se�al de reset
			reset	 	: in		std_logic
		);
	end component;

	component testFT601 IS 
	GENERIC 
	(
		-- Numero total de canales que maneja el bloque 
		CHANNELS: INTEGER := 2
	);
	PORT
	(
		userClk	 	: in		std_logic;
		-- Se�ales de las fifo's de transmisi�n
		txFifoWrclk	: out	std_logic_vector (2**CHANNELS-1 downto 0);
		txFifoWrreq	: out	std_logic_vector (2**CHANNELS-1 downto 0);
		txFifoWrfull: in	std_logic_vector (2**CHANNELS-1 downto 0);
		txFifoData	: out	comm_array_of_std_logic_vector_34(2**CHANNELS-1 downto 0);
		-- Se�ales de las fifo's de recepcici�n
		rxFifoRdclk	: out	std_logic_vector (2**CHANNELS-1 downto 0);
		rxFifoRdreq	: out	std_logic_vector (2**CHANNELS-1 downto 0);
		rxFifoRdempty: in	std_logic_vector (2**CHANNELS-1 downto 0);
		rxFifoQ		: in	comm_array_of_std_logic_vector_34(2**CHANNELS-1 downto 0);
		-- se�al de reset
		reset	 		: in		std_logic
	);
	END component;
 
	
BEGIN 
	reset 	<= not HRST_N;
		
	communication1 : communication
	GENERIC MAP (
		CHANNELS => CHANNELS,
		ROUND_ROBIN_COUNTS => ROUND_ROBIN_COUNTS
	)
	PORT MAP(
		usbClk			=> CLK,
		usbWRn			=> WR_N,
		usbRXFn			=> RXF_N,
		usbD				=> DATA,
		usbBE				=> BE,
		usbTXEn			=> TXE_N,
		usbOEn			=> OE_N,
		usbRDn			=> RD_N,
		usbSIWUn			=> SIWU_N,
		txFifoWrclk 	=> txFifoWrclk,
		txFifoWrreq		=> txFifoWrreq,
		txFifoWrfull	=> txFifoWrfull,
		txFifoData		=> txFifoData,
		rxFifoRdclk		=> rxFifoRdclk,
		rxFifoRdreq		=> rxFifoRdreq,
		rxFifoRdempty	=> rxFifoRdempty,
		rxFifoQ			=> rxFifoQ,
		reset 			=> reset
	);

	testFT601_1 : testFT601
	GENERIC MAP (
		CHANNELS => CHANNELS
	)
	PORT MAP(
		userClk			=> CLK,
		txFifoWrclk		=> txFifoWrclk,
		txFifoWrreq		=> txFifoWrreq,
		txFifoWrfull	=> txFifoWrfull,
		txFifoData		=> txFifoData,
		rxFifoRdclk		=> rxFifoRdclk,
		rxFifoRdreq 	=> rxFifoRdreq,
		rxFifoRdempty	=> rxFifoRdempty,
		rxFifoQ			=> rxFifoQ,
		reset 			=> reset
	);
	
END top_arch;
