-- Copyright (C) 1991-2011 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "12/03/2012 13:50:45"
                                                            
-- Vhdl Test Bench template for design  :  usbWrite
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;  
USE ieee.numeric_std.all;   
use IEEE.STD_LOGIC_UNSIGNED.ALL;    

library work; 
use work.communicationArray.all;                       

ENTITY top_level_vhd_tst IS
GENERIC 
	(
		-- Cantidad de canales 2^CHANNELS
		constant CHANNELS : INTEGER := 1;
		-- Periodo del clock de lectura 
		constant FT601_CLK_PERIOD : time := 10 ns;
		-- Periodo del clock de escritura
		constant USR_CLK_PERIOD : time := 25 ns;
		-- Inputs Min Hold Time 
		constant IN_MIN_HOLD_TIME : time := 3.5 ns;
		-- Outputs Min Setup Time
		constant OUT_MIN_SETUP_TIME : time := 1 ns;
		
		constant TX_STIM_INC : std_logic_vector(31 downto 0) := x"04030201";
		constant TX_BYTES_STIM_INC : std_logic_vector(1 downto 0) := "01"
	);

END top_level_vhd_tst;
ARCHITECTURE top_level_vhd_arch OF top_level_vhd_tst IS
-- constants                                                 
-- signals  
SIGNAL usbClk 	: std_logic;
SIGNAL usbWRn 	: std_logic;
SIGNAL usbRXFn 	: std_logic;
SIGNAL usbD  	: std_logic_vector (31 downto 0);
SIGNAL usbBE	: std_logic_vector (3 downto 0);
SIGNAL usbTXEn	: std_logic;
SIGNAL usbSRSTn	: std_logic;
SIGNAL usbWAKEUPn: std_logic;
SIGNAL usbOEn	: std_logic;
SIGNAL usbRDn	: std_logic;
SIGNAL usbSIWUn	: std_logic;
-- se�al de reset
SIGNAL reset	 : std_logic;
SIGNAL RSTn	 : std_logic;

SIGNAL usrClk 	: std_logic;

signal tx_data_stim : std_logic_vector(31 downto 0);
signal tx_be_stim: std_logic_vector(1 downto 0);
signal tx_beDecoded_stim: std_logic_vector(3 downto 0);
signal rx_data_expected : std_logic_vector(31 downto 0);
signal rx_be_expected: std_logic_vector(1 downto 0);
signal rx_beDecoded_expected: std_logic_vector(3 downto 0);

--alias test is
-- <<signal .i1.pendingData : std_logic>>;
 
component top IS 
	GENERIC 
	(
		-- Numero total de canales que maneja el bloque 
		CHANNELS: INTEGER := 2;
		-- Tiempo para correr la politica de Round-robin
		ROUND_ROBIN_COUNTS: INTEGER := 1024
	);
	PORT
	(
		--Se�ales del UMFT601A
		CLK:		IN 	STD_LOGIC;
		RXF_N:	IN 	STD_LOGIC;
		WR_N:		OUT 	STD_LOGIC;
		DATA:		INOUT	STD_LOGIC_VECTOR (31 DOWNTO 0);
		BE:		INOUT STD_LOGIC_VECTOR ( 3 DOWNTO 0);
		TXE_N:  	IN  	STD_LOGIC;
		SRST_N:	IN		STD_LOGIC;
		WAKEUP_N:IN		STD_LOGIC;
		OE_N:  	OUT  	STD_LOGIC;
		RD_N:  	OUT  	STD_LOGIC;
		SIWU_N:  OUT  	STD_LOGIC;
		-- Reset por Hardware
		HRST_N:	IN		STD_LOGIC
	);
END component;

BEGIN
	i1 : top -- placa master
	GENERIC MAP (
		CHANNELS => 2,
		ROUND_ROBIN_COUNTS => 1024
	)
	PORT MAP (
-- list connections usbbeinbetween master ports and signals
		CLK => usbClk,
		RXF_N => usbRXFn,
		WR_N => usbWRn,
		DATA => usbD,
		BE => usbBE,
		TXE_N => usbTXEn,
		SRST_N => usbSRSTn,
		WAKEUP_N => usbWAKEUPn,
		OE_N => usbOEn,
		RD_N => usbRDn,
		SIWU_N => usbSIWUn,
		HRST_N => RSTn
	);

init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
        -- code that executes only once                      
WAIT;                                                       
END PROCESS init;                                                                                                                        


 
always : PROCESS 
	procedure stimFT601(
		constant usbRXFnStim 	: in	std_logic;
		constant usbDoe 		: in	std_logic_vector (3 downto 0);
		constant usbDStim 		: in	std_logic_vector (31 downto 0);
		constant usbBEoe 		: in	std_logic;
		constant usbBEStim 		: in	std_logic_vector (3 downto 0);
		constant usbTXEnStim 	: in 	std_logic
	) is
	begin
		usbRXFn <= usbRXFnStim;
		for I in 0 to 3 loop
			if (usbDoe(I) = '1') then
				usbD(8*(I+1)-1 downto 8*I) <= usbDStim(8*(I+1)-1 downto 8*I);
			else 
				usbD(8*(I+1)-1 downto 8*I) <= (others => 'Z');
			end if;
		end loop;
		if (usbBEoe = '1') then
			usbBE <= usbBEStim;
		else 
			usbBE <= (others => 'Z');
		end if;
		usbTXEn <= usbTXEnStim;
	end procedure stimFT601;
	
	procedure checkFT601(
		constant usbWRnExpected	: in	std_logic;
		constant usbDoeExpected	: in	std_logic_vector (3 downto 0);
		constant usbDExpected	: in    std_logic_vector (31 downto 0);
		constant usbBEoeExpected: in	std_logic;
		constant usbBEExpected	: in    std_logic_vector (3 downto 0)
	) is 
	begin
		assert usbWRn = usbWRnExpected 
		report
			"Unexpected result: " & "usbWRn =" & std_logic'image(usbWRn) & 
			"; " & "Expected = " & std_logic'image(usbWRnExpected)
		severity error;
		for I in 0 to 3 loop
			if (usbDoeExpected(I) = '1') then
				assert usbD(8*(I+1)-1 downto 8*I) = usbDExpected(8*(I+1)-1 downto 8*I) 
				report
					"Unexpected result: " & "usbD(" & integer'image(8*(I+1)-1) & ":" &
					integer'image(8*I) & " = " & 
					integer'image(to_integer(unsigned(usbD(8*(I+1)-1 downto 8*I)))) &
					"; " & "Expected = " & 
					integer'image(to_integer(unsigned(usbDExpected(8*(I+1)-1 downto 8*I))))
				severity error;
			end if;
		end loop;
		if (usbBEoeExpected = '1') then 
			assert usbBE = usbBEExpected
			report
				"Unexpected result: " & "q = " & integer'image(to_integer(unsigned(usbBE))) &
				"; " & "Expected = " & integer'image(to_integer(unsigned(usbBEExpected)))
			severity error;
		end if;
	end procedure checkFT601;
	
	procedure stimFT601AndCheck(
		-- Entradas de estimulo
		constant usbRXFnStim 	: in	std_logic;
		constant usbDoe 		: in	std_logic_vector (3 downto 0);
		constant usbDStim 		: in	std_logic_vector (31 downto 0);
		constant usbBEoe 		: in	std_logic;
		constant usbBEStim 		: in	std_logic_vector (3 downto 0);
		constant usbTXEnStim 	: in 	std_logic;
		-- Salidas esperadas
		constant usbWRnExpected	: in	std_logic;
		constant usbDoeExpected	: in	std_logic_vector (3 downto 0);
		constant usbDExpected	: in    std_logic_vector (31 downto 0);
		constant usbBEoeExpected: in	std_logic;
		constant usbBEExpected	: in    std_logic_vector (3 downto 0)
	) is 
	begin
		wait until rising_edge(usbClk);
		-- wait inputs hold time
		wait for IN_MIN_HOLD_TIME;
		--stimulate inputs
		stimFT601(usbRXFnStim, usbDoe, usbDStim,usbBEoe,usbBEStim,usbTXEnStim);
		--wait until output setup time to next clock
		wait for FT601_CLK_PERIOD-IN_MIN_HOLD_TIME-OUT_MIN_SETUP_TIME;
		--check expected output
		checkFT601(usbWRnExpected,usbDoeExpected,usbDExpected,usbBEoeExpected,usbBEExpected);
	end procedure stimFT601AndCheck;
	
	procedure waitForUsbWRnLow(
		constant timeout		 : in 	integer
	)is
	begin
		for I in 0 to timeout loop
			if usbWRn = '0' then
				exit;
			else
				wait until rising_edge(usbClk);
				wait for FT601_CLK_PERIOD-OUT_MIN_SETUP_TIME;
			end if;
		end loop;
		assert usbWRn = '0' 
		report
			"Unexpected result: " & "usbWRn = " & std_logic'image(usbWRn) & 
			"; " & "Expected = " & std_logic'image('0')
		severity error;
	end procedure waitForUsbWRnLow;

-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN	
	tx_data_stim <= TX_STIM_INC;
	tx_be_stim <= TX_BYTES_STIM_INC;
	rx_data_expected <= TX_STIM_INC;
	rx_be_expected <= TX_BYTES_STIM_INC;
	
	-- Estado inicial
	-- (usbRXFn, usbDoe, usbD, usbBEoe, usbBE, usbTXEn), --Inputs
	stimFT601('1', x"2", x"0000F000", '0', x"0", '0');
	
	-- Recibimos datos de la PC
	stimFT601AndCheck('1', x"2", x"0000E000", '0', x"0", '0',
			'1', "0000", x"00000000",'0',x"0");
	-- Leemos el canal 1
	waitForUsbWRnLow(10);
	-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	checkFT601('0', "0001", x"00000001",'1',x"0");
	-- (usbRXFn, usbDoeStim, usbD, usbBEoeStim, usbBE, usbTXEn), --Inputs
		-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0000", x"00000000",'0',x"0");
	-- Ponemos el Header, Largo de packete y crc
	stimFT601AndCheck('0', x"F",  x"00000400", '1', x"F", '1',
			'0', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('0', x"F", x"F8FF0300", '1', x"F", '1',
			'0', "0000", x"00000000",'0',x"0");	
	-- Escribimos el valor del largo de paquete 10
	stimFT601AndCheck('0', x"F", x"0A000000", '1', x"F", '1',
			'0', "0000", x"00000000",'0',x"0");	
	-- Escribimos el incrmento de la rampa 0x"00800201"
	stimFT601AndCheck('0', x"F", x"01028000", '1', x"F", '1',
			'0', "0000", x"00000000",'0',x"0");	
	-- Escribimos el divisor de frecuencia como 1 => clk/2
	stimFT601AndCheck('0', x"F", x"01000000", '1', x"F", '1',
			'0', "0000", x"00000000",'0',x"0");	
	-- Lanzamos el test de step de rampa 
	stimFT601AndCheck('0', x"F", x"82000000", '1', x"F", '1',
			'0', "0000", x"00000000",'0',x"0");	
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'1', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"2", x"0000F000", '0', x"0", '0',
		'1', "0000", x"00000000",'0',x"0");
		
	-- Leemos el canal 0
	waitForUsbWRnLow(50);
	-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	checkFT601('0', "0001", x"00000001",'1',x"1");
	-- (usbRXFn, usbDoeStim, usbD, usbBEoeStim, usbBE, usbTXEn), --Inputs
		-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0001", x"00000001",'1',x"1");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
			'0', "1111", x"00000280",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"73FF0A00",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"00000000",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"01028000",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"02040001",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"03068001",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"04080002",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"050A8002",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"060C0003",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"070E8003",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"08100004",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'1', "0000", x"00000000",'0', x"F");
	stimFT601AndCheck('1', x"2", x"0000F000", '0', x"0", '0',
		'1', "0000", x"00000000",'0',x"0");	
	waitForUsbWRnLow(50);
	-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	checkFT601('0', "0001", x"00000001",'1',x"1");
	-- (usbRXFn, usbDoeStim, usbD, usbBEoeStim, usbBE, usbTXEn), --Inputs
		-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0001", x"00000001",'1',x"1");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"09128004",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"0A140005",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"00000280",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"73FF0A00",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"00000000",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"01028000",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"02040001",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"03068001",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"04080002",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"050A8002",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"060C0003",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"070E8003",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"08100004",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"09128004",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'1', "0000", x"00000000",'0', x"F");
	stimFT601AndCheck('1', x"2", x"0000F000", '0', x"0", '0',
		'1', "0000", x"00000000",'0',x"0");	
	waitForUsbWRnLow(50);
	-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	checkFT601('0', "0001", x"00000001",'1',x"1");
	-- (usbRXFn, usbDoeStim, usbD, usbBEoeStim, usbBE, usbTXEn), --Inputs
		-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0001", x"00000001",'1',x"1");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"0A140005",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"00000280",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"73FF0A00",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"00000000",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"01028000",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"02040001",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"03068001",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"04080002",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"050A8002",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"060C0003",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"070E8003",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"08100004",'1', x"F");	
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"09128004",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"0A140005",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'1', "0000", x"00000000",'0', x"F");
	stimFT601AndCheck('1', x"2", x"0000E100", '0', x"0", '0',
		'1', "0000", x"00000000",'0',x"0");	
	
	
	-- Frenamos la adquisici�n
	waitForUsbWRnLow(10);
	-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	checkFT601('0', "0001", x"00000001",'1',x"0");
	-- (usbRXFn, usbDoeStim, usbD, usbBEoeStim, usbBE, usbTXEn), --Inputs
		-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0000", x"00000000",'0',x"0");
	-- Ponemos el Header, Largo de packete y crc
	stimFT601AndCheck('0', x"F",  x"00000400", '1', x"F", '1',
			'0', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('0', x"F", x"F8FF0300", '1', x"F", '1',
			'0', "0000", x"00000000",'0',x"0");	
	-- Escribimos el valor del largo de paquete 10
	stimFT601AndCheck('0', x"F", x"0A000000", '1', x"F", '1',
			'0', "0000", x"00000000",'0',x"0");	
	-- Escribimos el incrmento de la rampa 0x"00800201"
	stimFT601AndCheck('0', x"F", x"01028000", '1', x"F", '1',
			'0', "0000", x"00000000",'0',x"0");	
	-- Escribimos el divisor de frecuencia como 1 => clk/2
	stimFT601AndCheck('0', x"F", x"01000000", '1', x"F", '1',
			'0', "0000", x"00000000",'0',x"0");	
	-- Lanzamos el test de step de rampa 
	stimFT601AndCheck('0', x"F", x"02000000", '1', x"F", '1',
			'0', "0000", x"00000000",'0',x"0");	
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'1', "0000", x"00000000",'0',x"0");
	stimFT601AndCheck('1', x"2", x"0000F000", '0', x"0", '0',
		'1', "0000", x"00000000",'0',x"0");
	
	-- vaciamos lo que queda
	-- Leemos el canal 0
	waitForUsbWRnLow(50);
	-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	checkFT601('0', "0001", x"00000001",'1',x"1");
	-- (usbRXFn, usbDoeStim, usbD, usbBEoeStim, usbBE, usbTXEn), --Inputs
		-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
		'0', "0001", x"00000001",'1',x"1");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
			'0', "1111", x"00000280",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"73FF0A00",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"00000000",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"01028000",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"02040001",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"03068001",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"04080002",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"050A8002",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"060C0003",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"070E8003",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"08100004",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"09128004",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"0A140005",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"00000280",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"73FF0A00",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"00000000",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"01028000",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"02040001",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"03068001",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"04080002",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"050A8002",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"060C0003",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"070E8003",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"08100004",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"09128004",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'0', "1111", x"0A140005",'1', x"F");
	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
		'1', "0000", x"00000000",'0', x"F");
	stimFT601AndCheck('1', x"2", x"0000F000", '0', x"0", '0',
		'1', "0000", x"00000000",'0',x"0");	
	
	wait;
	-- Recibimos datos de la PC
	--stimFT601AndCheck('1', x"2", x"0000D000", '0', x"0", '0',
	--		'1', "0000", x"00000000",'0',x"0");
	---- Leemos el canal 1
	--waitForUsbWRnLow(10);
	---- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	--checkFT601('0', "0001", x"00000002",'1',x"0");
	---- (usbRXFn, usbDoeStim, usbD, usbBEoeStim, usbBE, usbTXEn), --Inputs
	--	-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	--stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
	--	'0', "0000", x"00000000",'0',x"0");
	--stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
	--	'0', "0000", x"00000000",'0',x"0");
	--for I in 0 to 3 loop
	--	stimFT601AndCheck('0', x"F", tx_data_stim, '1', tx_beDecoded_stim, '1',
	--		'0', "0000", x"00000000",'0',x"0");
	--	tx_data_stim <= tx_data_stim+TX_STIM_INC;
	--	tx_be_stim <= tx_be_stim+TX_BYTES_STIM_INC;
	--end loop;	
	--stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
	--	'0', "0000", x"00000000",'0',x"0");
	--stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
	--	'1', "0000", x"00000000",'0',x"0");
	--stimFT601AndCheck('1', x"2", x"0000F000", '0', x"0", '0',
	--	'1', "0000", x"00000000",'0',x"0");
	--	
	---- Leemos el canal 0
	--waitForUsbWRnLow(30);
	---- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	--checkFT601('0', "0001", x"00000002",'1',x"1");
	---- (usbRXFn, usbDoeStim, usbD, usbBEoeStim, usbBE, usbTXEn), --Inputs
	--	-- [usbWRn, usbDoeExpected, usbDExpected, usbBEoeExpected, usbBEExpected] -- Outputs
	--stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
	--	'0', "0001", x"00000002",'1',x"1");
	--for I in 0 to 3 loop
	--	stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
	--		'0', "1111", rx_data_expected,'1',rx_beDecoded_expected);
	--	rx_data_expected <= rx_data_expected+TX_STIM_INC;
	--	rx_be_expected <= rx_be_expected+TX_BYTES_STIM_INC;
	--end loop;
	--stimFT601AndCheck('0', x"0", x"00000000", '0', x"0", '1',
	--		'1', "0000", x"00000000",'0',x"0");
	--stimFT601AndCheck('1', x"0", x"00000000", '0', x"0", '1',
	--		'1', "0000", x"00000000",'0',x"0");
	--stimFT601AndCheck('1', x"2", x"0000F000", '0', x"0", '0',
	--		'1', "0000", x"00000000",'0',x"0");

		
	    WAIT;             
END PROCESS always;

decode_be_stim: process (tx_be_stim)
begin
	case tx_be_stim is
		when "00"=>
			tx_beDecoded_stim <= "0001";
		when "01"=>
			tx_beDecoded_stim <= "0011";
		when "10"=>
			tx_beDecoded_stim <= "0111";
		when "11"=>
			tx_beDecoded_stim <= "1111";
		when others=>
			tx_beDecoded_stim <= "0000";
	end case;
end process decode_be_stim;

decode_be_expected: process (rx_be_expected)
begin
	case rx_be_expected is
		when "00"=>
			rx_beDecoded_expected <= "0001";
		when "01"=>
			rx_beDecoded_expected <= "0011";
		when "10"=>
			rx_beDecoded_expected <= "0111";
		when "11"=>
			rx_beDecoded_expected <= "1111";
		when others=>
			rx_beDecoded_expected <= "0000";
	end case;
end process decode_be_expected;

RSTn <= not reset;

reset_gen : PROCESS
begin
	reset <= '1';
	wait until rising_edge(usbClk);
	wait until rising_edge(usrClk);
	reset <= '0';	
	wait;
end process reset_gen;

ft601_clk_gen : PROCESS
begin
	usbClk <= '1';
	-- slave_clk <= TRANSPORT usb_clk AFTER 10 ns;
	loop
		-- La F real es de 60Mhz pero corremos la simulaci�n
		-- 66Mhz para facilitar los calculos y ver que con un poco m�s anda
		wait for FT601_CLK_PERIOD/2;
		usbClk <= not usbClk;
	end loop;
end process ft601_clk_gen;

usr_clk_gen : PROCESS
begin
	usrClk <= '1';
	-- slave_clk <= TRANSPORT usb_clk AFTER 10 ns;
	loop
		-- La F real es de 60Mhz pero corremos la simulaci�n
		-- 66Mhz para facilitar los calculos y ver que con un poco m�s anda
		wait for USR_CLK_PERIOD/2;
		usrClk <= not usrClk;
	end loop;
end process usr_clk_gen;

END top_level_vhd_arch;

