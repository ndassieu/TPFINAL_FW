add wave -noupdate -label i_usbClk /top_level_vhd_tst/clk
add wave -noupdate -group CONTROLLER -label i_deserSyncMode /top_level_vhd_tst/deserSyncMode
add wave -noupdate -group CONTROLLER -label i_deserRdreq /top_level_vhd_tst/deserRdreq
add wave -noupdate -group CONTROLLER -label o_deserRdempty /top_level_vhd_tst/deserRdempty
add wave -noupdate -group CONTROLLER -label o_deserQ_B0 -radix hexadecimal {/top_level_vhd_tst/deserq(7 downto 0)}
add wave -noupdate -group CONTROLLER -label o_deserQ_B1 -radix hexadecimal {/top_level_vhd_tst/deserq(15 downto 8)}
add wave -noupdate -group CONTROLLER -label o_deserQ_B2 -radix hexadecimal {/top_level_vhd_tst/deserq(23 downto 16)}
add wave -noupdate -group CONTROLLER -label o_deserQ_B3 -radix hexadecimal {/top_level_vhd_tst/deserq(31 downto 24)}
add wave -noupdate -group FIFO -label o_fifoRdreq /top_level_vhd_tst/fifoRdreq
add wave -noupdate -group FIFO -label i_fifoRdempty /top_level_vhd_tst/fifoRdempty
add wave -noupdate -group FIFO -label i_fifoQ_B0 -radix hexadecimal {/top_level_vhd_tst/fifoQ(7 downto 0)}
add wave -noupdate -group FIFO -label i_fifoQ_B1 -radix hexadecimal {/top_level_vhd_tst/fifoQ(15 downto 8)}
add wave -noupdate -group FIFO -label i_fifoQ_B2 -radix hexadecimal {/top_level_vhd_tst/fifoQ(23 downto 16)}
add wave -noupdate -group FIFO -label i_fifoQ_B3 -radix hexadecimal {/top_level_vhd_tst/fifoQ(31 downto 24)}
add wave -noupdate -group FIFO -label i_fifoQ_Valid {/top_level_vhd_tst/fifoQ(33 downto 32)}
add wave -noupdate -label i_reset /top_level_vhd_tst/reset

add wave -noupdate -group DBG -label byteCount /top_level_vhd_tst/i1/byteCount 
add wave -noupdate -group DBG -label NextByteCount /top_level_vhd_tst/i1/NextByteCount 
add wave -noupdate -group DBG -label deserRdN /top_level_vhd_tst/i1/deserRdN 
add wave -noupdate -group DBG -label deserBuffer -radix hexadecimal /top_level_vhd_tst/i1/deserBuffer
