-- Copyright (C) 1991-2011 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "12/03/2012 13:50:45"
                                                            
-- Vhdl Test Bench template for design  :  usbWrite
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;  
USE ieee.numeric_std.all;   
use IEEE.STD_LOGIC_UNSIGNED.ALL;                           

ENTITY top_level_vhd_tst IS
GENERIC 
	(
		-- Periodo del clock 
		constant CLK_PERIOD : time := 10 ns;
		-- Inputs Min Hold Time 
		constant IN_MIN_HOLD_TIME : time := 3.5 ns;
		-- Outputs Min Setup Time
		constant OUT_MIN_SETUP_TIME : time := 1 ns
	);

END top_level_vhd_tst;
ARCHITECTURE deserializerTest_arch OF top_level_vhd_tst IS
-- constants                                                 
-- signals      
SIGNAL clk	 	: std_logic;
-- Se�ales del bloque de control
SIGNAL deserSyncMode: std_logic;
SIGNAL deserRdreq	: std_logic;
SIGNAL deserRdempty : std_logic;
SIGNAL deserQ  		: std_logic_vector (31 downto 0);
-- se�ales de manejo de la FIFO
SIGNAL fifoRdreq	: std_logic;
SIGNAL fifoRdempty	: std_logic;
SIGNAL fifoQ		: std_logic_vector (33 downto 0);
-- se�al de reset
SIGNAL reset	 	: std_logic;
	
--alias test is
-- <<signal .i1.pendingData : std_logic>>;
COMPONENT deserializer is	
	port
	(
		clk	 			: in		std_logic;
		-- Se�ales del bloque de control
		deserSyncMode	: in		std_logic;
		deserRdreq	 	: in 		std_logic;
		deserRdempty	: out 		std_logic;
		deserQ  		: out		std_logic_vector (31 downto 0);
		-- se�ales de manejo de la FIFO
		fifoRdreq		: out		std_logic;
		fifoRdempty		: in		std_logic;
		fifoQ			: in		std_logic_vector (33 downto 0);
		-- se�al de reset
		reset	 	: in		std_logic
	);
	
end COMPONENT;

BEGIN
	i1 : deserializer -- placa master
	PORT MAP (
-- list connections between master ports and signals
	clk => clk,
	deserSyncMode => deserSyncMode,
	deserRdreq => deserRdreq,
	deserRdempty => deserRdempty,
	deserQ => deserQ,
	fifoRdreq => fifoRdreq,
	fifoRdempty => fifoRdempty,
	fifoQ => fifoQ,
	reset => reset
	);

init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
        -- code that executes only once                      
WAIT;                                                       
END PROCESS init;                                                                                                                        


 
always : PROCESS 
	
	procedure stim(
		constant deserSyncModeStim	: in 	std_logic;
		constant deserRdreqStim	 	: in	std_logic;
		constant fifoRdemptyStim 	: in	std_logic;
		constant fifoQStim		 	: in	std_logic_vector (33 downto 0)
	) is
	begin
		deserSyncMode <= deserSyncModeStim;
		deserRdreq <= deserRdreqStim;
		fifoRdempty <= fifoRdemptyStim;
		fifoQ <= fifoQStim;
	end procedure stim;
    
	procedure stim_and_check(
		-- Entradas de estimulo
		constant deserSyncModeStim	: in 	std_logic;
		constant deserRdreqStim	 	: in	std_logic;
		constant fifoRdemptyStim 	: in	std_logic;
		constant fifoQStim		 	: in	std_logic_vector (33 downto 0);
		-- Salidas esperadas
		constant deserRdemptyExpeted : in	std_logic;
		constant deserQExpeted  	: in	std_logic_vector (31 downto 0);
		constant fifoRdreqExpeted	: in	std_logic
	) is 
	begin
		wait until rising_edge(clk);
		-- wait inputs hold time
		--wait for IN_MIN_HOLD_TIME;
		--stimulate inputs
		stim(deserSyncModeStim, deserRdreqStim, fifoRdemptyStim, fifoQStim);
		--wait until output setup time to next clock
		wait for CLK_PERIOD-IN_MIN_HOLD_TIME-OUT_MIN_SETUP_TIME;
		--check expected output
		assert deserRdempty = deserRdemptyExpeted 
		report
			"Unexpected result: " & "deserRdempty = " & std_logic'image(deserRdempty) &
			"; " & "expected = " & std_logic'image(deserRdemptyExpeted)
		severity error;
		assert fifoRdreq = fifoRdreqExpeted 
		report
			"Unexpected result: " & "fifoRdreq = " & std_logic'image(fifoRdreq) &
			"; " & "expeted = " & std_logic'image(fifoRdreqExpeted)
		severity error;
		if (deserSyncModeStim = '0') and (deserRdreqStim = '1') then
			assert deserQ(7 downto 0) = deserQExpeted(7 downto 0) 
			report
				"Unexpected result: " & "deserQ(7:0) = " & integer'image(to_integer(unsigned(deserQ(7 downto 0)))) &
				"; " & "expeted = " & integer'image(to_integer(unsigned(deserQExpeted(7 downto 0) )))
			severity error;
		elsif (deserSyncModeStim = '1') and (deserRdreqStim = '1') then
			assert deserQ(31 downto 0) = deserQExpeted(31 downto 0) 
			report
				"Unexpected result: " & "deserQ(31:0) = " & integer'image(to_integer(unsigned(deserQ(31 downto 0)))) &
				"; " & "expeted = " & integer'image(to_integer(unsigned(deserQExpeted(31 downto 0) )))
			severity error;
		end if;
	end procedure stim_and_check;

	procedure stim_and_check_Desyncronized (
		-- Entradas de estimulo
		constant deserRdreqStim	 	: in	std_logic;
		constant fifoRdemptyStim 	: in	std_logic;
		constant fifoQStim		 	: in	std_logic_vector (33 downto 0);
		-- Salidas esperadas
		constant deserRdemptyExpeted : in	std_logic;
		constant deserQExpeted  	: in	std_logic_vector (7 downto 0);
		constant fifoRdreqExpeted	: in	std_logic
	) is 
	begin 
		stim_and_check('0', deserRdreqStim, fifoRdemptyStim, fifoQStim, 
				deserRdemptyExpeted, x"000000" & deserQExpeted, fifoRdreqExpeted );
	end procedure;
	
	procedure stim_and_check_syncronized (
		-- Entradas de estimulo
		constant deserRdreqStim	 	: in	std_logic;
		constant fifoRdemptyStim 	: in	std_logic;
		constant fifoQStim		 	: in	std_logic_vector (33 downto 0);
		-- Salidas esperadas
		constant deserRdemptyExpeted : in	std_logic;
		constant deserQExpeted  	: in	std_logic_vector (31 downto 0);
		constant fifoRdreqExpeted	: in	std_logic
	) is 
	begin 
		stim_and_check('1', deserRdreqStim, fifoRdemptyStim, fifoQStim, 
				deserRdemptyExpeted, deserQExpeted, fifoRdreqExpeted );
	end procedure;
	
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN
	-- Estado inicial
	-- (deserSyncMode, deserRdreq, fifoRdempty, fifoQ) -- Inputs
	stim('0', '0', '1', (others => '1'));
	wait until reset='0';
	-- (deserRdreq, fifoRdempty, fifoQ) -- Inputs
		-- (deserRdemptyExpeted, deserQExpeted, fifoRdreqExpeted) -- Outputs
	stim_and_check_Desyncronized('0','1',"11"& x"FFFFFFFF", 
			'1', (others => '0'), '0');
	
	-- Test Desincronizado - fifoRdempty start, stop
	-- (deserRdreq, fifoRdempty, fifoQ) -- Inputs
		-- (deserRdemptyExpeted, deserQExpeted, fifoRdreqExpeted) -- Outputs
	stim_and_check_Desyncronized('0','0',"11"& x"03020100", 
			'1', x"FF", '1');
	stim_and_check_Desyncronized('1','1',"11"& x"FFFFFFFF", 
			'0', x"00", '0');
	stim_and_check_Desyncronized('1','1',"11"& x"FFFFFFFF", 
			'0', x"01", '0');
	stim_and_check_Desyncronized('1','1',"11"& x"FFFFFFFF", 
			'0', x"02", '0');
	stim_and_check_Desyncronized('1','1',"11"& x"FFFFFFFF", 
			'0', x"03", '0');
	stim_and_check_Desyncronized('0','1',"11"& x"FFFFFFFF", 
			'1', x"FF", '0');
	stim_and_check_Desyncronized('0','0',"01"& x"F3F20504", 
			'1', x"FF", '1');
	stim_and_check_Desyncronized('1','1',"11"& x"FFFFFFFF", 
			'0', x"04", '0');
	stim_and_check_Desyncronized('1','1',"11"& x"FFFFFFFF", 
			'0', x"05", '0');
	stim_and_check_Desyncronized('0','1',"11"& x"FFFFFFFF", 
			'1', x"FF", '0');
	
	-- Test Desincronizado - deserRdreq start, stop
	-- (deserRdreq, fifoRdempty, fifoQ) -- Inputs
		-- (deserRdemptyExpeted, deserQExpeted, fifoRdreqExpeted) -- Outputs
	stim_and_check_Desyncronized('0','0',"11"& x"03020100", 
			'1', (others => '0'), '1');
	stim_and_check_Desyncronized('1','0',"11"& x"07060504", 
			'0', x"00", '1');
	stim_and_check_Desyncronized('1','0',"11"& x"0B0A0908", 
			'0', x"01", '0');
	stim_and_check_Desyncronized('1','0',"11"& x"0B0A0908", 
			'0', x"02", '0');
	stim_and_check_Desyncronized('1','0',"11"& x"0B0A0908", 
			'0', x"03", '0');
	stim_and_check_Desyncronized('1','0',"11"& x"0B0A0908", 
			'0', x"04", '1');
	stim_and_check_Desyncronized('1','0',"11"& x"0F0E0D0C", 
			'0', x"05", '0');
	stim_and_check_Desyncronized('1','0',"11"& x"0F0E0D0C", 
			'0', x"06", '0');
	stim_and_check_Desyncronized('1','0',"11"& x"0F0E0D0C", 
			'0', x"07", '0');
	stim_and_check_Desyncronized('1','0',"11"& x"0F0E0D0C", 
			'0', x"08", '1');
	stim_and_check_Desyncronized('0','0',"11"& x"13121110", 
			'0', x"FF", '0');

	-- Pasaje de modo desincronizado a modo sincronizado
	-- (deserRdreq, fifoRdempty, fifoQ) -- Inputs
		-- (deserRdemptyExpeted, deserQExpeted, fifoRdreqExpeted) -- Outputs
	stim_and_check_Desyncronized('0','0',"11"& x"13121110", 
			'0', x"FF", '0');
	stim_and_check_Desyncronized('1','0',"11"& x"13121110", 
			'0', x"09", '0');
	stim_and_check_Desyncronized('1','0',"11"& x"13121110", 
			'0', x"0A", '0');
	stim_and_check_Desyncronized('1','0',"11"& x"13121110", 
			'0', x"0B", '0');
	-- (deserRdreq, fifoRdempty, fifoQ) -- Inputs
		-- (deserRdemptyExpeted, deserQExpeted, fifoRdreqExpeted) -- Outputs
	stim_and_check_syncronized('1','0',"11"& x"13121110", 
			'0', x"0F0E0D0C", '1');
	stim_and_check_syncronized('1','0',"11"& x"17161514", 
			'0', x"13121110", '1');
	stim_and_check_syncronized('0','0',"11"& x"24232221", 
			'0', x"FFFFFFFF", '0');
			
	-- deserRdreq start stop
	-- (deserRdreq, fifoRdempty, fifoQ) -- Inputs
		-- (deserRdemptyExpeted, deserQExpeted, fifoRdreqExpeted) -- Outputs
	stim_and_check_syncronized('0','0',"11"& x"24232221", 
			'0', x"FFFFFFFF", '0');
	stim_and_check_syncronized('1','0',"11"& x"24232221", 
			'0', x"17161514", '1');
	stim_and_check_syncronized('0','0',"11"& x"28272625", 
			'0', x"FFFFFFFF", '0');
	stim_and_check_syncronized('0','0',"11"& x"28272625", 
			'0', x"FFFFFFFF", '0');
	stim_and_check_syncronized('0','0',"11"& x"28272625", 
			'0', x"FFFFFFFF", '0');
	stim_and_check_syncronized('1','0',"11"& x"28272625", 
			'0', x"24232221", '1');
	stim_and_check_syncronized('1','0',"11"& x"2C2B2A29", 
			'0', x"28272625", '1');
	stim_and_check_syncronized('0','1',"11"& x"FFFFFFFF", 
			'0', x"FFFFFFFF", '0');
	stim_and_check_syncronized('1','1',"11"& x"FFFFFFFF", 
			'0', x"2C2B2A29", '0');
	stim_and_check_syncronized('0','1',"11"& x"FFFFFFFF", 
			'1', x"FFFFFFFF", '0');
	
	-- fifoRdEmpty start stop
	-- (deserRdreq, fifoRdempty, fifoQ) -- Inputs
		-- (deserRdemptyExpeted, deserQExpeted, fifoRdreqExpeted) -- Outputs
	stim_and_check_syncronized('0','1',"11"& x"FFFFFFFF", 
			'1', x"FFFFFFFF", '0');
	stim_and_check_syncronized('0','0',"00"& x"FFFFFF00", 
			'1', x"FFFFFFFF", '1');
	stim_and_check_syncronized('0','0',"11"& x"04030201", 
			'1', x"FFFFFFFF", '1');
	stim_and_check_syncronized('1','0',"11"& x"08070605", 
			'0', x"03020100", '1');
	stim_and_check_syncronized('1','0',"00"& x"FFFFFF09", 
			'0', x"07060504", '1');
	stim_and_check_syncronized('0','1',"11"& x"FFFFFFFF", 
			'1', x"FFFFFFFF", '0');
	stim_and_check_syncronized('0','1',"11"& x"FFFFFFFF", 
			'1', x"FFFFFFFF", '0');
	stim_and_check_syncronized('0','0',"01"& x"FFFF0B0A", 
			'1', x"FFFFFFFF", '1');
	stim_and_check_syncronized('1','1',"11"& x"FFFFFFFF", 
			'0', x"0B0A0908", '0');
	stim_and_check_syncronized('0','1',"11"& x"FFFFFFFF", 
			'1', x"FFFFFFFF", '0');
WAIT;                                                        
END PROCESS always;

reset_gen : process
begin
	reset <= '1';
	wait until rising_edge(clk);
	wait until rising_edge(clk);
	reset <= '0';
	wait;
end process;

clk_gen : PROCESS
begin
	clk <= '1';
	-- slave_clk <= TRANSPORT usb_clk AFTER 10 ns;
	loop
		-- La F real es de 60Mhz pero corremos la simulaci�n
		-- 66Mhz para facilitar los calculos y ver que con un poco m�s anda
		wait for CLK_PERIOD/2;
		clk <= not clk;
	end loop;
end process clk_gen;


END deserializerTest_arch;

