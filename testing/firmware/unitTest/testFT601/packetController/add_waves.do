add wave -noupdate -label i_usbClk /top_level_vhd_tst/clk
add wave -noupdate -group CONTROLLER -label o_deserSyncMode /top_level_vhd_tst/deserSyncMode
add wave -noupdate -group CONTROLLER -label o_deserRdreq /top_level_vhd_tst/deserRdreq
add wave -noupdate -group CONTROLLER -label i_deserRdempty /top_level_vhd_tst/deserRdempty
add wave -noupdate -group CONTROLLER -label i_deserQ_B0 -radix hexadecimal {/top_level_vhd_tst/deserq(7 downto 0)}
add wave -noupdate -group CONTROLLER -label i_deserQ_B1 -radix hexadecimal {/top_level_vhd_tst/deserq(15 downto 8)}
add wave -noupdate -group CONTROLLER -label i_deserQ_B2 -radix hexadecimal {/top_level_vhd_tst/deserq(23 downto 16)}
add wave -noupdate -group CONTROLLER -label i_deserQ_B3 -radix hexadecimal {/top_level_vhd_tst/deserq(31 downto 24)}

add wave -noupdate -group PERIPH -label o_periphAddr -radix decimal /top_level_vhd_tst/periphAddr
add wave -noupdate -group PERIPH -label o_periphWordsLeft -radix decimal /top_level_vhd_tst/periphWordsLeft
add wave -noupdate -group PERIPH -label i_periphAck /top_level_vhd_tst/periphAck
add wave -noupdate -group PERIPH -label o_periphEn /top_level_vhd_tst/periphEn
add wave -noupdate -group PERIPH -label i_periphRdreq /top_level_vhd_tst/periphRdreq
add wave -noupdate -group PERIPH -label o_periphRdempty /top_level_vhd_tst/periphRdempty
add wave -noupdate -group PERIPH -label o_periphQ -radix hexadecimal /top_level_vhd_tst/periphQ

add wave -noupdate -label i_reset /top_level_vhd_tst/reset

add wave -noupdate -group DBG -label state /top_level_vhd_tst/i1/state
add wave -noupdate -group DBG -label chkSumOk /top_level_vhd_tst/i1/chkSumOk
add wave -noupdate -group DBG -label nextLast8Bytes -radix hexadecimal /top_level_vhd_tst/i1/nextLast8Bytes 
add wave -noupdate -group DBG -label last8Bytes -radix hexadecimal /top_level_vhd_tst/i1/last8Bytes 
add wave -noupdate -group DBG -label nextChkSum /top_level_vhd_tst/i1/nextChkSum