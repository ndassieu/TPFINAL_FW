transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

# Top level files
vcom -93 -work work {../../../source/testFT601/packetController/packetController.vhd}
