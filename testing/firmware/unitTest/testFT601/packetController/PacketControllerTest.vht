-- Copyright (C) 1991-2011 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "12/03/2012 13:50:45"
                                                            
-- Vhdl Test Bench template for design  :  usbWrite
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;  
USE ieee.numeric_std.all;   
use IEEE.STD_LOGIC_UNSIGNED.ALL;                           

ENTITY top_level_vhd_tst IS
GENERIC 
	(
		-- Periodo del clock 
		constant CLK_PERIOD : time := 10 ns;
		-- Inputs Min Hold Time 
		constant IN_MIN_HOLD_TIME : time := 3.5 ns;
		-- Outputs Min Setup Time
		constant OUT_MIN_SETUP_TIME : time := 1 ns
	);

END top_level_vhd_tst;
ARCHITECTURE packetControllerTest_arch OF top_level_vhd_tst IS
-- constants                                                 
-- signals      
SIGNAL clk	 	: std_logic;
-- Se�ales del bloque de control
SIGNAL deserSyncMode: std_logic;
SIGNAL deserRdreq	: std_logic;
SIGNAL deserRdempty : std_logic;
SIGNAL deserQ  		: std_logic_vector (31 downto 0);
-- Se�ales para los perifericos
SIGNAL periphAddr		: std_logic_vector (15 downto 0);
SIGNAL periphWordsLeft	: std_logic_vector (31 downto 0);
SIGNAL periphAck		: std_logic;
SIGNAL periphEn	 		: std_logic;
SIGNAL periphRdreq		: std_logic;
SIGNAL periphRdempty	: std_logic;
SIGNAL periphQ			: std_logic_vector (31 downto 0);
-- se�al de reset
SIGNAL reset	 	: std_logic;
	
--alias test is
-- <<signal .i1.pendingData : std_logic>>;
COMPONENT packetController is	
	port
	(
		clk	 			: in		std_logic;
		-- Se�ales de bloque del control 
		deserSyncMode	: out		std_logic;
		deserRdreq	 	: out 		std_logic;
		deserRdempty	: in		std_logic;
		deserQ  		: in		std_logic_vector (31 downto 0);
		-- Se�ales para los perifericos
		periphAddr		: out		std_logic_vector (15 downto 0);
		periphWordsLeft	: out		std_logic_vector (31 downto 0);
		periphAck		: in		std_logic;
		periphEn	 	: out 		std_logic;
		periphRdreq		: in		std_logic;
		periphRdempty	: out		std_logic;
		periphQ			: out		std_logic_vector (31 downto 0);
		-- se�al de reset
		reset	 		: in		std_logic
	);
	
end COMPONENT;

BEGIN
	i1 : packetController -- placa master
	PORT MAP (
	-- list connections between master ports and signals
		clk => clk,
		deserSyncMode => deserSyncMode,
		deserRdreq => deserRdreq,
		deserRdempty => deserRdempty,
		deserQ => deserQ,
		periphAddr => periphAddr,
		periphWordsLeft => periphWordsLeft,
		periphAck => periphAck,
		periphEn => periphEn,
		periphRdreq => periphRdreq,
		periphRdempty => periphRdempty,
		periphQ => periphQ,
		reset => reset
	);

init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
        -- code that executes only once                      
WAIT;                                                       
END PROCESS init;                                                                                                                        


 
always : PROCESS 
	procedure stim(
		constant deserRdemptyStim	: in 	std_logic;
		constant deserQStim	 		: in	std_logic_vector (31 downto 0);
		constant periphAckStim 		: in	std_logic;
		constant periphRdreqStim	: in	std_logic
	) is
	begin
		deserRdempty <= deserRdemptyStim;
		deserQ <= deserQStim;
		periphAck <= periphAckStim;
		periphRdreq <= periphRdreqStim;
	end procedure stim;

	
    
	procedure stim_and_check(
		-- Entradas de estimulo
		constant deserRdemptyStim	: in 	std_logic;
		constant deserQStim	 		: in	std_logic_vector (31 downto 0);
		constant periphAckStim 		: in	std_logic;
		constant periphRdreqStim	: in	std_logic;
		-- Salidas esperadas
		constant deserSyncModeExpected	: in		std_logic;
		constant deserRdreqExpected	 	: in 		std_logic;
		constant periphAddrExpected		: in		std_logic_vector (15 downto 0);
		constant periphWordsLeftExpected: in		std_logic_vector (31 downto 0);
		constant periphEnExpected	 	: in 		std_logic
	) is 
	
	begin
		wait until rising_edge(clk);
		-- wait inputs hold time
		--wait for IN_MIN_HOLD_TIME;
		--stimulate inputs
		stim(deserRdemptyStim, deserQStim, periphAckStim, periphRdreqStim);
		--wait until output setup time to next clock
		wait for CLK_PERIOD-IN_MIN_HOLD_TIME-OUT_MIN_SETUP_TIME;
		--check expected output
		assert deserSyncMode = deserSyncModeExpected 
		report
			"Unexpected result: " & "deserSyncMode = " & std_logic'image(deserSyncMode) &
			"; " & "expected = " & std_logic'image(deserSyncModeExpected)
		severity error;
		assert deserRdreq = deserRdreqExpected 
		report
			"Unexpected result: " & "deserRdreq = " & std_logic'image(deserRdreq) &
			"; " & "expeted = " & std_logic'image(deserRdreqExpected)
		severity error;
		if (periphEnExpected = '1') then
			assert periphAddr = periphAddrExpected
			report
				"Unexpected result: " & "periphAddr = " & integer'image(to_integer(unsigned(periphAddr))) &
				"; " & "expeted = " & integer'image(to_integer(unsigned(periphAddrExpected )))
			severity error;
		end if;
		if (periphEnExpected = '1') then
			assert periphWordsLeft = periphWordsLeftExpected
			report
				"Unexpected result: " & "periphWordsLeft = " & integer'image(to_integer(unsigned(periphWordsLeft))) &
				"; " & "expeted = " & integer'image(to_integer(unsigned(periphWordsLeftExpected )))
			severity error;
		end if;
		assert periphEn = periphEnExpected 
		report
			"Unexpected result: " & "periphEn = " & std_logic'image(periphEn) &
			"; " & "expeted = " & std_logic'image(periphEnExpected)
		severity error;
		if (periphEnExpected = '1') then
			assert periphRdempty = deserRdemptyStim 
			report
				"Unexpected result: " & "periphRdempty = " & std_logic'image(periphRdempty) &
				"; " & "expeted = " & std_logic'image(deserRdemptyStim)
			severity error;
		end if;
		if (periphEnExpected = '1') then
			assert periphQ = deserQStim
			report
				"Unexpected result: " & "periphQ = " & integer'image(to_integer(unsigned(periphQ))) &
				"; " & "expeted = " & integer'image(to_integer(unsigned(deserQStim )))
			severity error;
		end if;
	end procedure stim_and_check;
	
	procedure stim_and_check_noPeripherals(
		-- Entradas de estimulo
		constant deserRdemptyStim	: in 	std_logic;
		constant deserQStim	 		: in	std_logic_vector (31 downto 0);
		-- Salidas esperadas
		constant deserSyncModeExpected	: in		std_logic;
		constant deserRdreqExpected	 	: in 		std_logic
		) is 
	begin
		stim_and_check(deserRdemptyStim, deserQStim, '0', '0',
				deserSyncModeExpected, deserRdreqExpected, (others => '0'), 
				(others => '0'), '0');
	end procedure;
	
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN
	-- Estado inicial
	-- (deserRdemptyStim, deserQStim, periphAckStim,periphRdreqStim) -- Inputs
	stim('1', (others => '0'), '0', '0');
	wait until reset='0';
	-- (deserRdemptyStim, deserQStim) -- Inputs
		-- (deserSyncModeExpected, deserRdreqExpected) -- Outputs
	stim_and_check_noPeripherals('1', x"FFFFFFFF", '1', '0');
	
	-- Probamos la perida de syncronismo y recuperaci�n del mismo
	-- (deserRdemptyStim, deserQStim) -- Inputs
		-- (deserSyncModeExpected, deserRdreqExpected) -- Outputs
	stim_and_check_noPeripherals('0', x"F3F2F1F0", '1', '1');
	stim_and_check_noPeripherals('0', x"F7F6F5F4", '1', '1');
	stim_and_check_noPeripherals('0', x"FAF9F800", '0', '1'); -- Header 0x0001
	stim_and_check_noPeripherals('0', x"FDFCFB01", '0', '1');
	stim_and_check_noPeripherals('0', x"80FFFE00", '0', '1'); -- Length 0x00000001
	stim_and_check_noPeripherals('0', x"83828100", '0', '1');
	stim_and_check_noPeripherals('0', x"86858400", '0', '1');
	stim_and_check_noPeripherals('0', x"89888701", '0', '1');
	stim_and_check_noPeripherals('0', x"8C8B8AFF", '0', '1'); -- crc = ~0x0002 = FFFD
	stim_and_check_noPeripherals('0', x"8F8E8DFD", '0', '1');
	-- (deserRdemptyStim, deserQStim, periphAckStim, periphRdreqStim) -- Inputs
		-- (deserSyncModeExpected, deserRdreqExpected) -- Outputs
		--periphAddrExpected, periphWordsLeftExpected, periphEnExpected) -- Outputs
	stim_and_check('0', x"04030201", 
			'1', '1', 
			'1', '1', x"0001", x"00000001", '1');
	stim_and_check('0', x"08070605", 
			'1', '1', 
			'1', '1', x"0001", x"00000000", '1');
	stim_and_check_noPeripherals('1', x"00000000", '1', '0');
	
	-- Probamos el flush de datos
	-- (deserRdemptyStim, deserQStim) -- Inputs
		-- (deserSyncModeExpected, deserRdreqExpected) -- Outputs
	stim_and_check_noPeripherals('1', x"00000000", '1', '0');
	-- Header= 0x0001, Length = 0x00000004, crc=~0x0005=FFFA
	stim_and_check_noPeripherals('0', x"00000100", '1', '1'); 
	stim_and_check_noPeripherals('0', x"FAFF0400", '1', '1'); 
	-- (deserRdemptyStim, deserQStim, periphAckStim, periphRdreqStim) -- Inputs
		-- (deserSyncModeExpected, deserRdreqExpected) -- Outputs
		--periphAddrExpected, periphWordsLeftExpected, periphEnExpected) -- Outputs
	stim_and_check('0', x"04030201", '0', '0', 
			'1', '0', x"0001", x"00000004", '1');
	stim_and_check_noPeripherals('0', x"04030201", '1', '1'); -- leemos 5 bytes
	stim_and_check_noPeripherals('0', x"40302010", '1', '1'); -- leemos 5 bytes
	stim_and_check_noPeripherals('0', x"04030201", '1', '1'); -- leemos 5 bytes
	stim_and_check_noPeripherals('0', x"40302010", '1', '1'); -- leemos 5 bytes
	stim_and_check_noPeripherals('0', x"04030201", '1', '1'); -- leemos 5 bytes
	stim_and_check_noPeripherals('1', x"00000000", '1', '0'); -- Fin de transaccion
	
	-- El periferico tarda en leer los datos
	stim_and_check_noPeripherals('1', x"00000000", '1', '0'); -- Fin de transaccion
	stim_and_check_noPeripherals('1', x"00000000", '1', '0'); -- Fin de transaccion
	-- Header= 0x0002, Length = 0x00000004, crc=~0x0006=FFF9
	stim_and_check_noPeripherals('0', x"00000200", '1', '1'); 
	stim_and_check_noPeripherals('0', x"F9FF0400", '1', '1'); 
	-- (deserRdemptyStim, deserQStim, periphAckStim, periphRdreqStim) -- Inputs
		-- (deserSyncModeExpected, deserRdreqExpected) -- Outputs
		--periphAddrExpected, periphWordsLeftExpected, periphEnExpected) -- Outputs
	-- lee primer byte
	stim_and_check('0', x"01000080", '1', '1', 
			'1', '1', x"0002", x"00000004", '1');
	-- espera 2 bytes sin leer
	stim_and_check('0', x"01325476", '1', '0', 
			'1', '0', x"0002", x"00000003", '1');
	stim_and_check('0', x"01325476", '1', '0', 
			'1', '0', x"0002", x"00000003", '1');
	-- leemos los bytes restantes
	stim_and_check('0', x"02000080", '1', '1', 
			'1', '1', x"0002", x"00000003", '1');
	stim_and_check('0', x"03008000", '1', '1', 
			'1', '1', x"0002", x"00000002", '1');
	stim_and_check('0', x"04800000", '1', '1', 
			'1', '1', x"0002", x"00000001", '1');
	stim_and_check('0', x"05000000", '1', '1', 
			'1', '1', x"0002", x"00000000", '1');
	stim_and_check_noPeripherals('1', x"00000000", '1', '0'); -- Fin de transaccion
	stim_and_check_noPeripherals('1', x"00000000", '1', '0'); -- Fin de transaccion
			
WAIT;                                                        
END PROCESS always;

reset_gen : process
begin
	reset <= '1';
	wait until rising_edge(clk);
	wait until rising_edge(clk);
	reset <= '0';
	wait;
end process;

clk_gen : PROCESS
begin
	clk <= '1';
	-- slave_clk <= TRANSPORT usb_clk AFTER 10 ns;
	loop
		-- La F real es de 60Mhz pero corremos la simulaci�n
		-- 66Mhz para facilitar los calculos y ver que con un poco m�s anda
		wait for CLK_PERIOD/2;
		clk <= not clk;
	end loop;
end process clk_gen;


END packetControllerTest_arch;

