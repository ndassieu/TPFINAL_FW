transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

# Top level files
vcom -93 -work work {../../../source/source/comunication/common/stdLogicArrayPkg.vhd}
vcom -93 -work work {../../../source/source/comunication/usbRead/usbRead.vhd}
vcom -93 -work work {../../../source/source/comunication/usbWrite/usbWrite.vhd}
vcom -93 -work work {../../../source/source/comunication/usbFIFO/usbFIFO.vhd}
vcom -93 -work work {../../../source/source/comunication/usbArbitrer/usbArbitrer.vhd}
vcom -93 -work work {../../../source/source/comunication/communicationArrays.vhd}
vcom -93 -work work {../../../source/source/comunication/communication.vhd}
vcom -93 -work work {../source/testFT601/testFT601.vhd}
vcom -93 -work work {../source/testFT601/deserializer/deserializer.vhd}
vcom -93 -work work {../source/testFT601/packetController/packetController.vhd}
vcom -93 -work work {../source/testFT601/peripherals/peripherals.vhd}
vcom -93 -work work {../source/testFT601/peripherals/periphMux/periphMux.vhd}
vcom -93 -work work {../source/testFT601/peripherals/transmitController/transmitController.vhd}
vcom -93 -work work {../source/testFT601/peripherals/configRegister/configRegister.vhd}
vcom -93 -work work {../source/top.vhd}
