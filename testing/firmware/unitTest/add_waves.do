add wave -noupdate -group FT601 -label i_usbClk /top_level_vhd_tst/usbClk
add wave -noupdate -group FT601 -label o_usbWRn /top_level_vhd_tst/usbWRn
add wave -noupdate -group FT601 -label i_usbRXFn /top_level_vhd_tst/usbRXFn
add wave -noupdate -group FT601 -label io_usbD -radix hexadecimal /top_level_vhd_tst/usbD
add wave -noupdate -group FT601 -label io_usbBE -radix hexadecimal /top_level_vhd_tst/usbBE
add wave -noupdate -group FT601 -label i_usbTXEn /top_level_vhd_tst/usbTXEn

add wave -noupdate -group RESET -label i_reset /top_level_vhd_tst/reset

add wave -noupdate -group DBG -group USBWRITE0 -label o_usbwrn /top_level_vhd_tst/i1/communication1/comusbwrite(0)/comusbwrite/usbwrn
add wave -noupdate -group DBG -group USBWRITE0 -label i_usbrxn /top_level_vhd_tst/i1/communication1/comusbwrite(0)/comusbwrite/usbrxn
add wave -noupdate -group DBG -group USBWRITE0 -label o_usbdoe -radix hexadecimal /top_level_vhd_tst/i1/communication1/comusbwrite(0)/comusbwrite/usbdoe
add wave -noupdate -group DBG -group USBWRITE0 -label o_usbdout -radix hexadecimal /top_level_vhd_tst/i1/communication1/comusbwrite(0)/comusbwrite/usbdout
add wave -noupdate -group DBG -group USBWRITE0 -label i_usbdin -radix hexadecimal /top_level_vhd_tst/i1/communication1/comusbwrite(0)/comusbwrite/usbdin
add wave -noupdate -group DBG -group USBWRITE0 -label o_usbbeoe /top_level_vhd_tst/i1/communication1/comusbwrite(0)/comusbwrite/usbbeoe
add wave -noupdate -group DBG -group USBWRITE0 -label o_usbbeout -radix hexadecimal /top_level_vhd_tst/i1/communication1/comusbwrite(0)/comusbwrite/usbbeout
add wave -noupdate -group DBG -group USBWRITE0 -label i_usbtxen /top_level_vhd_tst/i1/communication1/comusbwrite(0)/comusbwrite/usbtxen
add wave -noupdate -group DBG -group USBWRITE0 -label o_fifordreq /top_level_vhd_tst/i1/communication1/comusbwrite(0)/comusbwrite/fifordreq
add wave -noupdate -group DBG -group USBWRITE0 -label i_fifordempty /top_level_vhd_tst/i1/communication1/comusbwrite(0)/comusbwrite/fifordempty
add wave -noupdate -group DBG -group USBWRITE0 -label i_fifoq /top_level_vhd_tst/i1/communication1/comusbwrite(0)/comusbwrite/fifoq
add wave -noupdate -group DBG -group USBWRITE0 -label o_busrequest /top_level_vhd_tst/i1/communication1/comusbwrite(0)/comusbwrite/busrequest
add wave -noupdate -group DBG -group USBWRITE0 -label o_businuse /top_level_vhd_tst/i1/communication1/comusbwrite(0)/comusbwrite/businuse
add wave -noupdate -group DBG -group USBWRITE0 -label i_busgranted /top_level_vhd_tst/i1/communication1/comusbwrite(0)/comusbwrite/busgranted
add wave -noupdate -group DBG -group USBWRITE0 -label s-state /top_level_vhd_tst/i1/communication1/comusbwrite(0)/comusbwrite/state
add wave -noupdate -group DBG -group USBWRITE0 -label s-lastfifoq /top_level_vhd_tst/i1/communication1/comusbwrite(0)/comusbwrite/lastfifoq
add wave -noupdate -group DBG -group USBWRITE0 -label s-pendingdata /top_level_vhd_tst/i1/communication1/comusbwrite(0)/comusbwrite/pendingdata

add wave -noupdate -group DBG -group USBREAD0 -label o_usbwrn /top_level_vhd_tst/i1/communication1/comusbread(0)/comusbread/usbwrn
add wave -noupdate -group DBG -group USBREAD0 -label i_usbrxn /top_level_vhd_tst/i1/communication1/comusbread(0)/comusbread/usbrxn
add wave -noupdate -group DBG -group USBREAD0 -label o_usbdoe -radix hexadecimal /top_level_vhd_tst/i1/communication1/comusbread(0)/comusbread/usbdoe
add wave -noupdate -group DBG -group USBREAD0 -label o_usbdout -radix hexadecimal /top_level_vhd_tst/i1/communication1/comusbread(0)/comusbread/usbdout
add wave -noupdate -group DBG -group USBREAD0 -label i_usbdin -radix hexadecimal /top_level_vhd_tst/i1/communication1/comusbread(0)/comusbread/usbdin
add wave -noupdate -group DBG -group USBREAD0 -label o_usbbeoe /top_level_vhd_tst/i1/communication1/comusbread(0)/comusbread/usbbeoe
add wave -noupdate -group DBG -group USBREAD0 -label o_usbbeout -radix hexadecimal /top_level_vhd_tst/i1/communication1/comusbread(0)/comusbread/usbbeout
add wave -noupdate -group DBG -group USBREAD0 -label i_usbbein -radix hexadecimal /top_level_vhd_tst/i1/communication1/comusbread(0)/comusbread/usbbein
add wave -noupdate -group DBG -group USBREAD0 -label i_usbtxen /top_level_vhd_tst/i1/communication1/comusbread(0)/comusbread/usbtxen
add wave -noupdate -group DBG -group USBREAD0 -label o_fifowrreq /top_level_vhd_tst/i1/communication1/comusbread(0)/comusbread/fifowrreq
add wave -noupdate -group DBG -group USBREAD0 -label i_fifowrfull /top_level_vhd_tst/i1/communication1/comusbread(0)/comusbread/fifowrfull
add wave -noupdate -group DBG -group USBREAD0 -label o_fifod /top_level_vhd_tst/i1/communication1/comusbread(0)/comusbread/fifod
add wave -noupdate -group DBG -group USBREAD0 -label o_busrequest /top_level_vhd_tst/i1/communication1/comusbread(0)/comusbread/busrequest
add wave -noupdate -group DBG -group USBREAD0 -label o_businuse /top_level_vhd_tst/i1/communication1/comusbread(0)/comusbread/businuse
add wave -noupdate -group DBG -group USBREAD0 -label i_busgranted /top_level_vhd_tst/i1/communication1/comusbread(0)/comusbread/busgranted
add wave -noupdate -group DBG -group USBREAD0 -label s-state /top_level_vhd_tst/i1/communication1/comusbread(0)/comusbread/state

add wave -noupdate -group DBG -group USBWRITE1 -label o_usbwrn /top_level_vhd_tst/i1/communication1/comusbwrite(1)/comusbwrite/usbwrn
add wave -noupdate -group DBG -group USBWRITE1 -label i_usbrxn /top_level_vhd_tst/i1/communication1/comusbwrite(1)/comusbwrite/usbrxn
add wave -noupdate -group DBG -group USBWRITE1 -label o_usbdoe -radix hexadecimal /top_level_vhd_tst/i1/communication1/comusbwrite(1)/comusbwrite/usbdoe
add wave -noupdate -group DBG -group USBWRITE1 -label o_usbdout -radix hexadecimal /top_level_vhd_tst/i1/communication1/comusbwrite(1)/comusbwrite/usbdout
add wave -noupdate -group DBG -group USBWRITE1 -label i_usbdin -radix hexadecimal /top_level_vhd_tst/i1/communication1/comusbwrite(1)/comusbwrite/usbdin
add wave -noupdate -group DBG -group USBWRITE1 -label o_usbbeoe /top_level_vhd_tst/i1/communication1/comusbwrite(1)/comusbwrite/usbbeoe
add wave -noupdate -group DBG -group USBWRITE1 -label o_usbbeout -radix hexadecimal /top_level_vhd_tst/i1/communication1/comusbwrite(1)/comusbwrite/usbbeout
add wave -noupdate -group DBG -group USBWRITE1 -label i_usbtxen /top_level_vhd_tst/i1/communication1/comusbwrite(1)/comusbwrite/usbtxen
add wave -noupdate -group DBG -group USBWRITE1 -label o_fifordreq /top_level_vhd_tst/i1/communication1/comusbwrite(1)/comusbwrite/fifordreq
add wave -noupdate -group DBG -group USBWRITE1 -label i_fifordempty /top_level_vhd_tst/i1/communication1/comusbwrite(1)/comusbwrite/fifordempty
add wave -noupdate -group DBG -group USBWRITE1 -label i_fifoq /top_level_vhd_tst/i1/communication1/comusbwrite(1)/comusbwrite/fifoq
add wave -noupdate -group DBG -group USBWRITE1 -label o_busrequest /top_level_vhd_tst/i1/communication1/comusbwrite(1)/comusbwrite/busrequest
add wave -noupdate -group DBG -group USBWRITE1 -label o_businuse /top_level_vhd_tst/i1/communication1/comusbwrite(1)/comusbwrite/businuse
add wave -noupdate -group DBG -group USBWRITE1 -label i_busgranted /top_level_vhd_tst/i1/communication1/comusbwrite(1)/comusbwrite/busgranted
add wave -noupdate -group DBG -group USBWRITE1 -label s-state /top_level_vhd_tst/i1/communication1/comusbwrite(1)/comusbwrite/state
add wave -noupdate -group DBG -group USBWRITE1 -label s-lastfifoq /top_level_vhd_tst/i1/communication1/comusbwrite(1)/comusbwrite/lastfifoq
add wave -noupdate -group DBG -group USBWRITE1 -label s-pendingdata /top_level_vhd_tst/i1/communication1/comusbwrite(1)/comusbwrite/pendingdata

add wave -noupdate -group DBG -group USBREAD1 -label o_usbwrn /top_level_vhd_tst/i1/communication1/comusbread(1)/comusbread/usbwrn
add wave -noupdate -group DBG -group USBREAD1 -label i_usbrxn /top_level_vhd_tst/i1/communication1/comusbread(1)/comusbread/usbrxn
add wave -noupdate -group DBG -group USBREAD1 -label o_usbdoe -radix hexadecimal /top_level_vhd_tst/i1/communication1/comusbread(1)/comusbread/usbdoe
add wave -noupdate -group DBG -group USBREAD1 -label o_usbdout -radix hexadecimal /top_level_vhd_tst/i1/communication1/comusbread(1)/comusbread/usbdout
add wave -noupdate -group DBG -group USBREAD1 -label i_usbdin -radix hexadecimal /top_level_vhd_tst/i1/communication1/comusbread(1)/comusbread/usbdin
add wave -noupdate -group DBG -group USBREAD1 -label o_usbbeoe /top_level_vhd_tst/i1/communication1/comusbread(1)/comusbread/usbbeoe
add wave -noupdate -group DBG -group USBREAD1 -label o_usbbeout -radix hexadecimal /top_level_vhd_tst/i1/communication1/comusbread(1)/comusbread/usbbeout
add wave -noupdate -group DBG -group USBREAD1 -label i_usbbein -radix hexadecimal /top_level_vhd_tst/i1/communication1/comusbread(1)/comusbread/usbbein
add wave -noupdate -group DBG -group USBREAD1 -label i_usbtxen /top_level_vhd_tst/i1/communication1/comusbread(1)/comusbread/usbtxen
add wave -noupdate -group DBG -group USBREAD1 -label o_fifowrreq /top_level_vhd_tst/i1/communication1/comusbread(1)/comusbread/fifowrreq
add wave -noupdate -group DBG -group USBREAD1 -label i_fifowrfull /top_level_vhd_tst/i1/communication1/comusbread(1)/comusbread/fifowrfull
add wave -noupdate -group DBG -group USBREAD1 -label o_fifod /top_level_vhd_tst/i1/communication1/comusbread(1)/comusbread/fifod
add wave -noupdate -group DBG -group USBREAD1 -label o_busrequest /top_level_vhd_tst/i1/communication1/comusbread(1)/comusbread/busrequest
add wave -noupdate -group DBG -group USBREAD1 -label o_businuse /top_level_vhd_tst/i1/communication1/comusbread(1)/comusbread/businuse
add wave -noupdate -group DBG -group USBREAD1 -label i_busgranted /top_level_vhd_tst/i1/communication1/comusbread(1)/comusbread/busgranted
add wave -noupdate -group DBG -group USBREAD1 -label s-state /top_level_vhd_tst/i1/communication1/comusbread(1)/comusbread/state

add wave -noupdate -group DBG -group PACKETCTRL0 -label o_desersyncmode /top_level_vhd_tst/i1/testft601_1/testpacketcontroller(0)/testpacketcontroller/desersyncmode
add wave -noupdate -group DBG -group PACKETCTRL0 -label o_deserrdreq /top_level_vhd_tst/i1/testft601_1/testpacketcontroller(0)/testpacketcontroller/deserrdreq
add wave -noupdate -group DBG -group PACKETCTRL0 -label i_deserrdempty /top_level_vhd_tst/i1/testft601_1/testpacketcontroller(0)/testpacketcontroller/deserrdempty
add wave -noupdate -group DBG -group PACKETCTRL0 -label i_deserq -radix hexadecimal /top_level_vhd_tst/i1/testft601_1/testpacketcontroller(0)/testpacketcontroller/deserq
add wave -noupdate -group DBG -group PACKETCTRL0 -label o_periphaddr -radix hexadecimal /top_level_vhd_tst/i1/testft601_1/testpacketcontroller(0)/testpacketcontroller/periphaddr
add wave -noupdate -group DBG -group PACKETCTRL0 -label o_periphwordsleft -radix hexadecimal /top_level_vhd_tst/i1/testft601_1/testpacketcontroller(0)/testpacketcontroller/periphwordsleft
add wave -noupdate -group DBG -group PACKETCTRL0 -label i_periphack /top_level_vhd_tst/i1/testft601_1/testpacketcontroller(0)/testpacketcontroller/periphack
add wave -noupdate -group DBG -group PACKETCTRL0 -label o_periphen /top_level_vhd_tst/i1/testft601_1/testpacketcontroller(0)/testpacketcontroller/periphen
add wave -noupdate -group DBG -group PACKETCTRL0 -label i_periphrdreq /top_level_vhd_tst/i1/testft601_1/testpacketcontroller(0)/testpacketcontroller/periphrdreq
add wave -noupdate -group DBG -group PACKETCTRL0 -label o_periphrdempty /top_level_vhd_tst/i1/testft601_1/testpacketcontroller(0)/testpacketcontroller/periphrdempty
add wave -noupdate -group DBG -group PACKETCTRL0 -label o_periphq -radix hexadecimal /top_level_vhd_tst/i1/testft601_1/testpacketcontroller(0)/testpacketcontroller/periphq
add wave -noupdate -group DBG -group PACKETCTRL0 -label s_state /top_level_vhd_tst/i1/testft601_1/testpacketcontroller(0)/testpacketcontroller/state

add wave -noupdate -group DBG -group DESER0 -label i_desersyncmode /top_level_vhd_tst/i1/testft601_1/testdeserializer(0)/testdeserializer/desersyncmode
add wave -noupdate -group DBG -group DESER0 -label i_deserrdreq /top_level_vhd_tst/i1/testft601_1/testdeserializer(0)/testdeserializer/deserrdreq
add wave -noupdate -group DBG -group DESER0 -label o_deserrdempty /top_level_vhd_tst/i1/testft601_1/testdeserializer(0)/testdeserializer/deserrdempty
add wave -noupdate -group DBG -group DESER0 -label o_deserq -radix hexadecimal /top_level_vhd_tst/i1/testft601_1/testdeserializer(0)/testdeserializer/deserq
add wave -noupdate -group DBG -group DESER0 -label o_fifordreq /top_level_vhd_tst/i1/testft601_1/testdeserializer(0)/testdeserializer/fifordreq
add wave -noupdate -group DBG -group DESER0 -label i_fifordempty /top_level_vhd_tst/i1/testft601_1/testdeserializer(0)/testdeserializer/fifordempty
add wave -noupdate -group DBG -group DESER0 -label i_fifoq -radix hexadecimal /top_level_vhd_tst/i1/testft601_1/testdeserializer(0)/testdeserializer/fifoq


add wave -noupdate -group DBG -group TXCTRL0 -label o_fifowrreq /top_level_vhd_tst/i1/testft601_1/testperipherals(0)/testperipherals/transmitcontroller1/fifowrreq
add wave -noupdate -group DBG -group TXCTRL0 -label i_fifowrfull /top_level_vhd_tst/i1/testft601_1/testperipherals(0)/testperipherals/transmitcontroller1/fifowrfull
add wave -noupdate -group DBG -group TXCTRL0 -label o_fifodata -radix hexadecimal /top_level_vhd_tst/i1/testft601_1/testperipherals(0)/testperipherals/transmitcontroller1/fifodata
add wave -noupdate -group DBG -group TXCTRL0 -label state /top_level_vhd_tst/i1/testft601_1/testperipherals(0)/testperipherals/transmitcontroller1/state
add wave -noupdate -group DBG -group TXCTRL0 -label enable /top_level_vhd_tst/i1/testft601_1/testperipherals(0)/testperipherals/transmitcontroller1/enable
add wave -noupdate -group DBG -group TXCTRL0 -label divcount -radix decimal /top_level_vhd_tst/i1/testft601_1/testperipherals(0)/testperipherals/transmitcontroller1/divcount
add wave -noupdate -group DBG -group TXCTRL0 -label pendingcount -radix decimal /top_level_vhd_tst/i1/testft601_1/testperipherals(0)/testperipherals/transmitcontroller1/pendingcount
add wave -noupdate -group DBG -group TXCTRL0 -label bytesleft -radix decimal /top_level_vhd_tst/i1/testft601_1/testperipherals(0)/testperipherals/transmitcontroller1/bytesleft
add wave -noupdate -group DBG -group TXCTRL0 -label rampvalue -radix hexadecimal /top_level_vhd_tst/i1/testft601_1/testperipherals(0)/testperipherals/transmitcontroller1/rampvalue
