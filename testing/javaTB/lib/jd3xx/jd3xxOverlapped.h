
#include <jni.h>
#include <windows.h>

#ifndef _Included_jd3xxOverlapped 
#define _Included_jd3xxOverlapped

typedef struct {
	jbyte *data;
	jlong size;
} pipeBuffer_t;

typedef struct _JD3XX_OVERLAPPED {
	OVERLAPPED overlapped;
	pipeBuffer_t pipeBuffer;
} JD3XX_OVERLAPPED, *LPJD3XX_OVERLAPPED;
 
LPJD3XX_OVERLAPPED jd3xxCreateOverlapped();

void prepareBuffers(LPJD3XX_OVERLAPPED jd3xxOverlapped, jint len) ;

void jd3xxReleaseOverlapped(LPJD3XX_OVERLAPPED jd3xxOverlapped);

#endif