
#include "jd3xxOverlapped.h"

//#define DEBUG
#ifdef DEBUG
	#define javaPrintf(...) {printf(__VA_ARGS__);\
							fflush(stdout);} 
#else
	#define javaPrintf(...) {}
#endif

void initPipeBuffer(pipeBuffer_t* pipeBuffer);
void clearPipeBuffer(pipeBuffer_t* pipeBuffer);

LPJD3XX_OVERLAPPED jd3xxCreateOverlapped() {
	LPJD3XX_OVERLAPPED ans = NULL;
	javaPrintf("llamando Malloc\n");
	ans =  (LPJD3XX_OVERLAPPED)malloc(sizeof(JD3XX_OVERLAPPED));
	if (ans != NULL) {
		javaPrintf("llamadno clearPipe\n");
		initPipeBuffer(&(ans->pipeBuffer));
	}
	javaPrintf("pipeBuffer.data %p\n", ans->pipeBuffer.data);
	return ans;
}

void prepareBuffers(LPJD3XX_OVERLAPPED jd3xxOverlapped, jint len) {
	if (jd3xxOverlapped != NULL) {
		if ((jd3xxOverlapped->pipeBuffer.data == NULL) || 
				(jd3xxOverlapped->pipeBuffer.size != len)) {
			javaPrintf("pipeBuffer.data %p\n", jd3xxOverlapped->pipeBuffer.data);
			if (jd3xxOverlapped->pipeBuffer.data != NULL) {
				javaPrintf("limpiando pipeBuffer\n");
				free(jd3xxOverlapped->pipeBuffer.data);
			}
			javaPrintf("reservando espacio bytes, len %d\n", (int)len);
			javaPrintf("reservando espacio bytes, len %lu\n", (long unsigned int)(len*sizeof(jbyte)));
			jd3xxOverlapped->pipeBuffer.data = (jbyte *)malloc(len*sizeof(jbyte));
			if (jd3xxOverlapped->pipeBuffer.data != NULL) {
				jd3xxOverlapped->pipeBuffer.size = len;
			} else {
				jd3xxOverlapped->pipeBuffer.size = 0;
			}
		} else {
			javaPrintf("overlapped same size\n");
		}
	} else {
		javaPrintf("overlapped null pointer\n");
	}
}

void jd3xxReleaseOverlapped(LPJD3XX_OVERLAPPED jd3xxOverlapped) {
	if (jd3xxOverlapped != NULL) {
		clearPipeBuffer(&(jd3xxOverlapped->pipeBuffer));
	}
}

void clearPipeBuffer(pipeBuffer_t* pipeBuffer) {
	if (pipeBuffer->data != NULL) {
		free(pipeBuffer->data);
	} 
	initPipeBuffer(pipeBuffer);
}

void initPipeBuffer(pipeBuffer_t* pipeBuffer) {
	pipeBuffer->data = NULL;
	pipeBuffer->size = 0;
}