/*
	jd3xx by Nicolas Dassieu Blanchet
	Based on jd2xx by Pablo Bleyer Kocik.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright notice, this
	list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright notice,
	this list of conditions and the following disclaimer in the documentation
	and/or other materials provided with the distribution.

	3. The name of the author may not be used to endorse or promote products
	derived from this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
	EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
	PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
	BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
	ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
	POSSIBILITY OF SUCH DAMAGE.
*/

//#define DEBUG

#include <jni.h>
#include "jd3xx.h"
#include "ftd3xx.h"

#include "jd3xxOverlapped.h"

#ifdef WIN32
	#include <windows.h>
#endif

#undef WINAPI
#define WINAPI

#ifndef INVALID_HANDLE_VALUE
#define INVALID_HANDLE_VALUE (-1)
#endif

#ifdef DEBUG
	#define javaPrintf(...) {printf(__VA_ARGS__);\
							fflush(stdout);} 
#else
	#define javaPrintf(...) {}
#endif

///* Defines */
#define DESCRIPTION_SIZE 256 // size for serial numbers and descriptions
//#define MAX_DEVICES 64 // maximum number of devices to list
//
///* GLoabl variables */
static JavaVM *javavm;
static jclass JD3XXCls/*, JD2XXEventListenerCls*/; // JD2XX class object reference
static jfieldID handleID/*,eventID, killID, listenerID*/; // id field object reference
static jclass StringCls; // java.lang.String class object reference
//// static jclass pdCls; // ProgramData
//// static jclass diCls; // DeviceInfo
//

#include "jd3xxOverlapped.h"


#define TOTAL_CHANNELS 8

static pipeBuffer_t pipeBuffers[TOTAL_CHANNELS];
#define pipeIDtoBufferNum(pipeID) (((pipeID&0x80)>>5)+((pipeID-2)&0x3))

void allocPipeBuffers(jboolean allWrPipes, jboolean allRdPipes, jbyte pipeID, jlong streamSize);
void allocPipeBuffer(int bufferNum, jlong streamSize); 
void releasePipeBuffers(jboolean allWrPipes, jboolean allRdPipes, jbyte pipeID);
void releasePipeBuffer(int bufferNum);
void releaseAllpipeBuffers(void);

/** Error message descriptions */
static char*
status_message[] = {
	"ok",
	"invalid handle",
	"device not found",
	"device not opened",
	"io error",
	"insufficient resources",
	"invalid parameter",
	"invalid baud rate",
	"device not opened for erase",
	"device not opened for write",
	"failed to write device",
	"eeprom read failed",
	"eeprom write failed",
	"eeprom erase failed",
	"eeprom not present",
	"eeprom not programmed",
	"invalid args",
	"not supported",
	
	"no more items",
	"timeout",
	"operation aborted",
	"reserved pipe",
	"invalid control request direction",
	"invalid control request type",
	"io pending",
	"io incomplete",
	"handle eof",
	"busy",
	"no system resources",
	"device list not ready",
	"device not connected",
	"incorrect device path",
	
	"other error"
};

/** Get object handle */
inline static jint
get_handle(JNIEnv *env, jobject obj) {
	return (*env)->GetIntField(env, obj, handleID);
}

/** Set object handle */
inline static void
set_handle(JNIEnv *env, jobject obj, jint val) {
	(*env)->SetIntField(env, obj, handleID, val);
}

///** Get event handle */
//inline static jint
//get_event(JNIEnv *env, jobject obj) {
//	return (*env)->GetIntField(env, obj, eventID);
//}
//
///** Set event handle */
//inline static void
//set_event(JNIEnv *env, jobject obj, jint val) {
//	(*env)->SetIntField(env, obj, eventID, val);
//}
//
///** Get kill value */
//inline static jint
//get_kill(JNIEnv *env, jobject obj) {
//	return (*env)->GetBooleanField(env, obj, killID);
//}
//
///** Get listener */
//inline static jobject
//get_listener(JNIEnv *env, jobject obj) {
//	return (*env)->GetObjectField(env, obj, listenerID);
//}

/** Throw exception */
inline static void
io_exception(JNIEnv *env, const char *msg) {
	// jclass exc = (*env)->FindClass(env, "java/lang/RuntimeException");
	jclass exc = (*env)->FindClass(env, "java/io/IOException");
	if (exc == 0) return;
	(*env)->ThrowNew(env, exc, msg);
	(*env)->DeleteLocalRef(env, exc);
}

/** Format exception error message */
inline static char*
format_status(char *buf, FT_STATUS st) {
	if (buf != NULL)
		sprintf(buf, "%s (%d)",
			status_message[(st <= FT_OTHER_ERROR) ? st : FT_OTHER_ERROR],
			(int)st
		);
	return buf;
}

/** Format error message and throw exception */
inline static void
io_exception_status(JNIEnv *env, FT_STATUS st) {
	char msg[64];
	io_exception(env, format_status(msg, st));
}

/** Initialize JD2XX driver objects */
JNIEXPORT jint JNICALL
JNI_OnLoad(JavaVM *jvm, void *reserved) {
	JNIEnv *env;
	jclass cls;

	if ((*jvm)->GetEnv(jvm, (void **)&env, JNI_VERSION_1_2)) {
		return JNI_ERR; // not supported
	}

	// cls = (*env)->GetObjectClass(env, obj);
	cls = (*env)->FindClass(env, "Ljd3xx/JD3XX;");
	if (cls == 0) return JNI_ERR;
	JD3XXCls = (*env)->NewWeakGlobalRef(env, cls); // weak allows library unloading
	(*env)->DeleteLocalRef(env, cls);
	if (JD3XXCls == 0) return JNI_ERR;

	handleID = (*env)->GetFieldID(env, JD3XXCls, "handle", "I");
	if (handleID == 0) return JNI_ERR;

	//eventID = (*env)->GetFieldID(env, JD2XXCls, "event", "I");
	//if (eventID == 0) return JNI_ERR;

	//killID = (*env)->GetFieldID(env, JD2XXCls, "kill", "Z");
	//if (killID == 0) return JNI_ERR;

	//listenerID = (*env)->GetFieldID(env, JD2XXCls, "listener", "Ljd2xx/JD2XXEventListener;");
	//if (listenerID == 0) return JNI_ERR;

	//cls = (*env)->FindClass(env, "Ljd3xx/JD3XXEventListener;");
	//if (cls == 0) return JNI_ERR;
	//JD2XXEventListenerCls = (*env)->NewWeakGlobalRef(env, cls);
	//(*env)->DeleteLocalRef(env, cls);
	//if (JD2XXEventListenerCls == 0) return JNI_ERR;

	cls = (*env)->FindClass(env, "java/lang/String");
	if (cls == 0) return JNI_ERR;
	StringCls = (*env)->NewWeakGlobalRef(env, cls);
	(*env)->DeleteLocalRef(env, cls);
	if (StringCls == 0) return JNI_ERR;

//	cls = (*env)->FindClass(env, "Ljd2xx/JD2XX$ProgramData;");
//	if (cls == 0) return JNI_ERR;
//	pdCls = (*env)->NewWeakGlobalRef(env, cls);
//	if (pdCls == 0) return JNI_ERR;

	javavm = jvm; // initialize jvm pointer
	
	for (int i=0; i<TOTAL_CHANNELS; i++) {
		pipeBuffers[i].data = NULL;
		pipeBuffers[i].size = 0;
	}

	return JNI_VERSION_1_2;
}

///** Finalize JD2xx driver */
JNIEXPORT void JNICALL
JNI_OnUnLoad(JavaVM *jvm, void *reserved) {
	JNIEnv *env;

	releaseAllpipeBuffers();

	if ((*jvm)->GetEnv(jvm, (void **)&env, JNI_VERSION_1_2)) {
		return; // not supported
	}

	//(*env)->DeleteWeakGlobalRef(env, JD2XXCls);
	//(*env)->DeleteWeakGlobalRef(env, JD2XXEventListenerCls);
	//(*env)->DeleteWeakGlobalRef(env, StringCls);

//	fprintf(stderr,  "Bye!\n");
//	fflush(stderr);
}


//JNIEXPORT jint JNICALL
//Java_jd3xx_JD3XX_getLibraryVersion(JNIEnv *env, jobject obj) {
//	FT_STATUS st;
//	volatile DWORD ver;
//
//	if (!FT_SUCCESS(st = FT_GetLibraryVersion(&ver)))
//		io_exception_status(env, st);
//
//	return (jint)ver;
//}

JNIEXPORT jint JNICALL
Java_jd3xx_JD3XX_createDeviceInfoList(JNIEnv *env, jobject obj) {
	FT_STATUS st;
	DWORD n;

	if (!FT_SUCCESS(st = FT_CreateDeviceInfoList(&n)))
		io_exception_status(env, st);

	return (jint)n;
}

JNIEXPORT jobject JNICALL
Java_jd3xx_JD3XX_getDeviceInfoDetail(JNIEnv *env, jobject obj, jint dn) {
	FT_STATUS st;
	jobject result;

	jclass dicls;
	volatile jfieldID fid; //!!! volatile: MinGW bug hack! Access violation at fid
	jint deviceFlags, deviceType, deviceID, deviceLocation, deviceHandle;
	jstring str;

	char serialNumber[DESCRIPTION_SIZE];
	char description[DESCRIPTION_SIZE];

	if (!FT_SUCCESS(st = FT_GetDeviceInfoDetail(
		(DWORD)dn,
		(DWORD*)&deviceFlags, (DWORD*)&deviceType,
		(DWORD*)&deviceID, (DWORD*)&deviceLocation,
		serialNumber, description, (FT_HANDLE)&deviceHandle)
	)) {
		io_exception_status(env, st);
		return NULL;
	}

	// fprintf(stderr, "%x %x %s %s\n", deviceType, deviceID, serialNumber, description);

	dicls = (*env)->FindClass(env, "Ljd3xx/JD3XX$DeviceInfo;");
	if (dicls == 0) return NULL;

	result = (*env)->AllocObject(env, dicls);
	if (result == 0) goto panic;

	if ((fid = (*env)->GetFieldID(env, dicls, "flags", "I")) == 0) goto panic;
	(*env)->SetIntField(env, result, fid, deviceFlags);

	if ((fid = (*env)->GetFieldID(env, dicls, "type", "I")) == 0) goto panic;
	(*env)->SetIntField(env, result, fid, deviceType);

	if ((fid = (*env)->GetFieldID(env, dicls, "id", "I")) == 0) goto panic;
	(*env)->SetIntField(env, result, fid, deviceID);

	if ((fid = (*env)->GetFieldID(env, dicls, "location", "I")) == 0) goto panic;
	(*env)->SetIntField(env, result, fid, deviceLocation);

	if ((fid = (*env)->GetFieldID(env, dicls, "serial", "Ljava/lang/String;")) == 0) goto panic;
	if ((str = (*env)->NewStringUTF(env, serialNumber)) == 0) goto panic;
	// str = (*env)->GetObjectField(env, result, fid);
	// fprintf(stderr, "%x\n", fid);
	(*env)->SetObjectField(env, result, fid, str);

	if ((fid = (*env)->GetFieldID(env, dicls, "description", "Ljava/lang/String;")) == 0) goto panic;
	if ((str = (*env)->NewStringUTF(env, description)) == 0) goto panic;
	(*env)->SetObjectField(env, result, fid, str);

	if ((fid = (*env)->GetFieldID(env, dicls, "handle", "I")) == 0) goto panic;
	(*env)->SetIntField(env, result, fid, deviceHandle);

	(*env)->DeleteLocalRef(env, dicls);
	return result;

panic:
	(*env)->DeleteLocalRef(env, result);
	(*env)->DeleteLocalRef(env, dicls);
	return NULL;
}

JNIEXPORT void JNICALL
Java_jd3xx_JD3XX_create__Ljava_lang_String_2I(JNIEnv *env, jobject obj, jstring str, jint flg) {
	jint hnd = get_handle(env, obj);

	if (hnd != -1)//(jint)INVALID_HANDLE_VALUE) // previously initialized!
		io_exception(env, "device already opened 2");
	else {
		const char *cstr = (*env)->GetStringUTFChars(env, str, 0);
		FT_HANDLE h;
		FT_STATUS st = FT_Create((PVOID)cstr, (DWORD)flg, &h);
		(*env)->ReleaseStringUTFChars(env, str, cstr);

		if (FT_SUCCESS(st)) set_handle(env, obj, (jint)(intptr_t)h);
		else io_exception_status(env, st);
	}
}

JNIEXPORT void JNICALL
Java_jd3xx_JD3XX_create__II(JNIEnv *env, jobject obj, jint num, jint flg) {
	jint hnd = get_handle(env, obj);

	if (hnd != (jint)(intptr_t)INVALID_HANDLE_VALUE) // previously initialized!
		io_exception(env, "device already opened");
	else {
		FT_HANDLE h;
		FT_STATUS st = FT_Create((PVOID)(intptr_t)num, (DWORD)flg, &h);

		if (FT_SUCCESS(st)) set_handle(env, obj, (jint)(intptr_t)h);
		else io_exception_status(env, st);
	}
}

JNIEXPORT void JNICALL
Java_jd3xx_JD3XX_close(JNIEnv *env, jobject obj) {
	jint hnd = get_handle(env, obj);

	if (hnd != (jint)(intptr_t)INVALID_HANDLE_VALUE) {
		FT_STATUS st = FT_Close((FT_HANDLE)((intptr_t)hnd));
		if (!FT_SUCCESS(st)) io_exception_status(env, st);
		else set_handle(env, obj, (jint)(intptr_t)INVALID_HANDLE_VALUE);
	}
}

/**
//	@todo Check LIST_BY_LOCATION (array of Integers) implementation
//*/
//JNIEXPORT jobjectArray JNICALL
//Java_jd3xx_JD3XX_listDevices(JNIEnv *env, jobject obj, jint flg) {
//	jobjectArray result;
//	int r = 0;
//	FT_STATUS st;
//	volatile DWORD n; //!!! volatile: MinGW GCC bug hack, disable optimization
//	int i;
//
//	// first search for number of devices
//	st = FT_ListDevices((PVOID)&n, NULL, FT_LIST_NUMBER_ONLY);
//	if (!FT_SUCCESS(st)) {
//		io_exception_status(env, st);
//		return NULL;
//	}
//
//	n = (n <= MAX_DEVICES) ? n : MAX_DEVICES;
//
//	if (flg & jd3xx_JD3XX_OPEN_BY_LOCATION) {
//		DWORD ba[n];
//		jclass icls;
//		jmethodID mid;
//
//		icls = (*env)->FindClass(env, "java/lang/Integer");
//		if (icls == 0) return NULL;
//		mid = (*env)->GetMethodID(env, icls, "<init>", "(I)V");
//		if (mid == 0) return NULL;
//
//		result = (*env)->NewObjectArray(env, n, icls, 0);
//		if (result == NULL || n == 0) return result;
//
//		st = FT_ListDevices((PVOID)ba, (PVOID)&n, FT_LIST_ALL | (DWORD)flg);
//		if (!FT_SUCCESS(st)) {
//			io_exception_status(env, st);
//			return NULL;
//		}
//
//		for (i=0; i<n; ++i) {
//			jobject iobj = (*env)->NewObject(env, icls, mid, (jint)ba[i]);
//			if (iobj == 0) return NULL;
//			(*env)->SetObjectArrayElement(env, result, i, iobj);
//			// fprintf(stderr, "%d: %s\n", i, ba[i]);
//		}
//
//		(*env)->DeleteLocalRef(env, icls);
//	}
//	else {
//		char *ba[MAX_DEVICES+1];
//		char bd[MAX_DEVICES*DESCRIPTION_SIZE];
//
//		result = (*env)->NewObjectArray(env, n, StringCls, 0);
//		if (result == NULL || n == 0) return result;
//
//		for (i=0; i<n; ++i) ba[i] = bd + i*DESCRIPTION_SIZE;
//		ba[n] = NULL;
//
//		st = FT_ListDevices((PVOID)ba, (PVOID)&n, FT_LIST_ALL | (DWORD)flg);
//		if (!FT_SUCCESS(st)) {
//			io_exception_status(env, st);
//			return NULL;
//		}
//
//		// fprintf(stderr, "%s\n", bd);
//		// fprintf(stderr, "%d\n", n);
//
//		for (i=0; i<n; ++i) {
//			jstring str = (*env)->NewStringUTF(env, ba[i]);
//			if (str == 0) return NULL;
//			(*env)->SetObjectArrayElement(env, result, i, str);
//			// fprintf(stderr, "%d: %s\n", i, ba[i]);
//		}
//	}
//
//	return result;
//}

JNIEXPORT jint JNICALL
Java_jd3xx_JD3XX_readPipe(JNIEnv *env, jobject obj, jbyte pipeID, jbyteArray arr, jint off, jint len) {
	FT_STATUS st;
	volatile DWORD ret;
	jint hnd = get_handle(env, obj);
	int alen = (*env)->GetArrayLength(env, arr);
	jbyte *buf;
	if (arr == 0) {
		jclass exc = (*env)->FindClass(env, "java/lang/NullPointerException");
		if (exc != 0) (*env)->ThrowNew(env, exc, NULL);
		(*env)->DeleteLocalRef(env, exc);
		return 0;
	} else if ((off < 0) || (off > alen) || (len < 0)
		|| ((off + len) > alen) || ((off + len) < 0)) {
		jclass exc = (*env)->FindClass(env, "java/lang/IndexOutOfBoundsException");
		if (exc != 0) (*env)->ThrowNew(env, exc, NULL);
		(*env)->DeleteLocalRef(env, exc);
		return 0;
	} else if (len == 0) return 0;

	if (pipeBuffers[pipeIDtoBufferNum(pipeID)].size >= len) {
		// rx in local buffer
		if (!FT_SUCCESS(st = FT_ReadPipe((FT_HANDLE)(intptr_t)hnd, pipeID, (LPVOID)(pipeBuffers[pipeIDtoBufferNum(pipeID)].data), len, (LPDWORD)&ret, NULL)))
			io_exception_status(env, st);
		// Copy data to Java
		buf = (*env)->GetPrimitiveArrayCritical(env, arr, 0);
		memcpy(buf+off, pipeBuffers[pipeIDtoBufferNum(pipeID)].data, len);
		(*env)->ReleasePrimitiveArrayCritical(env, arr, buf, 0);
	} else {
		// When not in streaming mode use conventional lock mode
		buf = (*env)->GetByteArrayElements(env, arr, 0);
		if (!FT_SUCCESS(st = FT_ReadPipe((FT_HANDLE)(intptr_t)hnd, pipeID, (LPVOID)(buf+off), len, (LPDWORD)&ret, NULL)))
			io_exception_status(env, st);
		(*env)->ReleaseByteArrayElements(env, arr, buf, 0);
	}
	return (jint)ret;
}

/*
 * Class:     jd3xx_JD3XX
 * Method:    read
 * Signature: (B[BII)I
 */
JNIEXPORT void JNICALL
Java_jd3xx_JD3XX_startReadPipe(JNIEnv *env, jobject obj, jbyte pipeID, jint len, jlong overlapped) {
	FT_STATUS st;
	volatile DWORD ret;
	jint hnd = get_handle(env, obj);
	LPJD3XX_OVERLAPPED jd3xxOverlapped = (LPJD3XX_OVERLAPPED)overlapped;
	javaPrintf("startReadPipe jd3xxOverlapped: %p\n", jd3xxOverlapped);
	javaPrintf("startReadPipe : %p\n", &(jd3xxOverlapped->overlapped));
	
	if (jd3xxOverlapped==NULL) {
		jclass exc = (*env)->FindClass(env, "java/lang/NullPointerException");
		if (exc != 0) (*env)->ThrowNew(env, exc, NULL);
		(*env)->DeleteLocalRef(env, exc);
		return;
	}
	
	prepareBuffers(jd3xxOverlapped, len);
	javaPrintf("Saliendo de prepare buffers");
	st = FT_ReadPipe((FT_HANDLE)(intptr_t)hnd, pipeID, 
		(LPVOID)(jd3xxOverlapped->pipeBuffer.data), len, (LPDWORD)&ret, 
		(LPOVERLAPPED)&(jd3xxOverlapped->overlapped));
	if ((st!=FT_IO_PENDING) && (!FT_SUCCESS(st))) {
		io_exception_status(env, st);
	}
	
	javaPrintf("startReadPipeEnd jd3xxOverlapped: %p\n", jd3xxOverlapped);
	javaPrintf("startReadPipeEnd : %p\n", &(jd3xxOverlapped->overlapped));
}

JNIEXPORT jint JNICALL
Java_jd3xx_JD3XX_getOverlappedResults(JNIEnv *env, jobject obj, jbyte pipeID, jbyteArray arr, 
		jint off, jlong overlapped) {
	FT_STATUS st;
	volatile DWORD ret;
	jint hnd = get_handle(env, obj);
	int alen = (*env)->GetArrayLength(env, arr);
	jbyte *buf;
	LPJD3XX_OVERLAPPED jd3xxOverlapped = (LPJD3XX_OVERLAPPED)overlapped;
	javaPrintf("getOverlappedResult jd3xxOverlapped: %p\n", jd3xxOverlapped);
	javaPrintf("getOverlappedResult: %p\n", &(jd3xxOverlapped->overlapped));
	
	
	if (jd3xxOverlapped==NULL) {
		jclass exc = (*env)->FindClass(env, "java/lang/NullPointerException");
		if (exc != 0) (*env)->ThrowNew(env, exc, NULL);
		(*env)->DeleteLocalRef(env, exc);
		return 0;
	}
	jint len = jd3xxOverlapped->pipeBuffer.size;
	if (arr == 0) {
		jclass exc = (*env)->FindClass(env, "java/lang/NullPointerException");
		if (exc != 0) (*env)->ThrowNew(env, exc, NULL);
		(*env)->DeleteLocalRef(env, exc);
		return 0;
	} else if ((off < 0) || (off > alen) || (len < 0)
		|| ((off + len) > alen) || ((off + len) < 0)) {
		jclass exc = (*env)->FindClass(env, "java/lang/IndexOutOfBoundsException");
		if (exc != 0) (*env)->ThrowNew(env, exc, NULL);
		(*env)->DeleteLocalRef(env, exc);
		return 0;
	} else if (len == 0) return 0;

	if (!FT_SUCCESS(st = FT_GetOverlappedResult((FT_HANDLE)(intptr_t)hnd, &(jd3xxOverlapped->overlapped), (LPDWORD)&ret, TRUE))) {
		io_exception_status(env, st);
	}
	
	// Copy data to Java
	buf = (*env)->GetPrimitiveArrayCritical(env, arr, 0);
	memcpy(buf+off, jd3xxOverlapped->pipeBuffer.data, len);
	(*env)->ReleasePrimitiveArrayCritical(env, arr, buf, 0);

	return (jint)ret;
}


JNIEXPORT jlong JNICALL
Java_jd3xx_JD3XX_initializeOverlapped(JNIEnv *env, jobject obj) {
	FT_STATUS st;
	jint hnd = get_handle(env, obj);
	LPJD3XX_OVERLAPPED jd3xxOverlapped;
	javaPrintf("Creando jd3xxOverlapped\n");
	jd3xxOverlapped = jd3xxCreateOverlapped();
	javaPrintf("saliendo\n");
	if (jd3xxOverlapped == NULL)
		io_exception_status(env, FT_OTHER_ERROR);
	javaPrintf("Creando Overlapped\n");
	if (!FT_SUCCESS(st = FT_InitializeOverlapped((FT_HANDLE)(intptr_t)hnd, &(jd3xxOverlapped->overlapped))))
		io_exception_status(env, st);
	
	javaPrintf("creaci�n de overlapped: %p\n", &(jd3xxOverlapped->overlapped));
	
	return (jlong)jd3xxOverlapped;
}

JNIEXPORT void JNICALL
Java_jd3xx_JD3XX_releaseOverlapped(JNIEnv *env, jobject obj, jlong overlapped) {
	FT_STATUS st;
	jint hnd = get_handle(env, obj);
	LPJD3XX_OVERLAPPED jd3xxOverlapped = (LPJD3XX_OVERLAPPED)overlapped;
	
	javaPrintf("destrucci�n de overlapped: %p\n", &(jd3xxOverlapped->overlapped));
	
	if (!FT_SUCCESS(st = FT_ReleaseOverlapped((FT_HANDLE)(intptr_t)hnd, &(jd3xxOverlapped->overlapped))))
		io_exception_status(env, st);
	
	jd3xxReleaseOverlapped(jd3xxOverlapped);
}


JNIEXPORT jint JNICALL
Java_jd3xx_JD3XX_writePipe(JNIEnv *env, jobject obj, jbyte pipeID, jbyteArray arr, jint off, jint len) {
	FT_STATUS st;
	volatile DWORD ret;
	jint hnd = get_handle(env, obj);
	int alen = (*env)->GetArrayLength(env, arr);
	jbyte *buf;

	if (arr == 0) {
		jclass exc = (*env)->FindClass(env, "java/lang/NullPointerException");
		if (exc != 0) (*env)->ThrowNew(env, exc, NULL);
		(*env)->DeleteLocalRef(env, exc);
		return 0;
	} else if ((off < 0) || (off > alen) || (len < 0)
		|| ((off + len) > alen) || ((off + len) < 0)) {
		jclass exc = (*env)->FindClass(env, "java/lang/IndexOutOfBoundsException");
		if (exc != 0) (*env)->ThrowNew(env, exc, NULL);
		(*env)->DeleteLocalRef(env, exc);
		return 0;
	} else if (len == 0) return 0;

 	buf = (*env)->GetByteArrayElements(env, arr, 0);

	if (!FT_SUCCESS(st = FT_WritePipe((FT_HANDLE)(intptr_t)hnd, pipeID, (LPVOID)(buf+off), len, (LPDWORD)&ret, NULL)))
		io_exception_status(env, st);

	(*env)->ReleaseByteArrayElements(env, arr, buf, 0);
	return (jint)ret;
}

JNIEXPORT void JNICALL Java_jd3xx_JD3XX_setStreamPipe(JNIEnv *env, jobject obj, 
		jboolean allWrPipes, jboolean allRdPipes, jbyte pipeID, jlong streamSize) {
	FT_STATUS st;
	jint hnd = get_handle(env, obj);
	
	allocPipeBuffers(allWrPipes, allRdPipes, pipeID, streamSize);
	
	if (!FT_SUCCESS(st = FT_SetStreamPipe((FT_HANDLE)(intptr_t)hnd, allWrPipes, 
			allRdPipes, pipeID, streamSize)))
		io_exception_status(env, st);
}

JNIEXPORT void JNICALL Java_jd3xx_JD3XX_clearStreamPipe(JNIEnv *env, jobject obj, 
		jboolean allWrPipes, jboolean allRdPipes, jbyte pipeID) {
	FT_STATUS st;
	jint hnd = get_handle(env, obj);
	
	releasePipeBuffers(allWrPipes, allRdPipes, pipeID);
	
	if (!FT_SUCCESS(st = FT_ClearStreamPipe((FT_HANDLE)(intptr_t)hnd, allWrPipes, 
			allRdPipes, pipeID)))
		io_exception_status(env, st);
}

void allocPipeBuffers(jboolean allWrPipes, jboolean allRdPipes, jbyte pipeID, jlong streamSize) {
	if (allWrPipes) {
		for (int i=0; i<TOTAL_CHANNELS/2; i++) {
			allocPipeBuffer(i, streamSize);
		}
	} else if (allRdPipes) {
		for (int i=TOTAL_CHANNELS/2; i<TOTAL_CHANNELS; i++) {
			allocPipeBuffer(i, streamSize);
		}
	} else {
		allocPipeBuffer(pipeIDtoBufferNum(pipeID), streamSize);
	}
}

void allocPipeBuffer(int bufferNum, jlong streamSize) {
	releasePipeBuffer(bufferNum);
	pipeBuffers[bufferNum].data=(jbyte *)malloc(streamSize*sizeof(jbyte));
	if (pipeBuffers[bufferNum].data!=NULL) {
		pipeBuffers[bufferNum].size = streamSize;
	} else {
		pipeBuffers[bufferNum].size = 0;
	}
}


void releasePipeBuffers(jboolean allWrPipes, jboolean allRdPipes, jbyte pipeID) {
	if (allWrPipes) {
		for (int i=0; i<TOTAL_CHANNELS/2; i++) {
			releasePipeBuffer(i);
		}
	} else if (allRdPipes) {
		for (int i=TOTAL_CHANNELS/2; i<TOTAL_CHANNELS; i++) {
			releasePipeBuffer(i);
		}
	} else {
		releasePipeBuffer(pipeIDtoBufferNum(pipeID));
	}
}

void releasePipeBuffer(int bufferNum) {
	if (pipeBuffers[bufferNum].data!=NULL) {
		free(pipeBuffers[bufferNum].data);
	}
	pipeBuffers[bufferNum].data = NULL;
	pipeBuffers[bufferNum].size = 0;
}

void releaseAllpipeBuffers() {
	for (int i=0; i<TOTAL_CHANNELS; i++) {
		releasePipeBuffer(i);
	}
}

JNIEXPORT void JNICALL 
Java_jd3xx_JD3XX_setPipeTimeout(JNIEnv *env, jobject obj, jbyte pipeID, jlong msTimeout) {
	FT_STATUS st;
	jint hnd = get_handle(env, obj);
	
	if (!FT_SUCCESS(st = FT_SetPipeTimeout((FT_HANDLE)(intptr_t)hnd, pipeID, msTimeout)))
		io_exception_status(env, st);
}

JNIEXPORT jlong JNICALL 
Java_jd3xx_JD3XX_getPipeTimeout(JNIEnv *env, jobject obj, jbyte pipeID) {
	FT_STATUS st;
	ULONG msTimeout;
	jint hnd = get_handle(env, obj);
	
	if (!FT_SUCCESS(st = FT_GetPipeTimeout((FT_HANDLE)(intptr_t)hnd, pipeID, &msTimeout)))
		io_exception_status(env, st);
		
	return (jlong)msTimeout;
}

JNIEXPORT void JNICALL 
Java_jd3xx_JD3XX_abortPipe(JNIEnv *env, jobject obj, jbyte pipeID) {
	FT_STATUS st;
	jint hnd = get_handle(env, obj);
	
	if (!FT_SUCCESS(st = FT_AbortPipe((FT_HANDLE)(intptr_t)hnd, pipeID)))
		io_exception_status(env, st);
}

//JNIEXPORT void JNICALL
//Java_jd3xx_JD3XX_resetPort(JNIEnv *env, jobject obj) {
//	FT_STATUS st;
//	jint hnd = get_handle(env, obj);
//
//	if (!FT_SUCCESS(st = FT_ResetPort((FT_HANDLE)hnd)))
//		io_exception_status(env, st);
//}
//
//JNIEXPORT void JNICALL
//Java_jd3xx_JD3XX_cyclePort(JNIEnv *env, jobject obj) {
//	FT_STATUS st;
//	jint hnd = get_handle(env, obj);
//
//#ifdef WIN32
//	if (!FT_SUCCESS(st = FT_CyclePort((FT_HANDLE)hnd)))
//		io_exception_status(env, st);
//#else
//	// Not available in Linux or OS X
//	// See FTDO docs for more details.
//	io_exception_status(env, FT_NOT_SUPPORTED);
//#endif
//}
//
//JNIEXPORT jint JNICALL
//Java_jd3xx_JD3XX_getDriverVersion(JNIEnv *env, jobject obj) {
//	FT_STATUS st;
//	jint hnd = get_handle(env, obj);
//	volatile DWORD ver;
//
//	if (!FT_SUCCESS(st = FT_GetDriverVersion((FT_HANDLE)hnd, &ver)))
//		io_exception_status(env, st);
//
//	return (jint)ver;
//}
