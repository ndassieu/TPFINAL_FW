/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jd3xx;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ndassieu
 */
public abstract class Stats {
	protected boolean running = false;
	
	public synchronized void resetStats() {
		
	}
	
	public synchronized void playStats() {
		running = true;
	}
	
	public synchronized void pauseStats() {
		running = false;
	}
	
	public abstract String ToStringPartialStats();
	
	public abstract String toString();
	
	protected abstract void writeStats(PrintWriter pw);
	
	public void toFile(String fileName) {
		File file = new File(fileName + ".csv");
		file.getParentFile().mkdirs();
		try (PrintWriter pw = new PrintWriter(file)) {
			writeStats(pw);
			pw.close();
		} catch (FileNotFoundException ex) {
			Logger.getLogger(Stats.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
