/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jd3xx;

import java.io.PrintWriter;

/**
 *
 * @author ndassieu
 */
public class BufferStats extends Stats {
	private final int MAXSAMPLESINBUFFER = 0xFFFF;
	private long[] samplesInBuffer = new long[MAXSAMPLESINBUFFER+1];
	private int maxIndex = 0;
	
	public BufferStats () {
	}
	
	@Override
	public synchronized void resetStats() {
		samplesInBuffer = new long[MAXSAMPLESINBUFFER+1];
		maxIndex = 0;
	}
	
	public void addValue(int value) {
		if (running) {
			if (value < samplesInBuffer.length) {
				samplesInBuffer[value]++;
				if (value > maxIndex) {
					maxIndex = value;
				}
			} else {
				samplesInBuffer[samplesInBuffer.length-1]++;
			}
		}
	}

	@Override
	public synchronized String ToStringPartialStats() {
		return "";
	}

	@Override
	public String toString() {
		StringBuilder ans = new StringBuilder();
		int samplesPerLine = 32;
		int linesToPrint = 5;
		int limit = samplesPerLine*linesToPrint;
		ans.append("MaxBufferSize[").append(maxIndex).append("]=");
		ans.append(samplesInBuffer[maxIndex]).append("\n");
//		ans.append("First ").append(limit).append(" samples in buffer");
//		for (int i=0; i<limit;i++) {
//			if (i%samplesPerLine==0) {
//				ans.append("\n");
//			} else {
//				ans.append(";");
//			}
//			ans.append(samplesInBuffer[i]);
//		}
//		ans.append("\n");
		return ans.toString();
	}

	@Override
	protected void writeStats(PrintWriter pw) {
		for (int i=0; i<samplesInBuffer.length; i++) {
			pw.write(String.format("%d\n", samplesInBuffer[i]));
		}
	}
}
