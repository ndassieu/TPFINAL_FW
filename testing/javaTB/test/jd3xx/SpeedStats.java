/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jd3xx;

import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author ndassieu
 */
public class SpeedStats  extends Stats {
	private long totalSize = 0;
	private long totalTimeNs = 0;
	private long totalPackets = 0;
	
	
	@Override
	public synchronized void resetStats() {
		totalSize = 0;
		totalTimeNs = 0;
		totalPackets = 0;
	}
	
	public synchronized void addPacket(long packetSize, long packetTimeNs) {
		if (running) {
			totalSize += packetSize;
			totalTimeNs += packetTimeNs;
			totalPackets++;
		}
	}

	@Override
	public synchronized String ToStringPartialStats() {
		long speedMbps=0;
		if (totalTimeNs!=0) {
			speedMbps = Math.round(((double)(totalSize*1000*8))/(totalTimeNs));
		}
		return "Avg Speed: " + speedMbps + "Mbps = " + speedMbps/8 + " MBPs";
	}

	@Override
	public String toString() {
		return "Recibidos "+totalSize+" Bytes en "+TimeUnit.NANOSECONDS.toMillis(totalTimeNs)
				+"ms, en "+totalPackets + " paquetes\n"+ToStringPartialStats();
	}

	@Override
	protected void writeStats(PrintWriter pw) {
		StringBuilder sb = new StringBuilder();
        sb.append(totalSize);
        sb.append(',');
        sb.append(totalTimeNs);
        sb.append(',');
        sb.append(totalPackets);
        sb.append('\n');
		pw.write(sb.toString());
	}
	
	
}
