/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jd3xx;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jd3xx.ThreadMessage.ExepctionMsg;
import jd3xx.ThreadMessage.Sleep;

/**
 *
 * @author ndassieu
 */
public class RxThread extends AbstractThread {
	private final JD3XX jd3xx;
	private final int channel;
			
	public RxThread(JD3XX jd3xx, int channel, int inDataQueueSize, int inCommandQueueSize) {
		super(inDataQueueSize, inCommandQueueSize);
		this.jd3xx = jd3xx;
		this.channel = channel;
	}

	@Override
	public void run() {
		try {
			while (true) {
				ThreadMessage msg = inMessageQueue.take();
				
				if (msg.getClass().isAssignableFrom(ThreadMessage.PacketMode.class)) {
					waitPacket(((ThreadMessage.PacketMode)msg).getSize());
					sendMessage(new Sleep(this));
				} else if (ThreadMessage.StreamingMode.class.isAssignableFrom(msg.getClass())) {
					try {
						streamingMode(((ThreadMessage.StreamingMode)msg).getSize());
						sendMessage(new Sleep(this));
						flushLeftPackets(((ThreadMessage.StreamingMode)msg).getSize());
						sendMessage(new Sleep(this));
					} catch (IOException ex) {
						sendMessage(new ExepctionMsg( ex, this));
					}
				}
			}
		} catch (InterruptedException ex) {
			Logger.getLogger(RxThread.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	private void streamingMode(int packetSize) throws IOException {
		boolean notDone = true;
		ThreadMessage msg;
		long overlapped[] = new long[5];
		int i;
		for (i=0; i<overlapped.length; i++) {
			overlapped[i] = jd3xx.initializeOverlapped();
			jd3xx.startReadPipe(getRxPipeID(channel), packetSize, overlapped[i]);
		}
		byte []buffer;
		int countData;
		i=0;
		while (notDone) {
			buffer = createOrRecycle(packetSize);
			countData = jd3xx.getOverlappedResults(getRxPipeID(channel), buffer, 0, overlapped[i]);
			if (countData != buffer.length) {
				throw new IOException("Invalid Packet Length Received, expected" + 
						buffer.length + " received " + countData);
			} else {
				sendData(buffer);
			}
			jd3xx.startReadPipe(getRxPipeID(channel), packetSize, overlapped[i]);
			i = (i<overlapped.length-1)?i+1:0;
			msg = inMessageQueue.poll();
			if ((msg!=null) && (msg.getClass().isAssignableFrom(ThreadMessage.Sleep.class))) {
				notDone = false;
			}
		}
		jd3xx.abortPipe(getRxPipeID(channel));
		for (i=0;i<overlapped.length;i++) {
			jd3xx.releaseOverlapped(overlapped[i]);
		}	
	}
	
	
	private boolean waitPacket(int packetSize) {
		int countData;
		boolean ans = false;
		byte []buffer = createOrRecycle(packetSize);
		try {
			// recibimos el resultado
			countData = jd3xx.readPipe(getRxPipeID(channel), buffer, 0, buffer.length);
			if (countData != buffer.length) {
				this.sendMessage(new ExepctionMsg( new IOException(
						"Invalid Packet Length Received, expected" + 
						buffer.length + " received " + countData), this));
			} else {
				//System.out.println("Packet received un RxThread");
				sendData(buffer);
				ans = true;
			}
		} catch (IOException ex) {
			this.sendMessage(new ExepctionMsg(ex, this));
		}
		return ans;
	}
	
	private void flushLeftPackets(int packetSize)  {
		boolean notDone = true;
		byte []buffer;
		buffer = createOrRecycle(packetSize);
		int countData;
		while (notDone) {
			try {
				countData = jd3xx.readPipe(getRxPipeID(channel), buffer, 0, packetSize);
				if (countData ==0) {
					notDone = false;
				}
			} catch (IOException ex) {
				// Ocurrió timeout salimos abortamos el pipe y salimos
				notDone = false;
			}
		}
		try {
			jd3xx.abortPipe(getRxPipeID(channel));
		} catch (IOException ex) {
			this.sendMessage(new ExepctionMsg(ex, this));
		}
				
	}
	
	
	private static byte getRxPipeID(int channel) {
		return (byte) (0x80|(channel+2));
	}
}
