/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jd3xx;

import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import static jd3xx.CommPacket.buildExpectedRamp;
import static jd3xx.CommPacket.buildRunMode;
import static jd3xx.CommPacket.buildStopMode;
import static jd3xx.CommPacket.getExpectedRampPacketLength;
import static jd3xx.CommPacket.getRxPipeID;
import static jd3xx.CommPacket.getTxPipeID;
import jd3xx.ThreadMessage.ExepctionMsg;
import jd3xx.ThreadMessage.Sleep;
import jd3xx.ThreadMessage.StreamingBufferMode;
import jd3xx.ThreadMessage.StreamingMode;
import jd3xx.ThreadMessage.StreamingRampMode;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author ndassieu
 */
public class TestController {
	/** If  a packet size at current Speed takes less than MIN_PACKET_TIME_US
	 * to be transmitted, receive N packets together to ensure time between pipe
	 * events is no less than MIN_PACKET_TIME_US microseconds
	 **/
	private static final int MIN_PACKET_TIME_US = 2000;
	private static final int DUTY_CYCLE_STEPS = 32;
	
	public static final int FIRMWARE_CHANNELS = 1;
	public static final int FT601_CLK_MHZ = 66;
	
	public static final long TEST_WARMUP_SECS = 10;
	
	private static final int DATA_QUEUE_SIZE = 100;
	private static final int CMD_QUEUE_SIZE = 30;
	
	long msTimeout;
	private final JD3XX jd3xx;
	private final RxThread[] rxThreads = new RxThread[FIRMWARE_CHANNELS];
	private final CheckRxDataThread[] checkRxDataThreads = new CheckRxDataThread[FIRMWARE_CHANNELS];
	private final ArrayBlockingQueue<ThreadMessage> threadMessages;
			
	public TestController(JD3XX jd3xx, long msTimeout) {
		this.msTimeout = msTimeout;
		this.jd3xx = jd3xx;
		threadMessages = new ArrayBlockingQueue<>(100);
		for (int i=0; i<FIRMWARE_CHANNELS; i++) {
			rxThreads[i] = new RxThread(jd3xx, i, DATA_QUEUE_SIZE, CMD_QUEUE_SIZE);
			checkRxDataThreads[i] = new CheckRxDataThread(DATA_QUEUE_SIZE, CMD_QUEUE_SIZE, msTimeout);
			// Data queue
			rxThreads[i].setOutDataQueue(checkRxDataThreads[i].getInDataQueue());
			// Recycling queue;
			checkRxDataThreads[i].setOutDataQueue(rxThreads[i].getInDataQueue());
			rxThreads[i].setOutMessageQueue(threadMessages);
		}
		
	}
	
	public void start() {
		for (int i=0; i<FIRMWARE_CHANNELS; i++) {
			rxThreads[i].start();
			checkRxDataThreads[i].start();
		}
	}
	
	public void runRxChannelBufferTest(int channel, int packetBytes, int percentBW, 
			long testDuration, String outputFolder) throws IOException {
		System.out.println("runBufferTest");
		int packetLength = getRampPacketLen(packetBytes);
		int divisor = getFreqDC(percentBW);
		
		int Npackets = packTogether(packetBytes, getExpectedMbps(divisor), MIN_PACKET_TIME_US);
		
		streamChannel(channel, Npackets, packetLength, 0, divisor, testDuration,
				new StreamingBufferMode(Npackets*getExpectedRampPacketLength(packetLength), Npackets));
		
		System.out.println("runBufferTest Passed");
		System.out.println(checkRxDataThreads[channel].getSpeedStats().toString());
		System.out.println(checkRxDataThreads[channel].getBufferStats().toString());
		String testFile = generateTestFileName(outputFolder, "buffer", channel,
				packetBytes, getExpectedMbps(divisor),testDuration);
		checkRxDataThreads[channel].getSpeedStats().toFile(testFile+"_Speed");
		checkRxDataThreads[channel].getBufferStats().toFile(testFile+"_Buffer");
	}
	
	private int getRampPacketLen(int packetBytes) {
		return packetBytes/4-2;
	}
	
	private static int packTogether(int packetSize, float speedMbps, int minPacketTimeUs) {
		int ans = Math.round(minPacketTimeUs/(packetSize/(speedMbps/8)));
		if (ans <1) {
			ans = 1;
		}
		return ans;
	}
	
	/**
	 * Devuelve el entero a colocar en el DC de frecuencia para obtenere el 
	 * porcentaje de ancho de banda deseado
	 * @param dcPercent Porcentaje de ancho de banda
	 * @return Divisor a colocar en el registro de configuración
	 */
	private int getFreqDC(int dcPercent) {
		// dcPercent = (32-FreqDC)/32*100
		// => FreqDC = Math.Round(32-dcPercent/100*32);
		return Math.round(DUTY_CYCLE_STEPS-(((float)dcPercent)/100F*DUTY_CYCLE_STEPS));
	}
	
	private int getExpectedMbps(int freqDC) {
		// dc = (32-FreqDC)/32
		float dc = (DUTY_CYCLE_STEPS-((float)freqDC))/DUTY_CYCLE_STEPS;
		return Math.round(FT601_CLK_MHZ*dc*4*8);
	}
	
	/**
	 * Test de recepción de rampa de un canal
	 * MUY IMPORTANTE: para que funcione el test con divisores mayores que 0 el
	 * chip debe estar en modo Ignore Session Underrun On Multiple of FIFO
	 * bus-with bytes
	 * @param channel
	 * @param rampLength
	 * @param rampIncrement
	 * @param testDuration Duración del test en segundos
	 */
	public void runRxChannelRampTest(int channel, int packetBytes, int rampIncrement, 
			int percentBW, long testDuration, String outputFolder) throws IOException {
		byte[] expectedBuffer;
		System.out.println("runRampTest");
		int rampLength = getRampPacketLen(packetBytes);
		int divisor = getFreqDC(percentBW);
		
		int Npackets = packTogether(packetBytes, getExpectedMbps(divisor), MIN_PACKET_TIME_US);
		
		
		// creamos la rampa espera de respuesta
		expectedBuffer = buildExpectedRamp(Npackets, rampLength, rampIncrement); 
		streamChannel(channel, Npackets, rampLength, rampIncrement, divisor, testDuration,
				new StreamingRampMode(expectedBuffer));
		
		System.out.println("runRampTest Passed");
		System.out.println(checkRxDataThreads[channel].getSpeedStats().toString());
		String testFile = generateTestFileName(outputFolder, "ramp", channel,
				packetBytes, getExpectedMbps(divisor),testDuration);
		checkRxDataThreads[channel].getSpeedStats().toFile(testFile+"_Speed");
	}
	
	private String generateTestFileName(String outputFolder, String type, 
			int channel, int packetBytes, int expectedMbps, long testDuration) {
		StringBuilder sb = new StringBuilder();
        sb.append(outputFolder);
		sb.append(type).append(channel).append("_");
		sb.append(packetBytes).append("B_");
		sb.append(expectedMbps).append("Mbps_");
		sb.append(testDuration).append("s");
		return sb.toString();
	}
	
	private void streamChannel(int channel, int Npackets, int rampLength, int rampIncrement,
			int divisor, long testDuration, ThreadMessage streamingMode) throws IOException {
		long startTime, curTime;
		
		SpeedStats speedStats = checkRxDataThreads[channel].getSpeedStats();
		speedStats.resetStats();
		BufferStats bufferStats = checkRxDataThreads[channel].getBufferStats();
		bufferStats.resetStats();
		
		ThreadMessage msg;
		enterStreamingMode(channel, Npackets, rampLength, rampIncrement, divisor);
		
		// Enviamos el paquete esperado y ponemos en modo streaming
		rxThreads[channel].getInMessageQueue().add(streamingMode);
		checkRxDataThreads[channel].getInMessageQueue().add(streamingMode);
		// esperamos que transcurra el tiempo de test
		startTime = System.nanoTime();
		curTime = startTime;
		long lastReportTime = startTime;
		boolean statsRunning = false;
		try {
			while ((curTime-startTime)<(TimeUnit.SECONDS.toNanos(testDuration+TEST_WARMUP_SECS))) {
				if ((!statsRunning)&&((curTime-startTime)>TimeUnit.SECONDS.toNanos(TEST_WARMUP_SECS))) {
					statsRunning = true;
					speedStats.playStats();
					bufferStats.playStats();
				} 
				if ((curTime-lastReportTime)>TimeUnit.SECONDS.toNanos(1)){
					System.out.print("Progress: " + 100*(curTime-startTime)/
							TimeUnit.SECONDS.toNanos(testDuration+TEST_WARMUP_SECS) + "% [");
					System.out.println(speedStats.ToStringPartialStats() + "]");
					lastReportTime = curTime;
				}
				// Chequeamos si hay mensajes de los threads
				msg = threadMessages.poll(100, TimeUnit.MILLISECONDS);
				if ((msg!=null) && (msg.getClass().isAssignableFrom(ExepctionMsg.class))) {
					Logger.getLogger(TestController.class.getName()).log(Level.SEVERE, null,
							((ExepctionMsg)msg).getExeption());
					break;
				}
				curTime = System.nanoTime();
			}
		} catch (InterruptedException ex) {
			Logger.getLogger(TestController.class.getName()).log(Level.SEVERE, null, ex);
		}
		speedStats.pauseStats();
		bufferStats.pauseStats();
		
		stopStreamingMode(channel, rampLength, rampIncrement, divisor);
	}
	
	private void enterStreamingMode(int channel, int Npackets, int rampLength, int rampIncrement, 
			int divisor) throws IOException {
		int countData;
		byte[] configPacket;
		
		jd3xx.setStreamPipe(false, false, getRxPipeID(channel), Npackets*(4*rampLength+8));
		
		configPacket = buildRunMode(rampLength, rampIncrement, divisor);
		countData = jd3xx.writePipe(getTxPipeID(channel), configPacket, 0, configPacket.length);
		assertEquals("Bytes escritos distintos de bytes a escribir", countData, configPacket.length);
	}
	
	private void stopStreamingMode(int channel, int rampLength, int rampIncrement,
			int divisor) throws IOException {
		int countData;
		byte[] configPacket;
		// Enviamos la detension a todos los threads
		sleepThread(rxThreads[channel], 5*msTimeout);
		
		// Detenemos la corrida
		configPacket = buildStopMode(rampLength, rampIncrement, divisor);
		countData = jd3xx.writePipe(getTxPipeID(channel), configPacket, 0, configPacket.length);
		assertEquals("Bytes escritos distintos de bytes a escribir", countData, configPacket.length);
		
		sleepThread(checkRxDataThreads[channel], 5*msTimeout);
		
		
		jd3xx.clearStreamPipe(false, false, getRxPipeID(channel));
	}
	
	private void sleepThread(AbstractThread threadToSleep, long msTimeout) {
		threadToSleep.getInMessageQueue().add(new Sleep());
		waitThreadSleep(msTimeout);
//		try {
//			msg = threadMessages.poll(msTimeout, TimeUnit.MILLISECONDS);
//			if (msg!=null) {
//				assertEquals("Mensaje invalido Recibido", msg.getClass().isAssignableFrom(Sleep.class),true);
//			}
//		} catch (InterruptedException ex) {
//			Logger.getLogger(TestController.class.getName()).log(Level.SEVERE, null, ex);
//		}
	}
	
	private void waitThreadSleep(long msTimeout) {
		ThreadMessage msg;
		try {
			msg = threadMessages.poll(msTimeout, TimeUnit.MILLISECONDS);
			if (msg!=null) {
				assertEquals("Mensaje invalido Recibido", msg.getClass().isAssignableFrom(Sleep.class),true);
			}
		} catch (InterruptedException ex) {
			Logger.getLogger(TestController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
