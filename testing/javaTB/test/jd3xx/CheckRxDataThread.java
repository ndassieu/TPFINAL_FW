/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jd3xx;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import static jd3xx.CommPacket.getIntegerFromBuffer;
import jd3xx.ThreadMessage.ExepctionMsg;
import jd3xx.ThreadMessage.HasPacket;
import jd3xx.ThreadMessage.Sleep;

/**
 *
 * @author ndassieu
 */
public class CheckRxDataThread extends AbstractThread {
	private byte[] expectedPacket;
	private final long msTimeOut;
	
	private final SpeedStats speedStats;
	private final BufferStats bufferStats;
	
	public CheckRxDataThread(int inDataQueueSize, int inCommandQueueSize, long msTimeOut) {
		super(inDataQueueSize, inCommandQueueSize);
		this.msTimeOut = msTimeOut;
		speedStats = new SpeedStats();
		bufferStats = new BufferStats();
	}
	
	public BufferStats getBufferStats() {
		return bufferStats;
	}

	public SpeedStats getSpeedStats() {
		return speedStats;
	}
	
	@Override
	public void run() {
		try {
			while (true) {
				ThreadMessage msg = inMessageQueue.take();
				if (HasPacket.class.isAssignableFrom(msg.getClass())) {
					expectedPacket = Arrays.copyOf(((HasPacket)msg).getPacket(), 
							((HasPacket)msg).getSize());
				}
				if (msg.getClass().isAssignableFrom(ThreadMessage.PacketMode.class)) {
					waitForExpectedPacket();
					sendMessage(new Sleep(this));
				} else if (msg.getClass().isAssignableFrom(ThreadMessage.StreamingRampMode.class)) {
					streamingRampMode();
					sendMessage(new Sleep(this));
				} else if (msg.getClass().isAssignableFrom(ThreadMessage.StreamingBufferMode.class)) {
					streamingBufferMode(((ThreadMessage.StreamingBufferMode)msg).getSize(),
							((ThreadMessage.StreamingBufferMode)msg).getPacketsPerStream());
					sendMessage(new Sleep(this));
				}
			}
		} catch (InterruptedException ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	private void streamingBufferMode(int size, int packetsPerStream) {
		boolean notDone = true;
		ThreadMessage msg;
		long lastTime = System.nanoTime();
		long curTime;
		while (notDone) {
			notDone = waitForBufferPacket(size, packetsPerStream);
			curTime = System.nanoTime();
			speedStats.addPacket(size, curTime-lastTime);
			lastTime = curTime;
			msg = inMessageQueue.poll();
			if ((msg!=null)&&(msg.getClass().isAssignableFrom(ThreadMessage.Sleep.class))) {
				notDone = false;
			}
		}
		flushLeftPackets();
	}
	
	private boolean waitForBufferPacket(int size, int packetsPerStream) {
		boolean ans = true;
		byte[] actualPacket;
		int bytesInBuffer;
		int lastByteInBuffer;
		try {
			actualPacket = inDataQueue.poll(msTimeOut, TimeUnit.MILLISECONDS);
			lastByteInBuffer=0;
		
			if (actualPacket != null) {
				//System.out.println("Packet Received in un CheckRxDataThread");
			
				if (actualPacket.length == size) {
					int packetSize = size/packetsPerStream;
					for (int j=0; j<packetsPerStream;j++) {
						for (int i=2+(packetSize*j)/4; i<(packetSize*(j+1)/4); i++) {					
							bytesInBuffer = getIntegerFromBuffer(actualPacket,i);
							//if ((bytesInBuffer!=0) && ((lastByteInBuffer-1)!=bytesInBuffer)) {
							//	System.out.print("[" + i + "]" +bytesInBuffer + ";");
							//}
							//lastByteInBuffer = bytesInBuffer; 
							bufferStats.addValue(bytesInBuffer);
						}
					}
					//System.out.print("\n");
				} else {
					this.sendMessage(new ExepctionMsg( new IOException(
							"Invalid Packet Length Received, expected" + 
							size + " received " + actualPacket.length), this));
					ans = false;
				}
				dispose(actualPacket);
			}
		} catch (InterruptedException ex) {
			Logger.getLogger(CheckRxDataThread.class.getName()).log(Level.SEVERE, null, ex);
		}
		return ans;
	}
	
	private void streamingRampMode() {
		boolean notDone = true;
		ThreadMessage msg;
		long lastTime = System.nanoTime();
		long curTime;
		while (notDone) {
			notDone = waitForExpectedPacket();
			curTime = System.nanoTime();
			speedStats.addPacket(expectedPacket.length, curTime-lastTime);
			lastTime = curTime;
			msg = inMessageQueue.poll();
			if ((msg!=null)&&(msg.getClass().isAssignableFrom(ThreadMessage.Sleep.class))) {
				notDone = false;
			}
		}
		flushLeftPackets();
	}
	
	private void flushLeftPackets() {
		boolean notDone = true;
		byte[] actualData;
		while (notDone) {
			try {
				actualData = inDataQueue.poll(msTimeOut, TimeUnit.MILLISECONDS);
				notDone = (actualData!=null);
			} catch (InterruptedException ex) {
				Logger.getLogger(CheckRxDataThread.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}
	
	private boolean waitForExpectedPacket() {
		boolean ans = true;
		byte[] actualPacket;
		try {
			actualPacket = inDataQueue.poll(msTimeOut, TimeUnit.MILLISECONDS);
			
			if (actualPacket != null) {
				if (actualPacket.length == expectedPacket.length) {
					for (int i=0; i<expectedPacket.length; i++) {
						if (actualPacket[i] != expectedPacket[i]) {
							this.sendMessage(new ExepctionMsg( new IOException(
								"Invalid Data Received, expected" + 
								expectedPacket[i] + " received " + actualPacket[i] + 
								"at index " + i), this));
							ans = false;
							break;
						}
					}
				} else {
					this.sendMessage(new ExepctionMsg( new IOException(
							"Invalid Packet Length Received, expected" + 
							expectedPacket.length + " received " + actualPacket.length), this));
					ans = false;
				}
				dispose(actualPacket);
			}
		} catch (InterruptedException ex) {
			Logger.getLogger(CheckRxDataThread.class.getName()).log(Level.SEVERE, null, ex);
		}
		return ans;
	}
}
