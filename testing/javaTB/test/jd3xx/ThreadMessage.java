/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jd3xx;

/**
 *
 * @author ndassieu
 */
public class ThreadMessage  {

	private AbstractThread msgSource;
	
	public static class Sleep extends ThreadMessage {
		public Sleep() {
		}
		
		public Sleep(AbstractThread src) {
			setSource(src);
		}
	}
	
	public static class ExepctionMsg extends ThreadMessage {
		private final Exception ex;
		
		public ExepctionMsg(Exception ex, AbstractThread src) {
			this.ex = ex;
			setSource(src);
		}
		
		public Exception getExeption() {
			return ex;
		}
	}
	
	public static class StreamingRampMode extends HasPacket implements StreamingMode {
		public StreamingRampMode(byte[] expected) {
			super(expected);
		}
	}
	
	public static class StreamingBufferMode extends HasSize implements StreamingMode {
		int packetsPerStream;
		public StreamingBufferMode(int size, int packetsPerStream) {
			super(size);
			this.packetsPerStream = packetsPerStream;
		}
		
		public int getPacketsPerStream() {
			return packetsPerStream;
		}
	}
	
	public interface StreamingMode {
		public int getSize();
	}
	
	public static class PacketMode extends HasPacket {
		public PacketMode(byte[] packet) {
			super(packet);
		}
	}
	
	public final void setSource(AbstractThread msgSource) {
		this.msgSource = msgSource;
	}
	
	public final AbstractThread getSource() {
		return msgSource;
	}
	
	public static class HasPacket extends HasSize {
		byte[] packet;
		
		public HasPacket(byte[] packet) {
			super(packet.length);
			this.packet = packet;
		}
		
		public byte[] getPacket() {
			return packet;
		}
	}
	
	public static class HasSize  extends ThreadMessage {
		int size;
		public HasSize(int size) {
			this.size = size;
		}
		
		public int getSize() {
			return size;
		}
	}
}
