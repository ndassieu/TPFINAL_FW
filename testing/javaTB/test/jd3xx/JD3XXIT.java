/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jd3xx;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static jd3xx.CommPacket.buildHeader;
import static jd3xx.CommPacket.getRxPipeID;
import static jd3xx.CommPacket.getTxPipeID;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ndassieu
 */
public class JD3XXIT {
	private static final String OUTPUT_FOLDER = "testOutputs\\";
	private static final int HEADER_LEN = 8;
	private static final int LOOBACK_TX_ADDR = 0x0001;
	private static final int LOOBACK_RX_ADDR = 0x8001;
	private static final int FIRMWARE_CHANNELS = 1;
	private static final int MAX_RAMP_PACKET = 16777192;
	
	private static final int RX_PIPE_MS_TIMEOUT = 1000;
	// Tiempo del test de rampa en segundos
	private static final int RUN_RAMP_CHANNEL = 0;
	private static final int RUN_RAMP_TEST_TIME = 60*5;
	private static final int RUN_RAMP_INCREMENT =  1;//(int)((0xFFFFFFFFL)/(RUN_RAMP_PACKET_LEN-1));
	
	private static final int[] PACKET_LENGTHS = {16777216, 4194304, 1048576, 131072, 1024}; 
	private static final int[] FAST_SPPED_PACKET_LENGTHS = {16777216, 4194304, 1048576, 131072, 1024}; 
	private static final int[] BUFFER_SPEEDS = {70,  65, 40, 15};
	
	private static final byte[] txBuffer = new byte[4*MAX_RAMP_PACKET+HEADER_LEN];
	private static final byte[] expectedBuffer = new byte[4*MAX_RAMP_PACKET+HEADER_LEN];
	private static final byte[] rxBuffer = new byte[4*MAX_RAMP_PACKET+HEADER_LEN];
	
	private static JD3XX jd3xx;
	
	private static TestController testController;
	
	public JD3XXIT() {
	}
	
	@BeforeClass
	public static void setUp() {
		jd3xx = new JD3XX();
			
		try {
			int n = jd3xx.createDeviceInfoList();
			System.out.println("Devices Connected:" + n);
			
			assertNotEquals("No hay dispositivos conectados", n, 0);
			
			System.out.println("List of Connected Devices!"); 
			printConnectedDevices(n);
				
			System.out.println("Opening device FTDI SuperSpeed-FIFO Bridge"); 
			jd3xx.openByDescription("FTDI SuperSpeed-FIFO Bridge");
			
			for (int i=0; i<FIRMWARE_CHANNELS; i++) {
				jd3xx.setPipeTimeout(getRxPipeID(i), RX_PIPE_MS_TIMEOUT);
			}
		} catch (IOException ex) {
			Logger.getLogger(JD3XXIT.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		testController = new TestController(jd3xx, RX_PIPE_MS_TIMEOUT);
		testController.start();
	}
	
	@AfterClass
	public static void tearDown() {
		System.out.println("Closing device 0"); 
		try {
			jd3xx.close();
		} catch (IOException ex) {
			Logger.getLogger(JD3XXIT.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Test
	public void runBufferTest() {
		try {
			for (int j=0; j<BUFFER_SPEEDS.length; j++) {
				for (int i=0; i<PACKET_LENGTHS.length; i++) {
					testController.runRxChannelBufferTest(RUN_RAMP_CHANNEL, PACKET_LENGTHS[i],
							BUFFER_SPEEDS[j], RUN_RAMP_TEST_TIME, OUTPUT_FOLDER);
				}
			}
		} catch (IOException ex) {
			Logger.getLogger(JD3XXIT.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	@Test
	public void runRampTest() {
		try {
			for (int i=0; i<FAST_SPPED_PACKET_LENGTHS.length; i++) {
				System.out.print("Speed test " + (i+1) + " of " +FAST_SPPED_PACKET_LENGTHS.length);
				System.out.println(". Packet Size " + FAST_SPPED_PACKET_LENGTHS[i] +
						"Bytes @ max speed");
				testController.runRxChannelRampTest(RUN_RAMP_CHANNEL, FAST_SPPED_PACKET_LENGTHS[i],
						RUN_RAMP_INCREMENT, 100, RUN_RAMP_TEST_TIME, OUTPUT_FOLDER);
			}
		} catch (IOException ex) {
			Logger.getLogger(JD3XXIT.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	//@Test
	public void loopbackTest() {
		System.out.println("loopbackTest");
		
		for (int i=0; i<FIRMWARE_CHANNELS; i++) {
			System.out.println("Looback Channel: " + i);
			for (int j=1; j<100; j++) {
				try {
					loopbackArrayTest(i, j);
				} catch (IOException ex) {
					Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		System.out.println("loopbackTest Passed");
	}
	
	private static void loopbackArrayTest(int channel, int arraySize) throws IOException {
		byte []txbuffer = new byte[4*(arraySize+2)];
		byte []rxbuffer = new byte[4*(arraySize+2)];
		long packetLen = arraySize-1;
		
		for (int i=8; i< txbuffer.length; i++) {
			txbuffer[i] = (byte)(Math.random()*255);
		}
		buildHeader(txbuffer, LOOBACK_TX_ADDR, packetLen);
		int countData = jd3xx.writePipe(getTxPipeID(channel), txbuffer, 0, txbuffer.length);
		assertEquals("Bytes escritos distintos de bytes a escribir", countData, txbuffer.length); 
		
		countData = jd3xx.readPipe(getRxPipeID(channel), rxbuffer, 0, rxbuffer.length);
		assertEquals("Bytes leidos distintos de bytes a leer", countData, rxbuffer.length); 
		
		// Checkeamos el paquete recibido
		checkPacket(LOOBACK_RX_ADDR, packetLen, txbuffer, HEADER_LEN, rxbuffer);
	}
	
	private static void checkPacket(int expectedHeader, long expectedPacketLength,
			byte[] expectedData, int expectedDataOffset, byte[]actual) {
		checkHeader(expectedHeader, expectedPacketLength, actual);
		checkData(expectedData, expectedDataOffset, actual);
	}
	
	private static void checkData(byte[]expected, int expectedDataOffset , byte[]actual) {
		checkData(expected, expectedDataOffset, actual.length-expectedDataOffset, actual);
	}
	
	private static void checkData(byte[]expected, int expectedDataOffset, int length , byte[]actual) {
		for (int i=expectedDataOffset; i<expectedDataOffset+length; i++) {
			assertEquals("Valor leido difiere del esperado", expected[i], actual[i]);
		}
	}
	
	private static boolean checkHeader(int expectedHeader, long expectedPacketLength, byte[] actual) {
		boolean notFail = true;
		byte []expectedArray = new byte[HEADER_LEN];
		buildHeader(expectedArray, expectedHeader, expectedPacketLength);
		
		for (int i=0; i<HEADER_LEN; i++) {
			if (expectedArray[i]!=actual[i]) {
				System.out.println("Header( " +i+ ") = " + actual[i] + 
							" valor esperado:" + expectedArray[i]);
				notFail = false;
			}
		}
		return notFail;
	}
	
	private static void printConnectedDevices(int n) throws IOException {
		JD3XX.DeviceInfo curDevice;
		for (int i=0; i<n; i++) {
			curDevice = jd3xx.getDeviceInfoDetail(i);
			System.out.println(curDevice.toString()); 
		}
	}
}
