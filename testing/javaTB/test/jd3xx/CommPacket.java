/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jd3xx;

import java.io.IOException;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author ndassieu
 */
public class CommPacket {
	private static final int LOOBACK_TX_ADDR = 0x0001;
	private static final int LOOBACK_RX_ADDR = 0x8001;
	private static final int RAMP_TX_ADDR = 0x0002;
	private static final int RAMP_RX_ADDR = 0x8002;
	private static final int BUFFERSIZE_RX_ADDR = 0x8003;
	private static final int TEST_CFG_ADDR = 0x0004;
	private static final int HEADER_LEN = 8;
	
	public static byte[] buildExpectedRamp(int Npackets, int rampLength, int rampIncrement) {
		int rampValue = 0; 
		byte []expectedBuffer = new byte[Npackets*(4*rampLength+HEADER_LEN)];
		byte []temp = new byte [(4*rampLength+HEADER_LEN)];
		buildHeader(temp, RAMP_RX_ADDR, rampLength-1);
		for (int i=HEADER_LEN/4; i< rampLength+HEADER_LEN/4; i++) {
			addIntegerToBuffer(temp, i, rampValue);
			rampValue += rampIncrement;
		}
		for (int j=0; j<Npackets;j++) {
			System.arraycopy( temp, 0, expectedBuffer, j*temp.length , temp.length );
		}
		return expectedBuffer;
	}
	
	public static int getExpectedRampPacketLength(int rampLength) {
		return 4*rampLength+HEADER_LEN;
	}
	
	/**
	 * 
	 * @param length Largo de cada paquete de ramp
	 * @param rampIncrement Incrmento de la ramp, incremento =0 => bufferSizeMode
	 * @param div divisor de la frecuencia de la rampa
	 */
	public static byte[] buidStepMode(int length, int rampIncrement, int div) {
		byte [] d1, d2, ans;
		d1 = buildConfigTxMode(length, rampIncrement, div, false, true);
		d2 = buildConfigTxMode(length, rampIncrement, div, true, true);
		ans = new byte[d1.length+d2.length];
		System.arraycopy(d1, 0, ans, 0, d1.length);
		System.arraycopy(d2, 0, ans, d1.length, d2.length);
		return ans;
	}
	
	public static byte[] buildRunMode(int length, int rampIncrement, int div) {
		byte [] d1, d2, ans;
		d1 = buildConfigTxMode(length, rampIncrement, div, false, false);
		d2 = buildConfigTxMode(length, rampIncrement, div, true, false);
		ans = new byte[d1.length+d2.length];
		System.arraycopy(d1, 0, ans, 0, d1.length);
		System.arraycopy(d2, 0, ans, d1.length, d2.length);
		return ans;
	}
	
	public static byte[] buildStopMode(int length, int rampIncrement, int div) {
		return buildConfigTxMode(length, rampIncrement, div, false, false);
	}
	
	private static byte[] buildConfigTxMode(int length, int rampIncrement, int div, 
		boolean run, boolean step) {
		byte[] buffer = new byte[HEADER_LEN+4*4];
		buildHeader(buffer, TEST_CFG_ADDR, 3);
		int offset  = HEADER_LEN/4;
		int cfg = (run)?1<<7:0;
		cfg += (rampIncrement!=0)?1<<1:0;
		cfg += (step)?1:0;
		addIntegerToBuffer(buffer, offset, length-1);
		addIntegerToBuffer(buffer, offset+1, rampIncrement);
		addIntegerToBuffer(buffer, offset+2, div);
		addIntegerToBuffer(buffer, offset+3, cfg); 
		return buffer;
	}
	
	public static void buildHeader(byte[] data, int header, long packetlength) {
		data[0] = (byte)((header>>>8)&0xFF);
		data[1] = (byte)(header&0xFF);
		data[2] = (byte)((packetlength>>>24)&0xFF);
		data[3] = (byte)((packetlength>>>16)&0xFF);
		data[4] = (byte)((packetlength>>>8)&0xFF);
		data[5] = (byte)(packetlength&0xFF);
		int crc = computeCrc(data);
		data[6] = (byte)((crc>>>8)&0xFF);	
		data[7] = (byte)(crc&0xFF);
	}	
	
	public static int computeCrc(byte[] data) {
		int accum=0;
		for (int i=0; i<6; i++) {
			accum+=((long)data[i])&0xFF;
		}
		accum = ~accum;
		return (accum&0xFFFF);
	}
	
	public static void addIntegerToBuffer(byte[]buffer, int wordOffset, int data) {
		buffer[4*wordOffset] = (byte)((data>>>24)&0xFF);
		buffer[4*wordOffset+1] = (byte)((data>>>16)&0xFF);
		buffer[4*wordOffset+2] = (byte)((data>>>8)&0xFF);
		buffer[4*wordOffset+3] = (byte)(data&0xFF);
	}
	
	public static int getIntegerFromBuffer(byte[]buffer, int wordOffset) {
		int data;
		data = (((int)buffer[4*wordOffset])&0xFF)<<24;
		data |= (((int)buffer[4*wordOffset+1])&0xFF)<<16;
		data |= (((int)buffer[4*wordOffset+2])&0xFF)<<8;
		data |= (((int)buffer[4*wordOffset+3])&0xFF);
		return data;
	}
	
	public static byte getRxPipeID(int channel) {
		return (byte) (0x80|getTxPipeID(channel));
	}
	
	public static byte getTxPipeID(int channel) {
		return (byte) (channel+2);
	}
	
	public static void runMode(JD3XX jd3xx, int channel, int length, int rampIncrement, int div) throws IOException {
		configTxMode(jd3xx, channel, length, rampIncrement, div, false, false);
		configTxMode(jd3xx, channel, length, rampIncrement, div, true, false);
	}
	
	public static void stopMode(JD3XX jd3xx,int channel, int length, int rampIncrement, int div) throws IOException {
		configTxMode(jd3xx, channel, length, rampIncrement, div, false, false);
	}
	
	private static void configTxMode(JD3XX jd3xx,int channel, int length, int rampIncrement, int div, 
			boolean run, boolean step) throws IOException {
		byte[] txBuffer = new byte[50];
		buildHeader(txBuffer, TEST_CFG_ADDR, 3);
		int offset  = HEADER_LEN/4;
		int cfg = (run)?1<<7:0;
		cfg += (rampIncrement!=0)?1<<1:0;
		cfg += (step)?1:0;
		addIntegerToBuffer(txBuffer, offset, length-1);
		addIntegerToBuffer(txBuffer, offset+1, rampIncrement);
		addIntegerToBuffer(txBuffer, offset+2, div);
		addIntegerToBuffer(txBuffer, offset+3, cfg);
		int countData;
		countData = jd3xx.writePipe(getTxPipeID(channel), txBuffer, 0, HEADER_LEN+4*4);
		assertEquals("Bytes escritos distintos de bytes a escribir", countData, HEADER_LEN+4*4); 
	}
}
