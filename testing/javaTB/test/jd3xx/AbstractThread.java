/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jd3xx;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ndassieu
 */
public abstract class AbstractThread implements Runnable {
	protected ArrayBlockingQueue<byte[]> inDataQueue;
	protected ArrayBlockingQueue<byte[]> outDataQueue;
	
	protected ArrayBlockingQueue<ThreadMessage> inMessageQueue;
	protected ArrayBlockingQueue<ThreadMessage> outMessageQueue;
	private final Thread t;
	
	public AbstractThread(int inDataQueueSize, int inCommandQueueSize) {
		inDataQueue = new ArrayBlockingQueue<>(inDataQueueSize);
		inMessageQueue = new ArrayBlockingQueue<>(inCommandQueueSize);
		
		t = new Thread( this); 
	}
	
	public void start() {
		t.start();
	}
	
	public ArrayBlockingQueue<ThreadMessage> getInMessageQueue() {
		return inMessageQueue;
	}
	
	public void setOutMessageQueue(ArrayBlockingQueue<ThreadMessage> OutMessageQueue) {
		this.outMessageQueue = OutMessageQueue;
	}
	
	public ArrayBlockingQueue<byte[]> getInDataQueue() {
		return inDataQueue;
	}
	
	public void setOutDataQueue(ArrayBlockingQueue<byte[]> outDataQueue) {
		this.outDataQueue = outDataQueue;
	}
	
	protected byte[] createOrRecycle(int packetSize) {
		byte[] recycled = inDataQueue.poll();
		if ((recycled == null) || (packetSize!=recycled.length)) {
			recycled = new byte[packetSize];
		} 
		return recycled;
	}
	
	protected void sendData(byte[] toSend) {
		if (outDataQueue!=null) {
			try {
				outDataQueue.put(toSend);
			} catch (InterruptedException ex) {
				Logger.getLogger(AbstractThread.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}
	
	protected void dispose(byte[] toRecycle) {
		if (outDataQueue!=null) {
			outDataQueue.offer(toRecycle);
		}
	}
	
	protected void sendMessage(ThreadMessage toSend) {
		if (outMessageQueue != null) {
			try {
				outMessageQueue.put(toSend);
			} catch (InterruptedException ex) {
				Logger.getLogger(AbstractThread.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}
}
