/*
	JD3XX by Nicolás Dassieu Blanchet
	Based on JD3XX code by Pablo Bleyer Kocik.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright notice, this
	list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright notice,
	this list of conditions and the following disclaimer in the documentation
	and/or other materials provided with the distribution.

	3. The name of the author may not be used to endorse or promote products
	derived from this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
	EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
	PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
	BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
	ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
	POSSIBILITY OF SUCH DAMAGE.
*/

package jd3xx;

import java.io.IOException;

/** Java D3XX class */
public class JD3XX {

//	/* Device status */
//	public static final int
//		OK							= 0,
//		INVALID_HANDLE				= 1,
//		DEVICE_NOT_FOUND			= 2,
//		DEVICE_NOT_OPENED			= 3,
//		IO_ERROR					= 4,
//		INSUFFICIENT_RESOURCES		= 5,
//		INVALID_PARAMETER			= 6,
//		INVALID_BAUD_RATE			= 7,
//		DEVICE_NOT_OPENED_FOR_ERASE = 8,
//		DEVICE_NOT_OPENED_FOR_WRITE = 9,
//		FAILED_TO_WRITE_DEVICE		= 10,
//		EEPROM_READ_FAILED			= 11,
//		EEPROM_WRITE_FAILED			= 12,
//		EEPROM_ERASE_FAILED			= 13,
//		EEPROM_NOT_PRESENT			= 14,
//		EEPROM_NOT_PROGRAMMED		= 15,
//		INVALID_ARGS				= 16,
//		NOT_SUPPORTED				= 17,
//		OTHER_ERROR					= 18,
//		DEVICE_LIST_NOT_READY		= 19;

	/* openEx flags */
	public static final int
		OPEN_BY_SERIAL_NUMBER	= 1<<0,
		OPEN_BY_DESCRIPTION		= 1<<1,
		OPEN_BY_LOCATION		= 1<<2;

//	/* listDevices flags (used in conjunction with openEx flags) */
//	public static final int
//		LIST_NUMBER_ONLY	= 1<<31,
//		LIST_BY_INDEX		= 1<<30,
//		LIST_ALL			= 1<<29,
//		LIST_MASK			= LIST_NUMBER_ONLY|LIST_BY_INDEX|LIST_ALL;

	/* Device information flags */
	public static final int
		FLAGS_OPENED	= 1,
		FLAGS_HISPEED	= 2,
		FLAGS_SUPERSPEED = 4;

	/** Device information */
	public static class DeviceInfo {
		public int		index;			// device index in info list
		public int		flags;			// device flags
		public int		type;			// device type
		public int		id;				// device ID
		public int		location;		// device location ID
		public String	serial;
		public String	description;
		public int		handle;			// device handle

		@Override
		public String toString() {
			StringBuilder b = new StringBuilder();
			b.append("index: ").append(Integer.toString(index));
			b.append(", flags: 0x").append(Integer.toHexString(flags));
			switch(flags) {
				case FLAGS_SUPERSPEED:
					b.append("[USB 3.0]");
					break;
				case FLAGS_HISPEED:
					b.append("[USB 2.0]");
					break;
				case FLAGS_OPENED:
					b.append("[OPENED]");
					break;
			}
			b.append(", type: 0x").append(Integer.toHexString(type));
			b.append(", id: 0x").append(Integer.toHexString(id));
			b.append(", location: 0x").append(Integer.toHexString(location));
			b.append(", serial: ").append(serial);
			b.append(", description: ").append(description);
			b.append(", handle: 0x").append(Integer.toHexString(handle));
			return b.toString();
		}
	}

//	/*---------------------------- D2XX API ----------------------------------*/
//	
//	/** 
//	 * Get library version 
//	 * @return version number
//	 */
//	public native int getLibraryVersion();

	/** Builds a device information list and returns the number of D2XX devices
	connected to the system. The list contains information about both unopen and
	open devices.
	*/
	public native int createDeviceInfoList() throws IOException;

	/** Returns an entry from the device information list
		@return DeviceInfo object with device information details
	*/
	public native DeviceInfo getDeviceInfoDetail(int dn) throws IOException;

	/** Close device
	*/
	public native void close() throws IOException;
	
//	/** List devices
//		@param flags control how devices are listed
//		@return device information list as array of objects (types depend on flag)
//	*/
//	public native Object[] listDevices(int flags) throws IOException;
	
	/** Extended open (by name)
		@param name device serial number or description
		@param flags selects open from serial number or description
	*/
	public native void create(String name, int flags) throws IOException;
	
	/** Extended open (by number)
		@param location device location
		@param flags selects open by location
	*/
	public native void create(int location, int flags) throws IOException;

	/** Start Read transacction
		@param bytes array to store read bytes
		@param offset begin index
		@param length amount of bytes desired
		@return number of bytes actually read
	*/
	public native void startReadPipe(byte pipeID, int length, long overlapped) throws IOException;
	
	public native int getOverlappedResults(byte pipeID, byte[] bytes, int offset, long overlapped) throws IOException;
			
	public native long initializeOverlapped() throws IOException;
	
	public native void releaseOverlapped(long overlapped);
	
	/** Read bytes from device
		@param bytes array to store read bytes
		@param offset begin index
		@param length amount of bytes desired
		@return number of bytes actually read
	*/
	public native int readPipe(byte pipeID, byte[] bytes, int offset, int length) throws IOException;
	
	/** Write bytes to device
		@param bytes array with bytes to be sent
		@param offset begin index
		@param length amount of bytes desired
		@return number of bytes actually written
	*/
	public native int writePipe(byte pipeID, byte[] bytes, int offset, int length) throws IOException;

	/** Set pipe Streaming size
	 *	@param allWrPipes true si se desea setear todas las pipes de escritura
	 *  @param allRdPipes true si se desea setear todas las pipes de lectura
		@param pipeID pipe number to set the timeout
		@param stremSize tamaño del paquete de streaming
	*/
	public native void setStreamPipe(boolean allWrPipes, boolean allRdPipes, byte pipeID, long streamSize) throws IOException;
	
	/** clear pipe Streaming, deshabilita el streaming del pipe
	 *	@param allWrPipes true si se desea limpiar todas las pipes de escritura
	 *  @param allRdPipes true si se desea limpiar todas las pipes de lectura
		@param pipeID pipe number to set the timeout
	*/
	public native void clearStreamPipe(boolean allWrPipes, boolean allRdPipes, byte pipeID) throws IOException;
	
	/** Set pipe Timeout in Milliseconds
		@param pipeID pipe number to set the timeout
		@param msTimeout timeout in ms
	*/
	public native void setPipeTimeout(byte pipeID, long msTimeout) throws IOException;
	
	/** Get pipe Timeout in Milliseconds
		@param pipeID pipe number to get the timeout
		@return pipe timeout in ms
	*/
	public native long getPipeTimeout(byte pipeID) throws IOException;
	
	/** Aborts all of the pending transfers for a pipe
		@param pipeID pipe number
	*/
	public native void abortPipe(byte pipeID) throws IOException;
	
//	/** Get device information
//		@return DeviceInfo object with device information
//	*/
//	public native DeviceInfo getDeviceInfo() throws IOException;


//	/** -------------------- End of native calls -----------------------------*/
//	
//	
	/** Internal FT_HANDLE */
	protected int handle	= -1;
//	/** Internal event handle */
//	protected int event		= -1;
//	/** Internal event mask */
//	protected int mask		= 0;
//	/** Kill notifier thread */
//	protected boolean kill	= false;
//	/** Event listener object */
//	protected JD3XXEventListener listener = null;
//	/** Listener notifier thread */
//	protected Thread notifier = null;

	static {
		System.loadLibrary("FTD3XX"); // llamada original
		System.loadLibrary("jd3xx"); // llamada original
		//System.loadLibrary("FTD3XX"); // llamada original
	}

//	/** Create a new unopened JD3XX object */
//	public JD3XX() {
//		//System.out.println("Lib version: " + getLibraryVersion() );
//	}
//
//	/** Create a new JD3XX object and open device by number */
//	public JD3XX(int deviceNumber) throws IOException {
//		open(deviceNumber);
//	}
//
//	/** Create a new JD3XX object and open device by serial number or description */
//	public JD3XX(String name, int flags) throws IOException {
//		openEx(name, flags);
//	}
//
//	/** Create a new JD3XX object and open device by location */
//	public JD3XX(int location, int flags) throws IOException {
//		openEx(location, flags);
//	}

	protected void finalize()
	throws Throwable {
		try {
			// if (handle != 0) close();
			close();
		}
		finally {
			super.finalize();
		}
	}

	/** Open device by serial number alias */
	public void openBySerialNumber(String name) throws IOException {
		create(name, OPEN_BY_SERIAL_NUMBER);
	}

	/** Open device by description alias */
	public void openByDescription(String name) throws IOException {
		create(name, OPEN_BY_DESCRIPTION);
	}

	/** Open device by location alias */
	public void openByLocation(int location) throws IOException {
		create(location, OPEN_BY_LOCATION);
	}

//	/** List devices by serial number alias */
//	public Object[] listDevicesBySerialNumber() throws IOException {
//		return listDevices(OPEN_BY_SERIAL_NUMBER);
//	}
//
//	/** List devices by serial description alias */
//	public Object[] listDevicesByDescription() throws IOException {
//		return listDevices(OPEN_BY_DESCRIPTION);
//	}
//
//	/** List devices by location alias */
//	public Object[] listDevicesByLocation() throws IOException {
//		return listDevices(OPEN_BY_LOCATION);
//	}
//
//	/** Read bytes from device helper function */
//	public byte[] read(int s) throws IOException {
//		byte[] b = new byte[s];
//		int r = read(b);
//		if (r == b.length) return b;
//		else {
//			byte[] c = new byte[r];
//			System.arraycopy(b, 0, c, 0, r);
//			return c;
//		}
//	}
//
//	/** Read bytes from device helper function */
//	public int read(byte b[]) throws IOException {
//		return read(b, 0, b.length);
//	}
//
//	/** Force read single byte from device */
//	public int read() throws IOException {
//		byte[] b = new byte[1];
//		if (read(b) != 1) throw new IOException("io error");
//		return b[0] & 0xff;
//	}
//
//	/** Write single byte to device */
//	public int write(int b) throws IOException {
//		byte[] c = new byte[1];
//		c[0] = (byte)b;
//		return write(c);
//	}
//
//	/** Write bytes to device helper function */
//	public int write(byte b[]) throws IOException {
//		return write(b, 0, b.length);
//	}

}
